#!/bin/sh

export PATH=$PATH:$PWD/Tools/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD/Project/bin
echo 'Mesh-imprimator-tools'
echo 'You can now directly run "example", "plyviewer", "objviewer" or "masktester".'
$SHELL

