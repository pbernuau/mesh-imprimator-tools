#ifndef VORONOI_H
#define VORONOI_H

#include <MeshImprimator/Config.h>

#include <limits>
#include <map>
#include <set>
#include <utility>
#include <vector>

struct DelauTriangle
{
    // vi is the index of the Delaunay vertex
    int v1, v2, v3;
};

struct VoroEdge
{
    int v1, v2;

    Vector3d direction;

    // The Delaunay triangle that this edge goes through
    DelauTriangle delauTriangle;

    bool isFinite() const { return v2 != -1; }
    bool isInfinite() const { return v2 == -1; }
};

struct VoroCell
{
    // The Delaunay vertex at the center of the cell
    int delauVertex;

    // The Voronoi edges of this cell
    std::set<int> voroEdges;

    // The Voronoi vertices of this cell
    std::set<int> voroVertices;

    // The positive pole of this cell
    int positivePole;

    // The negative pole of this cell
    int negativePole;

    // The cosine of the angle psq (between ps and pq, p being the Delaunay
    // vertex, s the positive pole and q the negative pole)
    double cosBeta;

    // The local feature size of a cloud point that is used to estimate
    // whether a cell is well-shaped
    double lfs;

    VoroCell() :
        positivePole(-1),
        negativePole(-1),
        cosBeta(std::numeric_limits<double>::signaling_NaN()),
        lfs(0.0)
    {
    }
};

class VoroVertex;
typedef std::multimap<double, VoroVertex*> VoroQueue;

class VoroVertex
{
public:
    enum Label
    {
        UNDEFINED,
        BAD_POLE,
        IN,
        OUT
    };

    VoroVertex() :
        power(std::numeric_limits<double>::infinity()),
        label(UNDEFINED),
        in(0.0),
        out(0.0),
        _queue(NULL)
    {
    }

    ~VoroVertex()
    {
        bind(NULL);
    }

    Vector3d position;

    // The Voronoi vertices connected to this vertex by a Voronoi edge
    std::set<int> adjacentVoroVertices;

    // The Delaunay Vertices for which this vertex is the positive pole
    std::set<int> attachedDelauVertices;

    std::list< std::pair<int,double> > deeplyIntersectNeighPoles;

    // The power of this Voronoi vertex is the smallest distance between this
    // vertex and any Delaunay point in the cloud
    double power;

    Label label;

    double in, out;

    void bind(VoroQueue* queue)
    {
        if (_queue != NULL)
            _queue->erase(_position);

        if (queue != NULL)
            _position = queue->insert(std::make_pair(priority(), this));

        _queue = queue;
    }

    void update()
    {
        if (_queue == NULL)
            return;

        _queue->erase(_position);
        _position = _queue->insert(std::make_pair(priority(), this));
    }

    // Computes Amenta's pole priority
    double priority() const
    {
        if (in > 0.0 && out > 0.0)
        {
            return fabs(in - out) - 1.0;
        }
        else
        {
            return std::max(in, out);
        }
    }

private:
    VoroQueue* _queue;
    VoroQueue::iterator _position;
};

struct VoroData
{
    std::vector<Vector3d>       delauVertices;
    std::vector<VoroVertex>     voroVertices;
    std::vector<VoroEdge>       voroEdges;
    std::vector<VoroCell>       voroCells;

    std::set<int> getInfiniteEdges(const VoroCell& cell) const
    {
        std::set<int> edges;

        for (std::set<int>::iterator it = cell.voroEdges.begin();
             it != cell.voroEdges.end();
             ++it)
        {
            const VoroEdge& edge = voroEdges.at(*it);
            if (edge.isInfinite())
                edges.insert(*it);
        }

        return edges;
    }
};



#endif // VORONOI_H
