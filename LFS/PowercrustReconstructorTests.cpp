#include "PowerCrustReconstructorTests.h"

#include <cmath>

class NullProgress : public IProgress
{
public:
    virtual void setValue(int) {}
    virtual bool wasCanceled() const { return false; }
    virtual void setRange(int, int) {}
};

PowerCrustReconstructorTests::PowerCrustReconstructorTests() :
    powercrust(NULL),
    voroData(NULL)
{
}

void PowerCrustReconstructorTests::initTestCase()
{
    PointCloud cube, tetrahedron;

    // init ply file
    PLYReader reader;
    std::string inputFilename = "models/suzanne.ply";
    reader.open(inputFilename);

    int errorCode, line;

    if (!reader.readHeader(&errorCode, &line))
    {
        std::cerr << "Error: " << errorCode << std::endl;
        std::cerr << "Line: " << line << std::endl;
        //printUsage();
        return;
    }

    std::cerr << "Header successfully read." << std::endl;

    if (!reader.readContent(&errorCode, &line))
    {
        std::cerr << "Error: " << errorCode << std::endl;
        std::cerr << "Line: " << line << std::endl;

        return;
    }

    std::cerr << "Content successfully read." << std::endl;

    reader.close();

    const Ply::Object* obj = reader.getObject();
    obj->print(std::cerr);

    PointCloud cloud = obj->getPointCloud();

    // init cube
    cube.points.insert(Vector3d(0.0, 0.0, 0.0));
    cube.points.insert(Vector3d(1.0, 0.0, 0.0));
    cube.points.insert(Vector3d(1.0, 1.0, 0.0));
    cube.points.insert(Vector3d(0.0, 1.0, 0.0));
    cube.points.insert(Vector3d(0.0, 0.0, 1.0));
    cube.points.insert(Vector3d(1.0, 0.0, 1.0));
    cube.points.insert(Vector3d(1.0, 1.0, 1.0));
    cube.points.insert(Vector3d(0.0, 1.0, 1.0));

    // init tetrahedron
    const double a = sqrt(2.0 / 3.0);
    const double b = 1.0 / (2.0 * sqrt(6.0));
    const double c = 1.0 / (2.0 * sqrt(3.0));
    const double d = 1.0 / sqrt(3.0);

    tetrahedron.points.insert(Vector3d(0.0, 0.0, a - b));
    tetrahedron.points.insert(Vector3d(-c, -0.5, -b));
    tetrahedron.points.insert(Vector3d(-c, +0.5, -b));
    tetrahedron.points.insert(Vector3d(+d, -0.5, -b));

    // init Powercrust
    IProgress* progress = new NullProgress();
    powercrust = new PowercrustReconstructor(cloud);
    powercrust->reconstruct(progress);
    voroData = powercrust->debugVoroData();
    delete progress;
}

void PowerCrustReconstructorTests::cleanupTestCase()
{
    if (powercrust != NULL)
        delete powercrust;

    powercrust = NULL;
    voroData = NULL;
}

// Test of the voronoi diagram function
void PowerCrustReconstructorTests::testVoronoiDiagram()
{
    // For each cell, the voronoi vertices of the cell are contained
    // in the list of the adjacent voronoi vertices of the others
    for(std::vector<VoroCell>::const_iterator c =
            voroData->voroCells.begin();
        c != voroData->voroCells.end();
        ++c)
    {
        for(std::set<int>::const_iterator it = c->voroEdges.begin();
            it != c->voroEdges.end();
            ++it)
        {
            const VoroEdge& edge = voroData->voroEdges[*it];

            if (edge.isFinite() && edge.v1 != edge.v2)
            {
                QVERIFY2(voroData->voroVertices[edge.v1].adjacentVoroVertices
                        .count(edge.v2),
                "Error in adjacentVoroVertices");

                QVERIFY2(voroData->voroVertices[edge.v2].adjacentVoroVertices
                        .count(edge.v1),
                "Error in adjacentVoroVertices");
            }
        }
    }

    // Each cell must have at least one voronoi vertex
    // and at least two voronoi edges
    for(std::vector<VoroCell>::const_iterator c =
            voroData->voroCells.begin();
        c != voroData->voroCells.end();
        ++c)
    {
        QVERIFY2(c->voroVertices.size() > 0,
                 "A cell has no voronoi vertex");

        QVERIFY2(c->voroEdges.size() > 0,
                 "A cell has less than two voronoi edges");
    }
}

// Tests the post-conditions of computeVoronoiPoles()
void PowerCrustReconstructorTests::testVoronoiPoles()
{
    //
    // Test 3: the cosBeta of a cell is in the range [-1;0]
    //

    for (int i = 0; i < voroData->voroCells.size(); i++)
    {
        const VoroCell& cell = voroData->voroCells[i];

        QVERIFY(cell.cosBeta >= -1 && cell.cosBeta <= 0.0);
    }

    //
    // Test 1: the two poles of a VoroVertex are different and the negative
    // pole is not -1
    //

    int voroVertexCount = voroData->voroVertices.size();

    // for each VoroVertex
    for (int i = 0; i < voroData->voroCells.size(); i++)
    {
        const VoroCell& cell = voroData->voroCells[i];

        // sanity checks: test range value [ -1; voroVertexCount [
        QVERIFY2(cell.positivePole >= -1 && cell.positivePole < voroVertexCount,
                 "The positive pole is out of bounds.");
        QVERIFY2(cell.negativePole >= -1 && cell.negativePole < voroVertexCount,
                 "The negative pole is out of bounds.");

        // infinite negative pole?
        QVERIFY2(cell.negativePole != -1,
                 "The negative pole of a cell is -1.");

        // identical poles?
        QVERIFY2(cell.positivePole != cell.negativePole,
                 "A VoroCell has two identical poles.");
    }

    //
    // Test 2: the VoroVertices and their positive poles are correctly attached
    //

    std::set<int>::const_iterator it;

    // for each VoroVertex
    for (int i = 0; i < voroData->voroVertices.size(); i++)
    {
        const VoroVertex& vert = voroData->voroVertices[i];

        // for each DelauVertex attached to it as a pole
        for (it = vert.attachedDelauVertices.begin();
             it != vert.attachedDelauVertices.end();
             ++it)
        {
            // check that a pole of that DelauVertex is i
            QVERIFY2(voroData->voroCells[*it].positivePole == i ||
                     voroData->voroCells[*it].negativePole == i,
                     "A positive pole is not attached correctly.");
        }
    }

    // Do the same verification the other way around

    // for each DelauVertex / VoroCell
    for (int i = 0; i < voroData->voroCells.size(); i++)
    {
        const VoroCell& cell = voroData->voroCells[i];

        if (cell.positivePole == -1)
            continue;

        const VoroVertex& pole = voroData->voroVertices[cell.positivePole];

        // check that the positive pole of the cell is attached to the vertex

        QVERIFY2(pole.attachedDelauVertices.count(i) == 1,
                 "A positive pole is not attached correctly.");
    }


}

// Test of the power diagram function
void PowerCrustReconstructorTests::testPowerDiagram()
{
    // The powers of each vorovertex must be > 0
    for(std::vector<VoroVertex>::const_iterator v =
            voroData->voroVertices.begin();
        v != voroData->voroVertices.end();
        ++v)
    {
        QVERIFY2(v->power > 0, "The power of a voronoi vertex is not > 0");
        QVERIFY2(!std::isinf(v->power),
                 "The power of a voronoi vertex is infinite");
    }
}

void PowerCrustReconstructorTests::testDeeplyIntersectNeighPoles()
{
    for (int i = 0; i < voroData->voroVertices.size(); i++)
    {
        const VoroVertex& p = voroData->voroVertices[i];

        std::list< std::pair<int,double> >::const_iterator it1, it2;

        for (it1 = p.deeplyIntersectNeighPoles.begin();
             it1 != p.deeplyIntersectNeighPoles.end();
             ++it1)
        {
            int ind             = it1->first;
            double cosAlpha     = it1->second;

            const VoroVertex& vq = voroData->voroVertices[ind];

            // Check that no pole is in the list twice
            bool found = false;
            for (it2 = vq.deeplyIntersectNeighPoles.begin();
                 it2 != vq.deeplyIntersectNeighPoles.end();
                 ++it2)
            {
                if (i != it2->first)
                    continue;

                QVERIFY2(!found, "Duplicate pole in neighboring poles.");
                QCOMPARE(cosAlpha, it2->second);

                found = true;
            }

            // Check that any deeply intersecting pole belongs to the set of
            // adjacent Voronoi vertices
            QVERIFY2(p.adjacentVoroVertices.count(ind) == 1,
                     "A deeply intersecting pole is not in the set"
                     " of adjacent Voronoi vertices.");

            // Check that the poles are properly connected
            QVERIFY2(found, "Missing reciprocal neighboring pole.");
            QVERIFY(cosAlpha < MI_EPSILON);
            QVERIFY(cosAlpha + MI_EPSILON > -1.0);
        }
    }
}

// Test of the voronoi pole labelling
void PowerCrustReconstructorTests::testLabels()
{
    // Each VoroVertex label must not be undefined and must be labeled
    // consistently with its in and out attributes
    for(std::vector<VoroVertex>::const_iterator v =
            voroData->voroVertices.begin();
        v != voroData->voroVertices.end();
        ++v)
    {
        bool in = (v->in > v->out + MI_EPSILON);

        switch (v->label)
        {
        case VoroVertex::UNDEFINED:
            QFAIL("A VoroVertex is labeled UNDEFINED.");
            break;
        case VoroVertex::BAD_POLE:
            continue;
        case VoroVertex::IN:
            QVERIFY2(in, "A VoroVertex is badly labeled.");
            break;
        case VoroVertex::OUT:
            QVERIFY2(!in, "A VoroVertex is badly labeled.");
            break;
        }
    }


    // The two poles of a cell must have opposite labels
    for(std::vector<VoroCell>::const_iterator c = voroData->voroCells.begin();
        c != voroData->voroCells.end();
        ++c)
    {
        int p = c->positivePole, q = c->negativePole;

        if (p == -1)
        {
            QVERIFY2(voroData->voroVertices[q].label == VoroVertex::IN,
                    "The negative pole of an unbounded cell is not labeled IN");
        }
        else if (voroData->voroVertices[p].label == VoroVertex::OUT)
        {
            QVERIFY2(voroData->voroVertices[q].label == VoroVertex::IN,
                    "The two poles of a cell are labelled OUT");
        }
        else if (voroData->voroVertices[p].label == VoroVertex::IN)
        {
            QVERIFY2(voroData->voroVertices[q].label == VoroVertex::OUT,
                    "The two poles of a cell are labelled IN");
        }
    }


    // There exists a voronoi vertex that is labelled IN
    int count = 0;
    for(std::vector<VoroVertex>::const_iterator v =
            voroData->voroVertices.begin();
        v != voroData->voroVertices.end();
        ++v)
    {
        if (v->label == VoroVertex::IN)
            count++;
    }
    QVERIFY2(count > 0, "All voronoi vertices are labeled OUT");
}

