#include <MeshImprimator/Algorithms/PowercrustReconstructor.h>

#include <MeshImprimator/Geometry/TriangleMesh.h>
#include <MeshImprimator/Algorithms/IProgress.h>
#include <MeshImprimator/Algorithms/Voronoi.h>

#include <tetgen.h>

#include <algorithm>
#include <deque>

#include <cmath>

bool ComputeIntersectAngle(Vector3d c1, double r1,
                           Vector3d c2, double r2,
                           double& cosAlpha);

inline bool equals(const Vector3d& u, const Vector3d& v)
{
    Compare3d cmp;
    return !cmp(u, v) && !cmp(v, u);
}

inline bool equals(double u, double v)
{
    Compare3d cmp;
    return cmp.equals(u, v);
}

//==============================================================================

PowercrustReconstructor::PowercrustReconstructor(const PointCloud& pointCloud,
                                                 double r) :
    pointCloud(pointCloud),
    r(r),
    voroData(NULL)
{
}

PowercrustReconstructor::~PowercrustReconstructor()
{
    clear();
}

TriangleMesh PowercrustReconstructor::reconstruct(IProgress* progress)
{
    reconstructIndexed(progress);
    return triangleMesh.toUnindexed();
}

IndexedTriangleMesh PowercrustReconstructor::reconstructIndexed(
        IProgress* progress)
{
    clear();

    progress->setRange(0, 600);
    progress->setValue(0);

    createVoronoiDiagram();
    progress->setValue(100);

    computeVoronoiPoles(progress);
    computePowerDiagram(progress);
    computeDINP(progress);
    labelVoronoiPoles(progress);
    labelBadPoles(progress);
    filterDelaunayFaces(progress);

    return triangleMesh;
}

void PowercrustReconstructor::clear()
{
    triangleMesh.triangles.clear();
    triangleMesh.vertices.clear();

    if (voroData != NULL)
    {
        delete voroData;
        voroData = NULL;
    }
}

// Compute the power of every voronoi vertex
// The power of this Voronoi vertex is the smallest distance between this
// vertex and any Delaunay point in the cloud
void PowercrustReconstructor::createVoronoiDiagram()
{
#ifdef MI_DEBUG
    std::cerr << "Creating Voronoi diagram..." << std::endl;
#endif
    tetgenio in;
    in.firstnumber              = 0;
    in.numberofpoints           = pointCloud.points.size();
    std::vector<double> points  = pointCloud.flatten();
    in.pointlist                = points.data();

    tetgenbehavior b;
    b.voroout = true;
    b.edgesout = true;
    b.facesout = true;
#ifndef MI_DEBUG
    b.quiet = true;
#endif

    tetgenio out;
    // Compute the tetrahedra of the point cloud
    tetrahedralize(&b, &in, &out);
    in.pointlist = NULL;

    //
    // Create new Voronoi Data object
    //

    voroData = new VoroData();

    // Step 1: set Delaunay vertices

    voroData->delauVertices.reserve(out.numberofpoints);

    for(int i = 0; i < out.numberofpoints; i++)
    {
        const REAL* p = &out.pointlist[3*i];
        voroData->delauVertices.push_back(Vector3d(p[0], p[1], p[2]));
    }

    // Step 2: set Voronoi vertices

    // Tetgen Voronoi vertices are not unique
    // vvertices is the set of unique Voronoi vertex positions
    // vindices is an array mapping a Voronoi index to its new unique index
    std::set<Vector3d, Compare3d> vvertices;
    std::vector<int> vindices;

    // fill the set
    for (int i = 0; i < out.numberofvpoints; i++)
    {
        const REAL* ptr = &out.vpointlist[3*i];
        vvertices.insert(Vector3d(ptr[0], ptr[1], ptr[2]));
    }

    // fill Voronoi vertices in VoroData
    voroData->voroVertices.reserve(vvertices.size());
    for (std::set<Vector3d, Compare3d>::iterator it = vvertices.begin();
         it != vvertices.end();
         ++it)
    {
        VoroVertex vv;
        vv.position = *it;
        voroData->voroVertices.push_back(vv);
    }

    // compute index map
    vindices.resize(out.numberofvpoints, -1);
    for (int i = 0; i < out.numberofvpoints; i++)
    {
        const REAL* ptr = &out.vpointlist[3*i];
        const Vector3d position = Vector3d(ptr[0], ptr[1], ptr[2]);

        for (unsigned int j = 0; j < voroData->voroVertices.size(); j++)
        {
            if (equals(position, voroData->voroVertices[j].position))
            {
                vindices[i] = j;
                break;
            }
        }
    }

    // Step 3: set Voronoi edges

    // Compute and push all VoroEdge and connect the vertices.

    for (int i = 0; i < out.numberofvedges; i++)
    {
        const tetgenio::voroedge* edge = &(out.vedgelist[i]);

        int v1 = vindices[edge->v1];
        int v2 = (edge->v2 == -1 ? -1 : vindices[edge->v2]);

        // create a VoroEdge
        VoroEdge myEdge;
        myEdge.direction(0) = edge->vnormal[0];
        myEdge.direction(1) = edge->vnormal[1];
        myEdge.direction(2) = edge->vnormal[2];
        myEdge.v1 = v1;
        myEdge.v2 = v2;
        myEdge.delauTriangle.v1 = out.trifacelist[3 * i + 0];
        myEdge.delauTriangle.v2 = out.trifacelist[3 * i + 1];
        myEdge.delauTriangle.v3 = out.trifacelist[3 * i + 2];
        voroData->voroEdges.push_back(myEdge);

        // connect the VoroVertices
        if (v2 != -1 && v1 != v2)
        {
            voroData->voroVertices[v1].adjacentVoroVertices.insert(v2);
            voroData->voroVertices[v2].adjacentVoroVertices.insert(v1);
        }
    }

    // Step 4: set Voronoi cells and set adjacent Voronoi vertices

    voroData->voroCells.reserve(out.numberofvcells);

    for(int i = 0; i < out.numberofvcells; i++)
    {
        VoroCell cell;

        cell.delauVertex = i;

        // for each facet...
        int facetCount = out.vcelllist[i][0] - 1;
        for(int j = 0; j < facetCount; j++)
        {
            const tetgenio::vorofacet* facet =
                    &(out.vfacetlist[out.vcelllist[i][j+1]]);

            // for each edges...
            int edgeCount = facet->elist[0] - 1;
            for(int k = 0; k < edgeCount; k++)
            {
                int edgeIndex = facet->elist[k+1];
                const tetgenio::voroedge& edge = out.vedgelist[edgeIndex];

                // add the index of this edge to the list
                cell.voroEdges.insert(edgeIndex);

                // add the VoroVertices to the cell
                cell.voroVertices.insert(vindices[edge.v1]);
                if (edge.v2 != -1)
                    cell.voroVertices.insert(vindices[edge.v2]);
            }
        }

        voroData->voroCells.push_back(cell);
    }
}

void PowercrustReconstructor::computeVoronoiPoles(IProgress* progress)
{
#ifdef MI_DEBUG
    std::cerr << "Computing Voronoi poles..." << std::endl;
#endif
int countBad = 0;
    std::vector<VoroCell>& cells = voroData->voroCells;

    // for each voronoi cell
    for(unsigned int i = 0; i < cells.size(); i++)
    {
        progress->setValue(100 + (i * 100)/voroData->voroCells.size());

        VoroCell* cell = &cells[i];

        // p is the Delaunay Vertex at the center of the cell
        Vector3d p = voroData->delauVertices[cell->delauVertex];
        std::set<int> infiniteEdges = voroData->getInfiniteEdges(*cell);

        //
        // compute the positive pole of the cell
        //

        Vector3d u(0.0, 0.0, 0.0);

        if (infiniteEdges.empty())
        {
            // the cell is bounded

            // the positive pole is the voronoi vertex of the cell with the
            // maximal distance to the sample point p

            double max = -1.0, d = 0.0;

            for(std::set<int>::iterator vert = cell->voroVertices.begin();
                vert != cell->voroVertices.end();
                ++vert)
            {
                d = (voroData->voroVertices.at(*vert).position - p).norm();

                if (d > max)
                {
                    max                 = d;
                    cell->positivePole  = *vert;
                }
            }

            // attach the delaunay vertex of the cell to its pole
            voroData->voroVertices[cell->positivePole]
                    .attachedDelauVertices.insert(cell->delauVertex);

            // let u be the vector from p to the positive pole
            u = voroData->voroVertices[cell->positivePole].position - p;
            u.normalize();
        }
        else
        {
            // the cell is unbounded

            // the positive pole is undefined
            cell->positivePole = -1;

            // let u be the average direction of all unbounded voro edges
            for(std::set<int>::iterator edge = infiniteEdges.begin();
                edge != infiniteEdges.end();
                ++edge)
            {
                u += voroData->voroEdges[*edge].direction;
            }

            u = -u / static_cast<double>(infiniteEdges.size());
            u.normalize();
        }

        //
        // compute the lfs and the negative pole of the cell
        //

        double max = -1.0;
        for(std::set<int>::iterator vert = cell->voroVertices.begin();
            vert != cell->voroVertices.end();
            ++vert)
        {
            // Compute the distance and direction of the vector vp between
            // the delaunay vertex (p) and the voronoi vertex
            Vector3d vp = voroData->voroVertices.at(*vert).position - p;

            double delauVoroVertexDist  = vp.norm();
            vp                          = vp.normalized();

            // Compute the cosine between the two directions
            double cos_ = u.dot(vp);

            // if the voronoi vertex is not on the same side as the first
            // pole
            if (cos_ < 0.0)
            {
                if (delauVoroVertexDist > max)
                {
                    max                 = delauVoroVertexDist;
                    cell->negativePole  = *vert;
                    cell->cosBeta       = cos_;
                }
            }

            // update the cell lfs
            if (r < 1.0)
            {
                double estimateLfs = 0.0;
                double cosR = ::cos(2*r);

                // the voronoi vertex must be near the sample
                if (cos_ + MI_EPSILON < cosR && cos_ > - cosR + MI_EPSILON)
                {
                    estimateLfs = delauVoroVertexDist / r *
                                  ((sqrt(1 - cos_ * cos_)) - r);

                    if (estimateLfs > cell->lfs)
                        cell->lfs = estimateLfs;
                }
            }

            // Check whether the two poles pass the skinnyness test
            Vector3d vq;

            if (cell->positivePole != -1)
            {
                vp = voroData->voroVertices[cell->positivePole].position - p;
                if (vp.norm() < cell->lfs)
                {
                    voroData->voroVertices[cell->positivePole].label =
                            VoroVertex::BAD_POLE;
                }
            }

            if (cell->negativePole != -1)
            {
                vq = voroData->voroVertices[cell->negativePole].position - p;
                if (vq.norm() < cell->lfs)
                {
                    voroData->voroVertices[cell->negativePole].label =
                            VoroVertex::BAD_POLE;
                }
            }
        }
    }
}

// For each voronoi vertex,
void PowercrustReconstructor::computePowerDiagram(IProgress* progress)
{
#ifdef MI_DEBUG
    std::cerr << "Computing power diagram..." << std::endl;
#endif

    for (unsigned int i = 0; i < voroData->voroCells.size(); i++)
    {
        progress->setValue(200 + (i * 100)/voroData->voroCells.size());

        VoroCell& cell = voroData->voroCells[i];
        Vector3d dV = voroData->delauVertices[cell.delauVertex];

        for (std::set<int>::iterator j = cell.voroVertices.begin();
             j != cell.voroVertices.end();
             ++j)
        {
            VoroVertex& voroV = voroData->voroVertices[*j];
            double distance = (voroV.position - dV).norm();

            voroV.power = std::min(voroV.power, distance);
        }
    }
}

void PowercrustReconstructor::computeDINP(IProgress* progress)
{
#ifdef MI_DEBUG
    std::cerr << "Computing deeply intersecting neighboring poles..."
              << std::endl;
#endif

    for (unsigned int i = 0; i < voroData->voroVertices.size(); i++)
    {
        progress->setValue(300 + (i * 100)/voroData->voroVertices.size());

        VoroVertex& v1 = voroData->voroVertices[i];

        for (std::set<int>::iterator it = v1.adjacentVoroVertices.begin();
             it != v1.adjacentVoroVertices.end();
             ++it)
        {
            int j = *it;

            // skip cases already done
            if (j > (int) i)
                continue;

            VoroVertex& v2 = voroData->voroVertices[j];

            double cosAlpha;

            // compute the intersection angle
            if (ComputeIntersectAngle(v1.position, v1.power,
                                      v2.position, v2.power,
                                      cosAlpha))
            {
                // the two balls deeply intersect
                if (cosAlpha < 0.0)
                {
                    v1.deeplyIntersectNeighPoles.push_back(
                                std::make_pair(j, cosAlpha));
                    v2.deeplyIntersectNeighPoles.push_back(
                                std::make_pair(i, cosAlpha));
                }
            }
        }
    }
}

class PriorityQueue
{
    struct PoleComparator
    {
        PoleComparator(const VoroData* data) : data(data) {}

        // compares poles p and q given their priority
        bool operator()(int p, int q) const
        {
            const double pp = data->voroVertices[p].priority();
            const double pq = data->voroVertices[q].priority();

            return pp + MI_EPSILON < pq;
        }

        const VoroData* data;
    };
public:
    PriorityQueue(const VoroData* data) : data(data), compare(data)
    {
        int size = data->voroVertices.size();

        // for each pole p, insert p in the queue
        queue.reserve(size);
        for (int i = 0; i < size; i++)
        {
            queue.push_back(i);
        }
    }

    bool empty() const
    {
        return queue.empty();
    }

    int size() const
    {
        return queue.size();
    }

    void update()
    {
        std::make_heap(queue.begin(), queue.end(), compare);
    }

    // returns the top priority element and remove it from the queu
    int pop()
    {
        std::pop_heap(queue.begin(), queue.end(), compare);
        int p = queue.back();
        queue.pop_back();
        return p;
    }

private:
    const VoroData* data;
    const PoleComparator compare;
    std::vector<int> queue;
};

void PowercrustReconstructor::labelVoronoiPoles(IProgress* progress)
{
#ifdef MI_DEBUG
    std::cerr << "Labeling Voronoi poles..." << std::endl;
#endif

    // for each cell c such as c.positivePole = -1 : q.in = w_psq
    for (unsigned int i = 0; i < voroData->voroCells.size(); i++)
    {
        VoroCell& cell = voroData->voroCells[i];

        if (cell.positivePole == -1)
        {
            VoroVertex& vv = voroData->voroVertices[cell.negativePole];
            vv.in = std::max(vv.in, -cell.cosBeta);
        }
    }

    VoroQueue queue;

    // for each pole p that is not a bad pole or that is a bad pole but also
    // the negative pole of an unbounded cell, insert p in the queue
    for (std::vector<VoroVertex>::iterator it = voroData->voroVertices.begin();
         it != voroData->voroVertices.end();
         ++it)
    {
        if (it->label != VoroVertex::BAD_POLE || it->in > 0)
            it->bind(&queue);
    }

    int count = 0;
    while (!queue.empty())
    {
        progress->setValue(400 + (count++ * 100)/voroData->voroVertices.size());

#ifdef MI_DEBUG
    std::cerr << "\rPoles remaining: " << queue.size() << "      ";
#endif
        double tmp;

        //Remove the top priority element
        VoroQueue::iterator last = queue.end();
        last--;
        VoroVertex& vp = *(last->second);
        vp.bind(NULL);

        if (vp.in > MI_EPSILON + vp.out)
        {
            vp.label = VoroVertex::IN;
            tmp = vp.in;
        }
        else
        {
            vp.label = VoroVertex::OUT;
            tmp = vp.out;
        }

        // For each sample s of which p is the pole
        for (std::set<int>::iterator it = vp.attachedDelauVertices.begin();
             it != vp.attachedDelauVertices.end();
             ++it)

        {
            const VoroCell& cell = voroData->voroCells.at(*it);

            int q = cell.negativePole;

            if (voroData->voroVertices[q].label == VoroVertex::BAD_POLE ||
                voroData->voroVertices[q].label != VoroVertex::UNDEFINED)
                continue;

            if (q == -1)
                continue;

            // Update the other pole opposite label of p
            VoroVertex& vq = voroData->voroVertices.at(q);
            if (vp.label == VoroVertex::IN)
            {
                vq.out = std::max(-tmp * cell.cosBeta, vq.out);
            }
            else if (vp.label == VoroVertex::OUT)
            {
                vq.in = std::max(-tmp * cell.cosBeta, vq.in);
            }
            else
            {
#ifdef MI_DEBUG
                std::cerr << "Error in labelVoronoiPoles()." << std::endl;
#endif
                return;
            }
            vq.update();
        }

        //For each deeply intersecting neighbouring pole q
        for (std::list< std::pair<int, double> >::iterator it =
                vp.deeplyIntersectNeighPoles.begin();
             it != vp.deeplyIntersectNeighPoles.end();
             ++it)
        {

            // Update the other pole same label
            VoroVertex& vq = voroData->voroVertices[it->first];

            if (vq.label == VoroVertex::BAD_POLE ||
                vq.label != VoroVertex::UNDEFINED)
                continue;

            if (vp.label == VoroVertex::IN)
            {
                vq.in = std::max(-tmp * (it->second), vq.in);
            }
            else if (vp.label == VoroVertex::OUT)
            {
                vq.out = std::max(-tmp * (it->second), vq.out);
            }
            else
            {
#ifdef MI_DEBUG
                std::cerr << "Error in labelVoronoiPoles()." << std::endl;
#endif
                return;
            }
            vq.update();
        }
    }

#ifdef MI_DEBUG
    std::cerr << "Done!" << std::endl;
#endif

}

void PowercrustReconstructor::labelBadPoles(IProgress* progress)
{
    int badPoles = 0, undef = 0;

    const VoroVertex::Label IN = VoroVertex::IN;
    const VoroVertex::Label OUT = VoroVertex::OUT;
    const VoroVertex::Label UNDEFINED = VoroVertex::UNDEFINED;

    for(std::vector<VoroVertex>::iterator vert = voroData->voroVertices.begin();
        vert != voroData->voroVertices.end();
        ++vert)
    {
        if (vert->label != VoroVertex::BAD_POLE)
            continue;

        if (vert->attachedDelauVertices.empty() && vert->deeplyIntersectNeighPoles.empty())
            continue;

        VoroVertex::Label oppLabel = UNDEFINED;

        for(std::set<int>::iterator it = vert->attachedDelauVertices.begin();
            it != vert->attachedDelauVertices.end();
            ++it)
        {
            VoroCell& cell = voroData->voroCells[*it];

            VoroVertex& oppPole = voroData->voroVertices[cell.negativePole];

            if (oppPole.label == VoroVertex::BAD_POLE)
                continue;

            if (oppLabel == UNDEFINED)
            {
                oppLabel = oppPole.label;
            }
            else if (oppLabel != oppPole.label)
            {
                // Two opposite poles have different labels,
                // which is inconsistent
                oppLabel = UNDEFINED;
            }
        }

        if (oppLabel != UNDEFINED) undef++;

        // Angle representing the maximum of intersecting angles with IN
        // poles
        double angleIN = -1.0;

        // Angle representing the maximum of intersecting angles with OUT
        // poles
        double angleOUT = -1.0;

        // For each adjacent vertex
        for(std::list< std::pair<int,double> >::iterator neigh =
                vert->deeplyIntersectNeighPoles.begin();
            neigh != vert->deeplyIntersectNeighPoles.end();
            ++neigh)
        {
            if (voroData->voroVertices[neigh->first].label == IN)
                angleIN = std::max(angleIN, -neigh->second);
            if (voroData->voroVertices[neigh->first].label == OUT)
                angleOUT = std::max(angleOUT, -neigh->second);
        }

        // if there is no IN pole
        if (equals(angleIN, -1.0))
        {
            // if there is no OUT pole
            if (equals(angleIN, -1.0))
            {
                if (oppLabel == UNDEFINED)
                {
#ifdef MI_DEBUG
std::cerr << "Cannot label pole!" << std::endl;

#endif
        badPoles++;
                }
                else
                {
                    if (oppLabel == IN)
                    {
                        vert->label = OUT;
                        vert->out   = 1.0;
                    }
                    else
                    {
                        vert->label = IN;
                        vert->in   = 1.0;
                    }
                }
            }
            else if (angleOUT > 0.4)
            {
                // There are only deeply intersecting OUT poles
                vert->label = OUT;
                vert->out   = 1.0;
            }
            else
            {
                if (oppLabel == UNDEFINED)
                {
#ifdef MI_DEBUG
std::cerr << "Cannot label pole!" << std::endl;

#endif
    badPoles++;
                }
                else
                {
                    if (oppLabel == IN)
                    {
                        vert->label = OUT;
                        vert->out   = 1.0;
                    }
                    else
                    {
                        vert->label = IN;
                        vert->in   = 1.0;
                    }
                }
            }
        }
        else if (equals(angleIN, -1.0))
        {
            // There are IN poles but no OUT pole

            if (angleIN > 0.4)
            {
                // The IN poles intersect deeply
                vert->label = IN;
                vert->in    = 1.0;
            }
            else
            {
                if (oppLabel == UNDEFINED)
                {
#ifdef MI_DEBUG
    std::cerr << "Cannot label pole!" << std::endl;

#endif
        badPoles++;
                }
                else
                {
                    if (oppLabel == IN)
                    {
                        vert->label = OUT;
                        vert->out   = 1.0;
                    }
                    else
                    {
                        vert->label = IN;
                        vert->in   = 1.0;
                    }
                }
            }
        }
        else
        {
            // There are both IN and OUT poles
            if (angleIN > 0.4)
            {
                if (angleOUT > 0.4)
                {
                    // intersecting both deeply
                    if (oppLabel == UNDEFINED)
                    {
                        if (angleIN > angleOUT)
                        {
                            vert->label = IN;
                            vert->in   = 1.0;
                        }
                        else
                        {
                            vert->label = OUT;
                            vert->out   = 1.0;
                        }
                    }
                    else
                    {
                        if (oppLabel == IN)
                        {
                            vert->label = OUT;
                            vert->out   = 1.0;
                        }
                        else
                        {
                            vert->label = IN;
                            vert->in   = 1.0;
                        }
                    }
                }
                else
                {
                    // intersecting only IN poles deeply
                    vert->label = IN;
                    vert->in    = 1.0;
                }
            }
            else if (angleOUT > 0.4)
            {
                // intersecting only OUT poles deeply
                vert->label = OUT;
                vert->out   = 1.0;
            }
            else
            {
                if (oppLabel == UNDEFINED)
                {
#ifdef MI_DEBUG
std::cerr << "Cannot label pole!" << std::endl;
#endif
    badPoles++;
                }
                else
                {
                    if (oppLabel == IN)
                    {
                        vert->label = OUT;
                        vert->out   = 1.0;
                    }
                    else
                    {
                        vert->label = IN;
                        vert->in   = 1.0;
                    }
                }
            }
        }
    }
    std::cout << "nb bad poles: " << badPoles << "\n";
    badPoles = 0;
    for(std::vector<VoroVertex>::iterator vert = voroData->voroVertices.begin();
        vert != voroData->voroVertices.end();
        ++vert)
    {
        if (vert->label == VoroVertex::BAD_POLE) badPoles++;
    }
        std::cout << "undef " << undef << "\n";
}

void PowercrustReconstructor::filterDelaunayFaces(IProgress* progress)
{
    triangleMesh.vertices = voroData->delauVertices;

    for (unsigned int i = 0; i < voroData->voroEdges.size(); i++)
    {
        progress->setValue(500 + (i * 100)/voroData->voroEdges.size());

        const VoroEdge& edge = voroData->voroEdges[i];
        const VoroVertex& v1 = voroData->voroVertices[edge.v1];

        if (edge.isFinite())
        {
            const VoroVertex& v2 = voroData->voroVertices[edge.v2];

            // We only keep edges that have endpoints with opposite labels
            if (v1.label != v2.label && v1.label != VoroVertex::BAD_POLE && v2.label != VoroVertex::BAD_POLE)
            {
                IndexedTriangle tri(edge.delauTriangle.v1,
                                    edge.delauTriangle.v2,
                                    edge.delauTriangle.v3);
                triangleMesh.triangles.insert(tri);
            }
        }
        else
        {
            // We only keep rays whose finite endpoint is inside the surface.
            if (v1.label == VoroVertex::IN)
            {
                IndexedTriangle tri(edge.delauTriangle.v1,
                                    edge.delauTriangle.v2,
                                    edge.delauTriangle.v3);
                triangleMesh.triangles.insert(tri);
            }
        }
    }
}


bool ComputeIntersectAngle(Vector3d c1, double r1,
                           Vector3d c2, double r2,
                           double& cosAlpha)
{
    //Compute the first axis u
    Vector3d u  = c2 - c1;
    double d    = u.norm();
    u.normalize();

    //Test if the two spheres intersect
    if (r1 + r2 < d)
    {
        return false;
    }

    Vector3d z(0.0, 0.0, 1.0);

    //Compute the second axis
    Vector3d v = u.cross(z);

    if (v.isZero())
    {
        v = u.cross(Vector3d(0.0, 1.0, 0.0));
    }

    //Compute the intersection
    double xI = (r1*r1 - r2*r2 + d*d)/(2*d);
    double yI = sqrt(r1*r1 - xI*xI);

    Vector3d I = c1 + xI * u + yI * v ;

    cosAlpha = - ((c1-I).dot(c2-I)/((c1-I).norm() * (c2-I).norm()));

    return true;
}

