#include <cstdlib>

#include <algorithm>
#include <fstream>
#include <iostream>

#include <MeshImprimator.h>

void print(const Vector3d& point)
{
    std::cout   << point(0) << ", "
                << point(1) << ", "
                << point(2) << std::endl;
}

int main(int argc, const char* argv[])
{
    if (argc < 2)
        return EXIT_FAILURE;

    int errorCode = -1, line = -1;

    PLYReader reader;
    reader.open(argv[1]);

    reader.readHeader(&errorCode, &line);
    std::cout << "Error: " << errorCode << std::endl;
    std::cout << "Line: " << line << std::endl;

    reader.readContent(&errorCode, &line);
    std::cout << "Error: " << errorCode << std::endl;
    std::cout << "Line: " << line << std::endl;

    reader.close();

    const Ply::Object* obj = reader.getObject();
    obj->print(std::cout);

    PointCloud cloud = obj->getPointCloud();
    std::for_each(cloud.points.begin(), cloud.points.end(), print);

    return EXIT_SUCCESS;
}
