TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

### Common part ###

TOOLS_DIR = $$clean_path($$relative_path($$_PRO_FILE_PWD_, $$OUT_PWD)..)

DESTDIR = $$TOOLS_DIR/bin
EXTLIBS = $$TOOLS_DIR/../extlibs

win32 {
    LIBS += -L$$EXTLIBS/libs-mingw/x86
    INCLUDEPATH += $$EXTLIBS/include
}

LIBS += -L$$TOOLS_DIR/../Project/bin
INCLUDEPATH += $$TOOLS_DIR/../Project/include

### Project Specific ###

CONFIG(debug, debug|release) {
    LIBS += -lmeshimprimator-d
    DEFINES += MI_DEBUG _GLIBCXX_DEBUG
}

CONFIG(release, debug|release) {
    LIBS += -lmeshimprimator
}

win32 {
    LIBS += -lfreeglut -lopengl32 -lglu32
} else {
    LIBS += -lglut -lGL -lGLU
}

TARGET = plyviewer

SOURCES += \
    main.cpp



