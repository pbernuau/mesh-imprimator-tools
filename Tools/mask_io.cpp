#include "mask_io.h"

#include "tinyxml2.h"

#include <iostream>
#include <fstream>

#include <climits>

using namespace std;
using namespace tinyxml2;

void WriteMask(const MaskData::Mask& mask, const std::string& filename)
{
    ofstream file(filename.c_str());

    if (!file)
    {
        cerr << "Cannot open file <" << filename << "> for writing." << endl;
        return;
    }

    file << mask.rows() << "\n";
    file << mask.cols() << "\n";

    for (int i = 0; i < mask.rows(); i++)
    {
        for (int j = 0; j < mask.cols(); j++)
        {
            file << (mask(i, j) > 0 ? '1' : '0');
        }

        file << "\n";
    }
}

bool ReadMask(MaskData::Mask& mask, const std::string& filename)
{
    ifstream maskFile;
    int nbRows, nbCols;

    maskFile.open(filename.c_str());
    if (!maskFile)
    {
        cerr << "Cannot open " << filename << endl;
        return false;
    }

    maskFile >> nbRows >> nbCols;
    maskFile.ignore();

    mask.resize(nbRows, nbCols);
    mask.setZero();

    for(int i = 0; i < nbRows; i++)
    {
        string line;
        getline(maskFile, line);

        if ((int) line.size() < nbCols)
        {
            cerr << "Error in mask at line " << i << endl;
            return false;
        }

        for (int j = 0; j < nbCols; j++)
        {
            switch (line[j])
            {
            case '0':
                break;
            case '1':
                mask(i, j) = SCHAR_MAX;
                break;
            default:
                cerr << "Unknown character: " << int(line[j]) << endl;
            }
        }
    }

    return true;
}


list<MaskData> ReadMasksFromXml(const string& xmlFilename, bool* success)
{
    bool b;
    if (success == NULL)
        success = &b;

    list<MaskData> maskDataList;

    string path = xmlFilename;
    replace(path.begin(), path.end(), '\\', '/');

    string::size_type n = path.rfind('/');
    if (n != string::npos)
    {
        path.erase(n+1);
    }
    else
    {
        path.clear();
    }

    XMLDocument doc;
    XMLError err = doc.LoadFile(xmlFilename.c_str());

    switch (err)
    {
    case XML_SUCCESS:
        break;

    case XML_ERROR_FILE_NOT_FOUND:
    case XML_ERROR_FILE_COULD_NOT_BE_OPENED:
    case XML_ERROR_FILE_READ_ERROR:
        cerr << "The XML file cannot be read." << endl;
        *success = false;
        return maskDataList;

    default:
        cerr << "There is a syntax error in the XML file." << endl;
        *success = false;
        return maskDataList;
    }

    XMLElement* camerasElement = doc.FirstChildElement();

    for(XMLElement* cameraElement =
            camerasElement->FirstChildElement("camera");
        cameraElement != NULL;
        cameraElement = cameraElement->NextSiblingElement("camera"))
    {
        MaskData maskData;

        // Load camera matrix and save it into maskData.camera

         maskData.camera.setZero();

        XMLElement* cameraMatrixElement =
                cameraElement->FirstChildElement("camera-matrix");

        if (cameraMatrixElement == NULL)
        {
            cerr << "Missing <camera-matrix></camera-matrix> tag." << endl;
            cerr << "Ignoring camera." << endl;
            continue;
        }

        // index to loop over rows
        int i = 0;

        for(XMLElement* rowElement =
                cameraMatrixElement->FirstChildElement("row");
            rowElement != NULL && i < 3;
            rowElement = rowElement->NextSiblingElement("row"))
        {
            // index to loop over columns
            int j = 0;

            for(XMLElement* cellElement =
                    rowElement->FirstChildElement("cell");
                cellElement != NULL && j < 4;
                cellElement = cellElement->NextSiblingElement("cell"))
            {
                maskData.camera(i, j) = strtod(cellElement->GetText(), NULL);
                j++;
            }
            i++;
        }

        // Load mask and save it into maskData.mask
        XMLElement* imagePathElement =
                cameraElement->FirstChildElement("image-path");

        if (imagePathElement == NULL)
        {
            cerr << "Missing <image-path></image-path> tag." << endl;
            cerr << "Ignoring camera." << endl;
            continue;
        }

        string maskPath =  imagePathElement->GetText();

        if (!ReadMask(maskData.mask, path + maskPath))
        {
            cerr << "Error while reading the mask." << endl;
            cerr << "Ignoring camera." << endl;
            continue;
        }

        // Add maskData to maskDataList
        maskDataList.push_back(maskData);
    }

    *success = true;
    return maskDataList;
}

