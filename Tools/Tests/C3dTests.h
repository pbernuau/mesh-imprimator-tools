#ifndef C3DTESTS_H
#define C3DTESTS_H

#include <MeshImprimator.h>

#include <QtTest>

class C3dTests : public QObject
{
    Q_OBJECT

public:
    C3dTests();
private Q_SLOTS:
    void c3dEqualsT1();
    void c3dEqualsT2();
    void c3dEqualsT3();
    void c3dEqualsT4();
    void c3dEqualsT5();
    void c3dOperatorT1();
    void c3dOperatorT2();
    void c3dOperatorT3();
    void c3dOperatorT4();
    void c3dOperatorT5();
    void c3dOperatorT6();
    void c3dOperatorT7();

};

#endif // C3DTESTS_H
