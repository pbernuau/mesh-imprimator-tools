#ifndef MASKDATATESTS_H
#define MASKDATATESTS_H

#include <MeshImprimator.h>

#include <QtTest>

class MaskDataTests : public QObject
{
    Q_OBJECT

public:
    MaskDataTests();

private:
    const Vector3d v;
    int maskRows;
    int maskCols;
    MaskData maskData;
    const int margin;

private Q_SLOTS:
    void initTestCase();
    void mdContainsT1();
    void mdContainsT2();
    void mdContainsT3();
    void mdContainsT4();
    void mdContainsT5();
    void mdContainsT6();
    void mdContainsT7();

};

#endif // MASKDATATESTS_H
