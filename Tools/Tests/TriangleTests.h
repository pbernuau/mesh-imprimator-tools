#ifndef TRIANGLETESTS_H
#define TRIANGLETESTS_H

#include <MeshImprimator.h>

#include <QtTest>

class TriangleTests : public QObject
{
    Q_OBJECT
public:
    TriangleTests();
private Q_SLOTS:
    void tGetBarycenterT1();
    void tGetBarycenterT2();
    void tGetNormalT1();
    void tGetNormalT2();
};

#endif // TRIANGLETESTS_H
