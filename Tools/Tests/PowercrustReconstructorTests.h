#ifndef POWERCRUSTRECONSTRUCTORTESTS_H
#define POWERCRUSTRECONSTRUCTORTESTS_H

#include <MeshImprimator.h>
#include <MeshImprimator/Algorithms/Voronoi.h>

#include <QtTest>

class PowerCrustReconstructorTests : public QObject
{
    Q_OBJECT
public:
    PowerCrustReconstructorTests();

private:
    PowercrustReconstructor* powercrust;
    const VoroData* voroData;

private slots:
    void initTestCase();
    void cleanupTestCase();

    void testVoronoiDiagram();
    void testVoronoiPoles();
    void testPowerDiagram();
    void testDeeplyIntersectNeighPoles();
    void testLabels();
};

#endif // POWERCRUSTRECONSTRUCTORTESTS_H
