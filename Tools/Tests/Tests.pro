#-------------------------------------------------
#
# Project created by QtCreator 2014-02-14T10:53:07
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tests
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

### Common part ###

TOOLS_DIR = $$clean_path($$relative_path($$_PRO_FILE_PWD_, $$OUT_PWD)..)

DESTDIR = $$TOOLS_DIR/bin
EXTLIBS = $$TOOLS_DIR/../extlibs

win32 {
    LIBS += -L$$EXTLIBS/libs-mingw/x86
    LIBS += -L$$EXTLIBS/bin/x86
    INCLUDEPATH += $$EXTLIBS/include
}

LIBS += -L$$TOOLS_DIR/../Project/bin
INCLUDEPATH += $$TOOLS_DIR/../Project/include

### Project Specific ###

LIBS += -lmeshimprimator-d
DEFINES += MI_DEBUG _GLIBCXX_DEBUG

win32 {
    LIBS += -lfreeglut -lopengl32 -lglu32
} else {
    LIBS += -lglut -lGL -lGLU
}

DEFINES += SRCDIR=\\\"$$PWD/\\\"

SOURCES += \
    main.cpp \
    MaskDataTests.cpp \
    C3dTests.cpp \
    DelaunayReconstructorTests.cpp \
    TriangleTests.cpp \
    PowercrustReconstructorTests.cpp

HEADERS += \
    MaskDataTests.h \
    C3dTests.h \
    DelaunayReconstructorTests.h \
    TriangleTests.h \
    PowercrustReconstructorTests.h
