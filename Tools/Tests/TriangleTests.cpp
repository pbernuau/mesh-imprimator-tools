#include "TriangleTests.h"

TriangleTests::TriangleTests()
{
}

void TriangleTests::tGetBarycenterT1()
{
    Triangle t(Vector3d(0.0, 0.0, 0.0),
             Vector3d(0.0, 0.0, 0.0),
             Vector3d(0.0, 0.0, 0.0));
    Vector3d b = t.getBarycenter();
    QVERIFY(b(0) == 0.0);
    QVERIFY(b(1) == 0.0);
    QVERIFY(b(2) == 0.0);
}

void TriangleTests::tGetBarycenterT2()
{
    Triangle t(Vector3d(3.0, 0.0, 0.0),
             Vector3d(0.0, 3.0, 0.0),
             Vector3d(0.0, 0.0, 3.0));
    Vector3d b = t.getBarycenter();
    QVERIFY(b(0) == 1.0);
    QVERIFY(b(1) == 1.0);
    QVERIFY(b(2) == 1.0);
}

void TriangleTests::tGetNormalT1()
{
    Triangle t(Vector3d(0.0, 0.0, 0.0),
             Vector3d(0.0, 0.0, 0.0),
             Vector3d(0.0, 0.0, 0.0));
    Vector3d n = t.getNormal();
    QVERIFY(n(0) == 0.0);
    QVERIFY(n(1) == 0.0);
    QVERIFY(n(2) == 0.0);
}

void TriangleTests::tGetNormalT2()
{
    Triangle t(Vector3d(0.0, 0.0, 0.0),
             Vector3d(1.0, 0.0, 0.0),
             Vector3d(0.0, 1.0, 0.0));
    Vector3d n = t.getNormal();
    QVERIFY(n(0) == 0.0);
    QVERIFY(n(1) == 0.0);
    QVERIFY(n(2) == 1.0);
}
