#ifndef DELAUNAYRECONSTRUCTORTESTS_H
#define DELAUNAYRECONSTRUCTORTESTS_H

#include <MeshImprimator.h>

#include <QtTest>

class DelaunayReconstructorTests : public QObject
{
    Q_OBJECT
public:
    DelaunayReconstructorTests();

private:
    PointCloud tetrahedron;
    PointCloud cube;
    const std::list<MaskData> emptyMaskDataList;
    const int epsilon;

    bool belongsTo(const Vector3d& point, const TriangleMesh& mesh) const;

    void test1(IMeshReconstructor* reconstructor);
    void test2(IMeshReconstructor* reconstructor);
    void test3(IMeshReconstructor* reconstructor);

private slots:
    void initTestCase();
    void cleanupTestCase();

    // Section 3.x
    void drTriangulateT1();
    void drTriangulateT2();
    void drTriangulateT3();

    // Section 4.x
    void drTetrahedralizeT1();
    void drTetrahedralizeT2();
    void drTetrahedralizeT3();
};

#endif // DELAUNAYRECONSTRUCTORTESTS_H
