#include "MaskDataTests.h"
#include "C3dTests.h"
#include "DelaunayReconstructorTests.h"
#include "TriangleTests.h"
#include "PowercrustReconstructorTests.h"

#include <QtTest>

int main()
{
    MaskDataTests mdt;
    C3dTests c3dt;
    DelaunayReconstructorTests drt;
    TriangleTests tt;
    PowerCrustReconstructorTests pcrt;

    QTest::qExec(&mdt);
    QTest::qExec(&c3dt);
    QTest::qExec(&drt);
    QTest::qExec(&tt);
    QTest::qExec(&pcrt);
}
