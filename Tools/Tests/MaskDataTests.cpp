#include "MaskDataTests.h"

MaskDataTests::MaskDataTests():
    v(0, 0, 0),
    maskRows(6),
    maskCols(8),
    margin(1)
{
}

void MaskDataTests::initTestCase()
{
    // camera
    maskData.camera(0, 0) = (maskCols)/2;
    maskData.camera(0, 1) = 0;
    maskData.camera(0, 2) = 0;
    maskData.camera(0, 3) = -(maskCols)/2;
    maskData.camera(1, 0) = 0;
    maskData.camera(1, 1) = (maskRows)/2;
    maskData.camera(1, 2) = 0;
    maskData.camera(1, 3) = -(maskRows)/2;
    maskData.camera(2, 0) = 0;
    maskData.camera(2, 1) = 0;
    maskData.camera(2, 2) = 1;
    maskData.camera(2, 3) = -1;

    // mask
    maskData.mask = MaskData::Mask(maskRows, maskCols);
}

void MaskDataTests::mdContainsT1()
{
    // The area is completely inside the mask.

    /* the mask:
     * 0 0 0 0 0 0 0 0
     * 0 1 1 1 1 1 0 0
     * 0 1 1 1 1 1 0 0
     * 0 1 1 1 1 1 0 0
     * 0 1 1 1 1 1 0 0
     * 0 0 0 0 0 0 0 0
     *
     * The genrated area is the following:
     * 0 0 0 0 0 0 0 0
     * 0 0 0 0 0 0 0 0
     * 0 0 0 1 1 1 0 0
     * 0 0 0 1 1 1 0 0
     * 0 0 0 1 1 1 0 0
     * 0 0 0 0 0 0 0 0 */
    for (int i = 0; i < maskRows; i++)
    {
        for (int j = 0; j < maskCols; j++)
        {
            maskData.mask(i, j) = 0;
        }
    }
    for (int i = 1; i < 5; i++)
    {
        for (int j = 1; j < 6; j++)
        {
            maskData.mask(i, j) = 1;
        }
    }

    QVERIFY(maskData.contains(v, margin));
}

void MaskDataTests::mdContainsT2()
{
    // The area is completely outside the mask.

    /* the mask:
     * 1 1 1 1 1 1 1 1
     * 1 1 0 0 0 0 1 1
     * 1 1 0 0 0 0 1 1
     * 1 1 0 0 0 0 1 1
     * 1 1 0 0 0 0 1 1
     * 1 1 1 1 1 1 1 1
     *
     * The genrated area is the following:
     * 0 0 0 0 0 0 0 0
     * 0 0 0 0 0 0 0 0
     * 0 0 0 1 1 1 0 0
     * 0 0 0 1 1 1 0 0
     * 0 0 0 1 1 1 0 0
     * 0 0 0 0 0 0 0 0 */
    for (int i = 0; i < maskRows; i++)
    {
        for (int j = 0; j < maskCols; j++)
        {
            maskData.mask(i, j) = 1;
        }
    }
    for (int i = 1; i < 5; i++)
    {
        for (int j = 2; j < 6; j++)
        {
            maskData.mask(i, j) = 0;
        }
    }

    QVERIFY(!maskData.contains(v, margin));
}

void MaskDataTests::mdContainsT3()
{
    // The area is partly inside and partly outside the mask.

    /* the mask:
     * 0 0 0 0 0 0 0 0
     * 0 1 1 1 0 0 0 0
     * 0 1 1 1 0 0 0 0
     * 0 0 0 0 0 0 0 0
     * 0 0 0 0 0 0 0 0
     * 0 0 0 0 0 0 0 0
     *
     * The genrated area is the following:
     * 0 0 0 0 0 0 0 0
     * 0 0 0 0 0 0 0 0
     * 0 0 0 1 1 1 0 0
     * 0 0 0 1 1 1 0 0
     * 0 0 0 1 1 1 0 0
     * 0 0 0 0 0 0 0 0 */
    for (int i = 0; i < maskRows; i++)
    {
        for (int j = 0; j < maskCols; j++)
        {
            maskData.mask(i, j) = 0;
        }
    }
    for (int i = 1; i < 3; i++)
    {
        for (int j = 1; j < 4; j++)
        {
            maskData.mask(i, j) = 1;
        }
    }

    QVERIFY(maskData.contains(v, margin));
}

void MaskDataTests::mdContainsT4()
{
    // The mask is empty.

    for (int i = 0; i < maskRows; i++)
    {
        for (int j = 0; j < maskCols; j++)
        {
            maskData.mask(i, j) = 0;
        }
    }

    QVERIFY(!maskData.contains(v, margin));
}

void MaskDataTests::mdContainsT5()
{
    // The vertex is at the edge of the camera's visible area and the mask intersects this area.

    for (int i = 0; i < maskRows; i++)
    {
        for (int j = 0; j < maskCols; j++)
        {
            maskData.mask(i, j) = 0;
        }
    }
    maskData.mask(maskRows-1, 0) = 1;

    const Vector3d u(1, -1, 0);

    QVERIFY(maskData.contains(u, margin));
}

void MaskDataTests::mdContainsT6()
{
    // The vertex is at the edge of the camera's visible area and the mask is empty.

    for (int i = 0; i < maskRows; i++)
    {
        for (int j = 0; j < maskCols; j++)
        {
            maskData.mask(i, j) = 0;
        }
    }

    const Vector3d u(1, 1, 0);

    QVERIFY(!maskData.contains(u, margin));
}

void MaskDataTests::mdContainsT7()
{
    // The vertex is behind the camera and its projection is within the mask.

    /* the mask:
     * 0 0 0 0 0 0 0 0
     * 0 1 1 1 1 1 0 0
     * 0 1 1 1 1 1 0 0
     * 0 1 1 1 1 1 0 0
     * 0 1 1 1 1 1 0 0
     * 0 0 0 0 0 0 0 0
     */
    for (int i = 0; i < maskRows; i++)
    {
        for (int j = 0; j < maskCols; j++)
        {
            maskData.mask(i, j) = 0;
        }
    }
    for (int i = 1; i < 5; i++)
    {
        for (int j = 1; j < 6; j++)
        {
            maskData.mask(i, j) = 1;
        }
    }

    const Vector3d u(0, 0, -2);

    QVERIFY(!maskData.contains(u, margin));
}
