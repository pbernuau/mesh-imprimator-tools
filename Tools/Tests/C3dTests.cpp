#include "C3dTests.h"

C3dTests::C3dTests()
{
}

void C3dTests::c3dEqualsT1()
{

    Compare3d c;
    double x(1.0);
    double y(2.0);
    QVERIFY(!c.equals(x,y));
}


void C3dTests::c3dEqualsT2()
{

    Compare3d c;
    double x(1.0);
    double y(1.0);
    QVERIFY(c.equals(x,y));
}

void C3dTests::c3dEqualsT3()
{

    Compare3d c;
    double x(1.00000001);
    double y(1.00000003);
    QVERIFY(!c.equals(x,y));
}

void C3dTests::c3dEqualsT4()
{

    Compare3d c;
    double x(1.000000010);
    double y(1.000000019);
    QVERIFY(c.equals(x,y));
}

void C3dTests::c3dEqualsT5()
{

    Compare3d c;
    double x(1.000000001) ;
    double y(1.000000003);
    QVERIFY(c.equals(x,y));
}

void C3dTests::c3dOperatorT1()
{

    Compare3d c;
    Vector3d u(0.0,2.0,3.0);
    Vector3d v(1.0,4.0,1.0);
    QVERIFY(c(u,v));
}


void C3dTests::c3dOperatorT2()
{

    Compare3d c;
    Vector3d u(0.0,0.0,3.0);
    Vector3d v(0.0,1.0,1.0);
    QVERIFY(c(u,v));
}

void C3dTests::c3dOperatorT3()
{

    Compare3d c;
    Vector3d u(0.0,0.0,0.0);
    Vector3d v(0.0,0.0,1.0);
    QVERIFY(c(u,v));
}

void C3dTests::c3dOperatorT4()
{

    Compare3d c;
    Vector3d u(1.0,2.0,3.0);
    Vector3d v(0.0,4.0,1.0);
    QVERIFY(!c(u,v));
}

void C3dTests::c3dOperatorT5()
{

    Compare3d c;
    Vector3d u(0.0,1.0,3.0);
    Vector3d v(0.0,0.0,1.0);
    QVERIFY(!c(u,v));
}

void C3dTests::c3dOperatorT6()
{

    Compare3d c;
    Vector3d u(0.0,0.0,1.0);
    Vector3d v(0.0,0.0,0.0);
    QVERIFY(!c(u,v));
}

void C3dTests::c3dOperatorT7()
{

    Compare3d c;
    Vector3d u(0.0,0.0,0.0);
    Vector3d v(0.0,0.0,0.0);
    QVERIFY(!c(u,v));
}
