#include "DelaunayReconstructorTests.h"

#include <cmath>

class NullProgress : public IProgress
{
public:
    virtual void setValue(int) {}
    virtual bool wasCanceled() const { return false; }
    virtual void setRange(int, int) {}
};

class TestProgress : public IProgress
{
public:
    TestProgress() :
        setValueCalled(false),
        setRangeCalled(false)
    {
    }

    virtual void setValue(int) { setValueCalled = true; }
    virtual bool wasCanceled() const { return false; }
    virtual void setRange(int, int) { setRangeCalled = true; }

    bool setValueCalled;
    bool setRangeCalled;
};

class StopProgress : public IProgress
{
public:
    StopProgress() :
        canceled(false),
        valueSetAfterCancelation(false)
    {
    }

    virtual void setValue(int)
    {
        if (canceled)
            valueSetAfterCancelation = true;
    }

    virtual bool wasCanceled() const
    {
        canceled = true;
        return true;
    }

    virtual void setRange(int, int) {}

    mutable bool canceled;
    mutable bool valueSetAfterCancelation;
};

DelaunayReconstructorTests::DelaunayReconstructorTests() :
    epsilon(1)
{
}

bool DelaunayReconstructorTests::belongsTo(const Vector3d& point,
                                           const TriangleMesh& mesh) const
{
    for (std::list<Triangle>::const_iterator tri = mesh.triangles.begin();
         tri != mesh.triangles.end();
         ++tri)
    {
        for (int v = 0; v < 3; v++)
        {
            if (point == tri->vertex(v))
                return true;
        }
    }

    return false;
}

void DelaunayReconstructorTests::test1(IMeshReconstructor* reconstructor)
{
    NullProgress progress;
    TriangleMesh mesh = reconstructor->reconstruct(&progress);
    delete reconstructor;

    // No point has been created
    for (std::list<Triangle>::const_iterator it = mesh.triangles.begin();
         it != mesh.triangles.end();
         ++it)
    {
        for (int i = 0; i < 3; i++)
        {
            QVERIFY2(tetrahedron.points.count(it->vertex(i)) == 1,
                     "A point has been created.");
        }
    }

    // Each point belongs to a face
    for (PointCloud::Container::const_iterator pt = tetrahedron.points.begin();
         pt != tetrahedron.points.end();
         ++pt)
    {
        QVERIFY2(belongsTo(*pt, mesh),
                 "A point does not belong to a face.");
    }

    // One tetrahedron has been created
    QCOMPARE(mesh.triangles.size(), (unsigned int)4);
}

void DelaunayReconstructorTests::test2(IMeshReconstructor* reconstructor)
{
    TestProgress progress;
    TriangleMesh mesh = reconstructor->reconstruct(&progress);
    delete reconstructor;

    // There is no face with twice the same point
    for (std::list<Triangle>::const_iterator it = mesh.triangles.begin();
         it != mesh.triangles.end();
         ++it)
    {
        const Vector3d& v1 = it->vertex(0);
        const Vector3d& v2 = it->vertex(1);
        const Vector3d& v3 = it->vertex(2);

        QVERIFY2(!v1.isApprox(v2), "Invalid face.");
        QVERIFY2(!v1.isApprox(v3), "Invalid face.");
        QVERIFY2(!v2.isApprox(v3), "Invalid face.");
    }

    // There are no identical triangles
    for (std::list<Triangle>::const_iterator it = mesh.triangles.begin();
         it != mesh.triangles.end();
         ++it)
    {
        for (std::list<Triangle>::const_iterator it2 = mesh.triangles.begin();
             it2 != mesh.triangles.end();
             ++it2)
        {
            if (it == it2)
                continue;

            // we add vertices of both triangles to a set
            // the set should have more than 3 elements

            std::set<Vector3d, Compare3d> vertices;

            vertices.insert(it->vertex(0));
            vertices.insert(it->vertex(1));
            vertices.insert(it->vertex(2));
            vertices.insert(it2->vertex(0));
            vertices.insert(it2->vertex(1));
            vertices.insert(it2->vertex(2));

            QVERIFY2(vertices.size() > 3, "Identical triangles detected.");
        }
    }

    // progress is displayed
    QVERIFY2(progress.setValueCalled, "Progress value not set.");
    QVERIFY2(progress.setRangeCalled, "Progress range not set.");

    // 12 triangles have been created
    QCOMPARE(mesh.triangles.size(), (unsigned int)12);
}

void DelaunayReconstructorTests::test3(IMeshReconstructor* reconstructor)
{
    StopProgress progress;
    reconstructor->reconstruct(&progress);
    delete reconstructor;

    QVERIFY2(progress.canceled && !progress.valueSetAfterCancelation,
             "The reconstruction did not stop.");
}

void DelaunayReconstructorTests::initTestCase()
{
    // init tetrahedron
    const double a = sqrt(2.0 / 3.0);
    const double b = 1.0 / (2.0 * sqrt(6.0));
    const double c = 1.0 / (2.0 * sqrt(3.0));
    const double d = 1.0 / sqrt(3.0);

    tetrahedron.points.insert(Vector3d(0.0, 0.0, a - b));
    tetrahedron.points.insert(Vector3d(-c, -0.5, -b));
    tetrahedron.points.insert(Vector3d(-c, +0.5, -b));
    tetrahedron.points.insert(Vector3d(+d, -0.5, -b));

    // init cube
    cube.points.insert(Vector3d(0.0, 0.0, 0.0));
    cube.points.insert(Vector3d(1.0, 0.0, 0.0));
    cube.points.insert(Vector3d(1.0, 1.0, 0.0));
    cube.points.insert(Vector3d(0.0, 1.0, 0.0));
    cube.points.insert(Vector3d(0.0, 0.0, 1.0));
    cube.points.insert(Vector3d(1.0, 0.0, 1.0));
    cube.points.insert(Vector3d(1.0, 1.0, 1.0));
    cube.points.insert(Vector3d(0.0, 1.0, 1.0));
}

void DelaunayReconstructorTests::cleanupTestCase()
{
    tetrahedron.points.clear();
    cube.points.clear();
}



//==============================================================================

// ref: DR_TRIANGULATE_T1
// input: a point cloud (PointCloud)
// description: a point cloud of 4 points (a tetrahedron) is triangulated
// expected result: no point has been created; each point belongs to a face;
// 1 tetrahedron has been created
void DelaunayReconstructorTests::drTriangulateT1()
{
    IMeshReconstructor* reconstructor = new DelaunayReconstructor(
                tetrahedron,
                emptyMaskDataList,
                epsilon,
                DelaunayReconstructor::TRIANGULATE);

    test1(reconstructor);
}

// ref: DR_TRIANGULATE_T2
// input: a point cloud (PointCloud)
// description: a point cloud of 8 points representing a (unit) cube is
// triangulated
// expected result: there is no face with twice the same point; there are no
// identical triangles; progress is displayed
void DelaunayReconstructorTests::drTriangulateT2()
{
    IMeshReconstructor* reconstructor = new DelaunayReconstructor(
                cube,
                emptyMaskDataList,
                epsilon,
                DelaunayReconstructor::TRIANGULATE);

    test2(reconstructor);
}

// ref: DR_TRIANGULATE_T3
// input: a point cloud (PointCloud)
// description: a point cloud of 8 points representing a (unit) cube is
// triangulated. The execution is canceled (via Progress) at the beginning of
// the program.
// expected result: the triangulate method terminates and the progress bar stops
void DelaunayReconstructorTests::drTriangulateT3()
{
    IMeshReconstructor* reconstructor = new DelaunayReconstructor(
                cube,
                emptyMaskDataList,
                epsilon,
                DelaunayReconstructor::TRIANGULATE);

    test3(reconstructor);
}

//==============================================================================

// ref: DR_TETRAHEDRALIZE_T1
// input: a point cloud (PointCloud)
// description: a point cloud of 4 points (a tetrahedron) is triangulated
// expected result: no point has been created; each point belongs to a face;
// 1 tetrahedron has been created
void DelaunayReconstructorTests::drTetrahedralizeT1()
{
    IMeshReconstructor* reconstructor = new DelaunayReconstructor(
                tetrahedron,
                emptyMaskDataList,
                epsilon,
                DelaunayReconstructor::TETRAHEDRALIZE);

    test1(reconstructor);
}

// ref: DR_TETRAHEDRALIZE_T2
// input: a point cloud (PointCloud)
// description: a point cloud of 8 points representing a (unit) cube is
// triangulated
// expected result: there is no face with twice the same point; there are no
// identical triangles; progress is displayed
void DelaunayReconstructorTests::drTetrahedralizeT2()
{
    IMeshReconstructor* reconstructor = new DelaunayReconstructor(
                cube,
                emptyMaskDataList,
                epsilon,
                DelaunayReconstructor::TETRAHEDRALIZE);

    test2(reconstructor);
}

// ref: DR_TETRAHEDRALIZE_T3
// input: a point cloud (PointCloud)
// description: a point cloud of 8 points representing a (unit) cube is
// triangulated. The execution is canceled (via Progress).
// expected result: the triangulate method terminates and the progress bar stops
void DelaunayReconstructorTests::drTetrahedralizeT3()
{
    IMeshReconstructor* reconstructor = new DelaunayReconstructor(
                cube,
                emptyMaskDataList,
                epsilon,
                DelaunayReconstructor::TETRAHEDRALIZE);

    test3(reconstructor);
}
