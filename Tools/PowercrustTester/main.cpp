#include "VoroViewer.h"

#include <MeshImprimator.h>

#include <iostream>
#include <vector>

#include <cstdlib>

const int WIDTH = 800;
const int HEIGHT = 600;

using namespace std;

class SimpleProgress : public IProgress
{
public:
    SimpleProgress() : _min(0), _max(100), _value(0)
    {
    }

    virtual ~SimpleProgress()
    {
    }

    virtual void setValue(int value)
    {
        _value = value;

        int percent = 100 * static_cast<float>(value - _min) / (_max - _min);
        std::cerr << "\r" << percent << " %";
    }

    virtual bool wasCanceled() const
    {
        return false;
    }

    virtual void setRange(int min, int max)
    {
        _min = min;
        _max = max;
    }

private:
    int _min, _max, _value;
};

void prompt()
{
    string tmp;
    cerr << "Press any key to continue..." << std::endl;
    getline(std::cin, tmp);
}

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        cerr << "Missing PLY file." << endl;
        return EXIT_FAILURE;
    }

    PLYReader reader;
    reader.open(argv[1]);

    int errorCode, line;

    if (!reader.readHeader(&errorCode, &line))
    {
        std::cerr << "Error: " << errorCode << std::endl;
        std::cerr << "Line: " << line << std::endl;

        return EXIT_FAILURE;
    }

    std::cerr << "Header successfully read." << std::endl;

    if (!reader.readContent(&errorCode, &line))
    {
        std::cerr << "Error: " << errorCode << std::endl;
        std::cerr << "Line: " << line << std::endl;

        return EXIT_FAILURE;
    }

    std::cerr << "Content successfully read." << std::endl;

    reader.close();

    const Ply::Object* obj = reader.getObject();
    obj->print(std::cerr);

    PointCloud cloud = obj->getPointCloud();


    prompt();

    //==========================================================================

    IProgress* progress = new SimpleProgress();

    PowercrustReconstructor pc(cloud);
    TriangleMesh triangleMesh = pc.reconstruct(progress);

    delete progress;

    std::cerr << "I reconstructed " << triangleMesh.triangles.size()
              << " triangles." << std::endl;

    //==========================================================================

    GlutWindow::Init(argc, argv);

    VoroViewer* viewer = new VoroViewer("Voronoi Viewer", WIDTH, HEIGHT);
    viewer->setVoronoiData(pc.debugVoroData());


    GlutWindow::Loop();

    delete viewer;

    return EXIT_SUCCESS;
}
