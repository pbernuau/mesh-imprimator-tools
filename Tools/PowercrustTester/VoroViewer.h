#ifndef VOROVIEWER_H
#define VOROVIEWER_H

#include <MeshImprimator.h>
#include <MeshImprimator/Algorithms/Voronoi.h>

#include "../GlutWindow.h"

#include <string>
#include <vector>
#include <cstdio>

class VoroViewer : public GlutWindow
{
public:
    VoroViewer(const std::string& title, int width, int height);
    virtual ~VoroViewer();

    void setVoronoiData(const VoroData* data);

protected:
    virtual void display(void);
    virtual void reshape(int width, int height);
    virtual void keyboard(unsigned char key, int x, int y);
    virtual void special(int key, int x, int y);

private:
    double fieldOfView, zNear, zFar;
    double angleX, angleZ, cameraDistance;

    const VoroData* data;
    std::vector<Vector3d> colors;

    Vector3d point;

    void initOpenGL();
    void setupProjection(int width, int height);
};


#endif // VOROVIEWER_H
