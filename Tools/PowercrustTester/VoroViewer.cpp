#include "VoroViewer.h"

const double DELTA_ANGLE_X  = 5;
const double DELTA_ANGLE_Y  = 5;
const double DELTA_DISTANCE = 0.3;
const double DISTANCE_MIN   = 0.0;

VoroViewer::VoroViewer(const std::string& title, int width, int height) :
    GlutWindow("VoroViewer", title.c_str(), width, height),
    fieldOfView(45),
    zNear(1),
    zFar(500),
    angleX(-60),
    angleZ(30),
    cameraDistance(25),
    data(NULL),
    point(0.0, 0.0, 0.0)
{
    initOpenGL();
    setupProjection(width, height);
}

VoroViewer::~VoroViewer()
{
}


void VoroViewer::setVoronoiData(const VoroData* data)
{
    this->data = data;
    colors.clear();

    int size = data->voroCells.size();
    colors.resize(size);
    for (int i = 0; i < size; i++)
    {
        colors[i] = (Vector3d::Random() + Vector3d(1.0, 1.0, 1.0)) / 2.0;
    }

}

void VoroViewer::display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glPushMatrix();
    gluLookAt(0, cameraDistance, 0,
              0, 0, 0,
              0, 0, 1);

    glRotated(angleX, 1, 0, 0);
    glRotated(angleZ, 0, 0, 1);


    for (unsigned int i = 0; i < data->voroCells.size(); i++)
    {
        const VoroCell* cell = &data->voroCells[i];
        Vector3d p = data->delauVertices[cell->delauVertex];

        glColor3d(colors[i](0), colors[i](1), colors[i](2));

        glPushMatrix();
        glTranslated(p(0), p(1), p(2));
        glutWireSphere(0.1, 10, 10);
        glPopMatrix();

        //glColor3d(1, 1, 1);

        for (std::set<int>::iterator it = cell->voroEdges.begin();
             it != cell->voroEdges.end();
             ++it)
        {
            const VoroEdge* edge = &data->voroEdges[*it];

            Vector3d v1, v2;

            v1 = data->voroVertices[edge->v1].position;
            v2 = edge->isFinite() ?
                        data->voroVertices[edge->v2].position :
                    v1 + edge->direction * 9999.0;

            glBegin(GL_LINES);
            glVertex3d(v1(0), v1(1), v1(2));
            glVertex3d(v2(0), v2(1), v2(2));
            glEnd();
        }

    }

/*
 *         glBegin(GL_LINES);
        glColor3f(1, 0, 0); glVertex3f(0, 0, 0); glVertex3f(50, 0, 0);
        glColor3f(0, 1, 0); glVertex3f(0, 0, 0); glVertex3f(0, 50, 0);
        glColor3f(0, 0, 1); glVertex3f(0, 0, 0); glVertex3f(0, 0, 50);
        glEnd();

        if (maskData.contains(point, 1))
            glColor3f(0, 1, 0);
        else
            glColor3f(1, 0, 0);

        glBegin(GL_POINTS);
        glVertex3f(point(0), point(1), point(2));
        glEnd();
        */

    glPopMatrix();

    glutSwapBuffers();
}

void VoroViewer::reshape(int width, int height)
{
    setupProjection(width, height);
    postRedisplay();
}

void VoroViewer::keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
    case '4':
        point(0) -= 0.1;
        break;
    case '6':
        point(0) += 0.1;
        break;
    case '8':
        point(1) += 0.1;
        break;
    case '2':
        point(1) -= 0.1;
        break;
    case '9':
        point(2) += 0.1;
        break;
    case '3':
        point(2) -= 0.1;
        break;
    default:
        GlutWindow::keyboard(key, x, y);
        return;
    }

    postRedisplay();
}

void VoroViewer::special(int key, int x, int y)
{
    switch (key)
    {
    case GLUT_KEY_UP:
        angleX -= DELTA_ANGLE_X;
        break;
    case GLUT_KEY_DOWN:
        angleX += DELTA_ANGLE_X;
        break;
    case GLUT_KEY_LEFT:
        angleZ += DELTA_ANGLE_Y;
        break;
    case GLUT_KEY_RIGHT:
        angleZ -= DELTA_ANGLE_Y;
        break;
    case GLUT_KEY_PAGE_DOWN:
        cameraDistance += DELTA_DISTANCE;
        break;
    case GLUT_KEY_PAGE_UP:
        cameraDistance -= DELTA_DISTANCE;
        cameraDistance = std::max(DISTANCE_MIN, cameraDistance);
        break;
    default:
        GlutWindow::special(key, x, y);
    }

    postRedisplay();
}

void VoroViewer::initOpenGL()
{
    // setup options
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glEnable(GL_CULL_FACE);

    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glPointSize(5.f);

    // setup lighting
    GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat mat_shininess[] = { 100.0 };
    GLfloat light0_position[] = { 0.0, 0.0, -1.0, 0.0 };
    GLfloat light1_position[] = { 0.0, 1.0, 0.0, 0.0 };
    GLfloat light2_position[] = { 1.0, 0.0, 5.0, 0.0 };
    glShadeModel(GL_SMOOTH);

    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
    glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
    glLightfv(GL_LIGHT0, GL_POSITION, light1_position);
    glLightfv(GL_LIGHT0, GL_POSITION, light2_position);

    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);
}

void VoroViewer::setupProjection(int width, int height)
{
    glViewport(0, 0, width, height);

    double aspect = static_cast<double>(width) / height;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(fieldOfView, aspect, zNear, zFar);
}
