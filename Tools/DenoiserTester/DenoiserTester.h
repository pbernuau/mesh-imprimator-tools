#ifndef DENOISERTESTER_H
#define DENOISERTESTER_H

#include <MeshImprimator.h>

/***************************************************************************
  Simple Class to load and draw 3D objects from OBJ files
  Using triangles and normals as static object. No texture mapping.
  OBJ files must be triangulated!!!
 ***************************************************************************/

#define VERTICES_PER_TRIANGLE 3
#define COORD_PER_VERTEX 3
#define TOTAL_FLOATS_IN_TRIANGLE (VERTICES_PER_TRIANGLE*COORD_PER_VERTEX)

class ObjModel
{
public:
    ObjModel();

    /**
     * Calculate the normal of a triangular face defined by three points
     *
     * @param[in] coord1 the first vertex
     * @param[in] coord2 the second vertex
     * @param[in] coord3 the third vertex
     * @param[out] norm the normal
     */
    void computeNormal(const float coord1[3], const float coord2[3], const float coord3[3], float norm[3] );

    /**
     *  Loads the model from file
     * @param filename the OBJ file
     * @return
     */
    int load(const char *filename);

    /**
     * Draws the model with the opengl primitives
     */
    void draw(bool wireframe);

    IndexedTriangleMesh mesh;   // Stores the indexed mesh
    long _numVertices;          // the actual number of loaded vertices
    long _numTriangles;         // the actual number of loaded faces


private:
    /**
     * Perform a first scan of the file in order to get the number of vertices and the number of faces
     * @param filename
     * @param vertexNum
     * @param faceNum
     */
    void firstScan(const char* filename, long &vertexNum, long &faceNum);
};

bool loadModel(const std::string& filename, ObjModel &model);

#endif // DENOISERTESTER_H
