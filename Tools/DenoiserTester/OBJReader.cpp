#include <cstdio>

#include <iostream>
#include <string>

#include "DenoiserTester.h"

using namespace std;

/*============================================================================
 * ObjModel
 * ===========================================================================*/

ObjModel::ObjModel()
{
    _numVertices = 0;
    _numTriangles = 0;
}

/**
 * Perform a first scan of the file in order to get the number of vertices and the number of faces
 * @param[in] filename the OBJ file
 * @param[out] vertexNum the number of vertices found
 * @param[out] triangleNum the number of faces found
 */
void ObjModel::firstScan(const char* filename, long &vertexNum, long &triangleNum)
{
    string line;
    ifstream objFile (filename);
    // If obj file is open, continue
    if (objFile.is_open())
    {
        vertexNum = 0;
        triangleNum = 0;

        // Start reading file data
        while (! objFile.eof() )
        {
            //**************************************************
            // get a line of the file (use getline )
            //**************************************************
            getline(objFile, line);

            if (line.empty())
                continue;

            //**************************************************
            // If the first character is a 'v'...
            //**************************************************
            if (line[0] == 'v')
            {
                //**************************************************
                // Increment the number of vertices
                //**************************************************
                vertexNum++;

            }
            //**************************************************
            // If the first character is a 'f' ...
            //**************************************************
            else if (line[0] == 'f')
            {
                //**************************************************
                // Increment the number of triangles
                //**************************************************
                triangleNum++;
            }
        }

        // Close OBJ file
        objFile.close();
    }
    else
    {
        cerr << "Unable to open file" << endl;
    }
}

/**
 * @brief
 * @param filename
 * @return
 */
int ObjModel::load(const char* filename)
{
    // Count the number of vertices and the number of triangles
    firstScan(filename, _numVertices, _numTriangles);

    cerr << "Found object with "<< _numVertices << " vertices and "
         << _numTriangles << " faces" << endl;

    string line;
    ifstream objFile (filename);

    // If obj file is open, continue
    if (objFile.is_open())
    {
        // This index is used to run through the triangle array
        int triangle_index = 0;
        // This index is used to run through the vertex array
        int vertex_index = 0;

        // Start reading file data
        while (! objFile.eof() )
        {
            //**************************************************
            // Get a line from file
            //**************************************************
            getline(objFile, line);

            if (line.empty())
                continue;

            istringstream stream(line);
            stream.ignore(1);

            //**************************************************
            // If the first character is a 'v'...
            //**************************************************
            if (line[0] == 'v')
            {
                //**************************************************
                // Read 3 floats from the line:  X Y Z and store them in the corresponding place in _vertices
                // In order to read the floats in one shot from string you can use sscanf
                //**************************************************
                double x, y, z; // TODO: or float?
                stream >> x;
                stream >> y;
                stream >> z;
                mesh.vertices.push_back(Vector3d(x, y, z));

                // update the vertex
                vertex_index ++;

                // just a security check...
                assert((vertex_index <= _numVertices));
            }
            //**************************************************
            // If the first character is an 'f'...
            //**************************************************
            else if (line[0] == 'f')
            {
                // this contains temporary the indices of the vertices
                int vertexIdx[3] = { 0, 0, 0 };

                //**************************************************
                // Read 3 integers from the line:  idx1 idx2 idx3 and store them in the corresponding place in vertexIdx
                // In order to read the 3 integers in one shot from string you can use sscanf
                //**************************************************
                stream >> vertexIdx[0];
                stream >> vertexIdx[1];
                stream >> vertexIdx[2];

                //**************************************************
                // correct the indices: OBJ starts counting from 1, in C the arrays starts at 0...
                //**************************************************
                vertexIdx[0]--;
                vertexIdx[1]--;
                vertexIdx[2]--;

                //just a security check
                assert((vertexIdx[0] >= 0 )&&(vertexIdx[0] <= _numVertices));
                assert((vertexIdx[1] >= 0 )&&(vertexIdx[1] <= _numVertices));
                assert((vertexIdx[2] >= 0 )&&(vertexIdx[2] <= _numVertices));

                //*********************************************************************
                //  fill the _triangles array with the 3 vertices
                // tCounter gives you the starting position of each vertex in _triangles
                //*********************************************************************
                IndexedTriangle triangle(vertexIdx[0], vertexIdx[1], vertexIdx[2]);
                mesh.triangles.insert(triangle);

                // update the indices
                triangle_index += TOTAL_FLOATS_IN_TRIANGLE;

                // just a security check
                assert((triangle_index <= _numTriangles*TOTAL_FLOATS_IN_TRIANGLE));
            }
        }
        cerr << "TotalConnectedTriangles "<< triangle_index << endl;

        assert( _numTriangles == triangle_index/TOTAL_FLOATS_IN_TRIANGLE );
        assert( _numVertices == vertex_index );

        // Close OBJ file
        objFile.close();
    }
    else
    {
        cerr << "Unable to open file" << endl;
        return -1;
    }

    cerr << "Object loaded with "<< _numVertices << " vertices and "  << _numTriangles << " faces" << endl;
    return 0;
}

/*============================================================================
 * END ObjModel
 * ===========================================================================*/

// Load the obj model from file
bool loadModel(const std::string &filename, ObjModel &model)
{
    int ret = model.load(filename.c_str());
    if (ret != 0)
    {
        // Loading failed
        cerr << "Loading of .OBJ model failed with code " << ret << endl;
        return false;
    }
    return true;
}
