#include <cstdlib>
#include <iostream>
#include <time.h>

#include "DenoiserTester.h"

const int WIDTH = 800;
const int HEIGHT = 600;

using namespace std;

class SimpleProgress : public IProgress
{
public:
    SimpleProgress() : _min(0), _max(100), _value(0)
    {
    }

    virtual ~SimpleProgress()
    {
    }

    virtual void setValue(int value)
    {
        _value = value;

        int percent = 100 * static_cast<float>(value - _min) / (_max - _min);
        std::cerr << "\r" << percent << " %";
    }

    virtual bool wasCanceled() const
    {
        return false;
    }

    virtual void setRange(int min, int max)
    {
        _min = min;
        _max = max;
    }

private:
    int _min, _max, _value;
};

int main(int argc, char* argv[])
{
    if (argc < 3)
    {
        cerr << "Usage:\n"
             << "\tobjviewer <obj-file>\n"
             << "\n"
             << "\t obj-file: a path to a .OBJ file\n"
             << "\t sdl-folder: a path to save the .STL denoised model\n"
             << "\n"
             << "Example:\n"
             << argv[0] << " models/suzanne.obj out.stl\n";

        return EXIT_FAILURE;
    }

    // Load the obj model from file
    ObjModel model;
    if (!loadModel(argv[1], model))
        return EXIT_FAILURE;

    // Denoise mesh
    FeaturePreservingDenoiser denoiser(model.mesh);
    IProgress* progress = new SimpleProgress();

    clock_t init, final;
    init = clock();

    TriangleMesh denoisedMesh = denoiser.denoise(progress);
    std::cout << std::endl;

    final = clock() - init;
    std::cout << "Execution time: " << (double)final / ((double)CLOCKS_PER_SEC)
              << "\n";

    assert(model.mesh.triangles.size() == denoisedMesh.triangles.size());

    // Output an SDL file
    STLWriter writer;
    writer.write(denoisedMesh, argv[2], argv[2]);

    cerr << endl << "Bye." << endl;

    return EXIT_SUCCESS;
}
