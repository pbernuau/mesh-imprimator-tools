#include <climits>

#include <algorithm>
#include <exception>
#include <iostream>
#include <fstream>
#include <sstream>

#include "ObjViewer.h"
#include "../mask_io.h"

using namespace Eigen;

const double DELTA_ANGLE_X  = 5;
const double DELTA_ANGLE_Y  = 5;
const double DELTA_DISTANCE = 0.3;
const double DISTANCE_MIN   = 0.0;

ObjViewer::ObjViewer(const std::string& title, int width, int height) :
    GlutWindow("ObjViewer", title.c_str(), width, height),
    fieldOfView(45),
    zNear(1),
    zFar(500),
    angleX(-60),
    angleZ(30),
    cameraDistance(25),
    model(NULL),
    maskIndex(0),
    point(0.0, 0.0, 0.0),
    wireframe(false),
    capture(false),
    save(false),
    printer(NULL)
{
    initOpenGL();
    setupProjection(width, height);
    setTargetMaskFolder(std::string(), std::string());

    //points.reserve(1000);
    //for (int i = 0; i < 1000; i++)
    //{
    //    points.push_back(20.0 * Vector3d::Random());
    //}
}

ObjViewer::~ObjViewer()
{
    if (model != NULL)
        delete model;

    if (printer != NULL)
    {
        printer->CloseElement();
        delete printer;
    }

    model = NULL;
    printer = NULL;
}

bool ObjViewer::loadModel(const std::string& filename)
{
    if (model != NULL)
        delete model;

    model = new ObjModel();
    if (model->load(filename.c_str()) != 0)
    {
        delete model;
        model = NULL;
        return false;
    }

    std::string clean = filename;
    std::replace(clean.begin(), clean.end(), '\\', '/');

    std::string::size_type pos = clean.rfind("/");
    if (pos == std::string::npos)
        pos = 0;
    modelName = clean.substr(pos + 1);

    return true;
}

bool ObjViewer::setTargetXmlFile(FILE* file)
{
    if (printer != NULL)
    {
        printer->CloseElement();
        delete printer;
    }

    printer = new XMLPrinter(file);
    printer->PushHeader(false, true);
    printer->OpenElement("cameras");
    return true;
}

bool ObjViewer::setTargetMaskFolder(const std::string& folder,
                                    const std::string& path)
{
    maskFolder = folder.empty() ? "." : folder;
    maskPath = path.empty() ? "." : path;

    return true;
}

void ObjViewer::display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glPushMatrix();
    gluLookAt(0, cameraDistance, 0,
              0, 0, 0,
              0, 0, 1);

    glRotated(angleX, 1, 0, 0);
    glRotated(angleZ, 0, 0, 1);

    if (!capture)
    {
        glBegin(GL_LINES);
        glColor3f(1, 0, 0); glVertex3f(0, 0, 0); glVertex3f(50, 0, 0);
        glColor3f(0, 1, 0); glVertex3f(0, 0, 0); glVertex3f(0, 50, 0);
        glColor3f(0, 0, 1); glVertex3f(0, 0, 0); glVertex3f(0, 0, 50);
        glEnd();

        if (maskData.contains(point, 1))
            glColor3f(0, 1, 0);
        else
            glColor3f(1, 0, 0);

        glBegin(GL_POINTS);
        glVertex3f(point(0), point(1), point(2));
        glEnd();

        //glBegin(GL_POINTS);
        //for (int i = 0; i < points.size(); i++)
        //{
        //    const Vector3d& point = points.at(i);
        //
        //    if (maskData.isInside(point, 1))
        //        glColor3f(0, 1, 0);
        //    else
        //        glColor3f(1, 0, 0);
        //
        //
        //    glVertex3f(point(0), point(1), point(2));
        //}
        //glEnd();
    }

    if (capture)
        saveCamera();

    // draw the model
    if (model != NULL)
    {
        if (!capture && wireframe)
        {
            model->draw(true);
        }
        else
        {
            glEnable(GL_LIGHTING);
            model->draw(false);
            glDisable(GL_LIGHTING);
        }
    }

    glPopMatrix();

    glutSwapBuffers();

    if (capture)
    {
        saveMask();
        capture = false;
        postRedisplay();
    }

    if (save && printer != NULL)
    {
        write();
        save = false;
    }
}

void ObjViewer::reshape(int width, int height)
{
    setupProjection(width, height);
    postRedisplay();
}

void ObjViewer::keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 'c':
        capture = true;
        break;
    case 'm':
        wireframe = !wireframe;
        break;
    case '4':
        point(0) -= 0.1;
        break;
    case '6':
        point(0) += 0.1;
        break;
    case '8':
        point(1) += 0.1;
        break;
    case '2':
        point(1) -= 0.1;
        break;
    case '9':
        point(2) += 0.1;
        break;
    case '3':
        point(2) -= 0.1;
        break;
    case ' ':
        capture = true;
        save = true;
        break;
    default:
        GlutWindow::keyboard(key, x, y);
        return;
    }

    postRedisplay();
}

void ObjViewer::special(int key, int x, int y)
{
    switch (key)
    {
    case GLUT_KEY_UP:
        angleX -= DELTA_ANGLE_X;
        break;
    case GLUT_KEY_DOWN:
        angleX += DELTA_ANGLE_X;
        break;
    case GLUT_KEY_LEFT:
        angleZ += DELTA_ANGLE_Y;
        break;
    case GLUT_KEY_RIGHT:
        angleZ -= DELTA_ANGLE_Y;
        break;
    case GLUT_KEY_PAGE_DOWN:
        cameraDistance += DELTA_DISTANCE;
        break;
    case GLUT_KEY_PAGE_UP:
        cameraDistance -= DELTA_DISTANCE;
        cameraDistance = std::max(DISTANCE_MIN, cameraDistance);
        break;
    default:
        GlutWindow::special(key, x, y);
    }

    postRedisplay();
}

void ObjViewer::initOpenGL()
{
    // setup options
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glEnable(GL_CULL_FACE);

    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glPointSize(5.f);

    // setup lighting
    GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat mat_shininess[] = { 100.0 };
    GLfloat light0_position[] = { 0.0, 0.0, -1.0, 0.0 };
    GLfloat light1_position[] = { 0.0, 1.0, 0.0, 0.0 };
    GLfloat light2_position[] = { 1.0, 0.0, 5.0, 0.0 };
    glShadeModel(GL_SMOOTH);

    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
    glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
    glLightfv(GL_LIGHT0, GL_POSITION, light1_position);
    glLightfv(GL_LIGHT0, GL_POSITION, light2_position);

    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);
}

void ObjViewer::setupProjection(int width, int height)
{
    glViewport(0, 0, width, height);

    double aspect = static_cast<double>(width) / height;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(fieldOfView, aspect, zNear, zFar);
}

void ObjViewer::saveCamera()
{
    GLdouble buffer[16];

    MatrixXd calibration(3, 4);
    MatrixXd projection(4, 4);
    MatrixXd modelView(4, 4);

    // calibration
    int width = getWidth();
    int height = getHeight();

    calibration.setZero();

    calibration(0, 0) = width / 2;
    calibration(1, 1) = height / 2;
    calibration(0, 3) = width / 2;
    calibration(1, 3) = height / 2;
    calibration(2, 3) = 1;

    // projection
    glGetDoublev(GL_PROJECTION_MATRIX, buffer);

    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            projection(i, j) = buffer[j*4 + i];
        }
    }

    // model view
    glGetDoublev(GL_MODELVIEW_MATRIX, buffer);

    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            modelView(i, j) = buffer[j*4 + i];
        }
    }

    // Compute result
    maskData.camera = calibration * projection * modelView;
}

void ObjViewer::saveMask()
{
    int width = getWidth();
    int height = getHeight();

    GLfloat* buffer = new GLfloat[width * height];
    glReadPixels(0, 0, width, height, GL_RED, GL_FLOAT, buffer);

    maskData.mask.resize(height, width);

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            maskData.mask(i, j) =
                    buffer[i*width + j] > 0.f ? SCHAR_MAX : 0;
        }
    }

    delete buffer;
}

void ObjViewer::write()
{
    if (printer == NULL)
        return;

    std::ostringstream iss;
    iss << modelName << "." << maskIndex << ".mask";
    std::string name = iss.str();

    maskIndex++;

    writeMaskData(maskFolder + "/" + name);

    printer->OpenElement("camera");

    // write camera matrix
    std::cerr << "Camera: " << std::endl << maskData.camera << std::endl;

    printer->OpenElement("camera-matrix");
    for (int i = 0; i < 3; i++)
    {
        printer->OpenElement("row");
        for (int j = 0; j < 4; j++)
        {
            printer->OpenElement("cell");
            printer->PushText(maskData.camera(i, j));
            printer->CloseElement();
        }
        printer->CloseElement();
    }
    printer->CloseElement();

    // write mask path
    std::string path = maskPath + "/" + name;
    printer->OpenElement("image-path");
    printer->PushText(path.c_str());
    printer->CloseElement();

    printer->CloseElement();
}

void ObjViewer::writeMaskData(const std::string& filename)
{
    WriteMask(maskData.mask, filename);
}
