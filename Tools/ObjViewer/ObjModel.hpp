/***************************************************************************
  Simple Class to load and draw 3D objects from OBJ files
  Using triangles and normals as static object. No texture mapping.
  OBJ files must be triangulated!!!
 ***************************************************************************/

#ifndef _OBJMODEL_HPP_
#define	_OBJMODEL_HPP_

#include <GL/gl.h>

#define VERTICES_PER_TRIANGLE 3
#define COORD_PER_VERTEX 3
#define TOTAL_FLOATS_IN_TRIANGLE (VERTICES_PER_TRIANGLE*COORD_PER_VERTEX)


struct BoundingBox
{
    float Xmax;
    float Ymax;
    float Zmax;
    float Xmin;
    float Ymin;
    float Zmin;

    BoundingBox( ) :
        Xmax( 0.0 ),
        Ymax( 0.0 ),
        Zmax( 0.0 ),
        Xmin( 0.0 ),
        Ymin( 0.0),
        Zmin( 0.0 )
    {
    }
} ;


class ObjModel
{
  public:
    ObjModel();

        /**
         * Calculate the normal of a triangular face defined by three points
         *
         * @param[in] coord1 the first vertex
         * @param[in] coord2 the second vertex
         * @param[in] coord3 the third vertex
         * @param[out] norm the normal
         */
        void computeNormal(const float coord1[3], const float coord2[3], const float coord3[3], float norm[3] );

        /**
         *  Loads the model from file
         * @param filename the OBJ file
         * @return
         */
        int load(const char *filename);

        /**
         * Draws the model with the opengl primitives
         */
        void draw(bool wireframe);

        /**
         * Release the model
         */
        void release();

        /**
         * It scales the model to unitary size by translating it to the origin and
         * scaling it to fit in a unit cube around the origin.
         *
         * @return the scale factor used to transform the model
         */
        float unitizeModel();

        float* _normals;			// Stores the normals
        float* _triangles;			// Stores the triangles
        float* _vertices;			// Stores the points which make the object

        long _numVertices;          // the actual number of loaded vertices
        long _numTriangles;         // the actual number of loaded faces


  private:


    /**
     * Perform a first scan of the file in order to get the number of vertices and the number of faces
     * @param filename
     * @param vertexNum
     * @param faceNum
     */
    void firstScan(const char* filename, long &vertexNum, long &faceNum);

  private:

    // contains the bounding box of the model
    BoundingBox _bb;

};

#endif

