#include <cstdlib>
#include <iostream>

#include "ObjViewer.h"

const int WIDTH = 800;
const int HEIGHT = 600;

using namespace std;

int main(int argc, char* argv[])
{
    GlutWindow::Init(argc, argv);

    if (argc < 4)
    {
        cerr << "Usage:\n"
             << "\tobjviewer <obj-file> <mask-folder> <mask-path> <xml-file>\n"
             << "\n"
             << "\t obj-file: a path to a .OBJ file\n"
             << "\t mask-folder: the folder where the masks will be saved\n"
             << "\t mask-path: the path to the mask folder relative to the xml file\n"
             << "\t xml-file: the output xml file (optional)\n"
             << "\n"
             << "Important notes:\n"
             << "To run this program, you should run tools-unix.sh for Unix, or tools-x86.bat for Windows.\n"
             << "The mask folder must already exist before you run the program.\n"
             << "\n"
             << "Example:\n"
             << "\tobjviewer media/models/suzanne.obj ../masks masks > ../suzanne.xml\n";

        return EXIT_FAILURE;
    }

    ObjViewer* viewer = new ObjViewer("Obj Viewer", WIDTH, HEIGHT);

    // Load the obj model from file
    if (!viewer->loadModel(argv[1]))
    {
        cerr << "Cannot read .OBJ file." << endl;
        return EXIT_FAILURE;
    }

    viewer->setTargetMaskFolder(argv[2], argv[3]);

    if (argc > 4)
    {
        FILE* file = fopen(argv[4], "w");
        viewer->setTargetXmlFile(file);
    }
    else
    {
        viewer->setTargetXmlFile(stdout);
    }

    GlutWindow::Loop();

    delete viewer;

    cerr << endl << "Bye." << endl;

    return EXIT_SUCCESS;
}
