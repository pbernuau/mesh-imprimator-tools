#ifndef OBJVIEWER_H
#define OBJVIEWER_H

#include <cstdio>
#include <string>

#include <MeshImprimator.h>

#include "../tinyxml2.h"
#include "../GlutWindow.h"
#include "ObjModel.hpp"

using namespace tinyxml2;

class ObjViewer : public GlutWindow
{
public:
    ObjViewer(const std::string& title, int width, int height);
    virtual ~ObjViewer();

    bool loadModel(const std::string& filename);
    bool setTargetXmlFile(FILE* file);
    bool setTargetMaskFolder(const std::string& folder,
                             const std::string& path);

protected:
    virtual void display(void);
    virtual void reshape(int width, int height);
    virtual void keyboard(unsigned char key, int x, int y);
    virtual void special(int key, int x, int y);

private:
    double fieldOfView, zNear, zFar;
    double angleX, angleZ, cameraDistance;

    ObjModel* model;
    MaskData maskData;
    int maskIndex;

    Vector3d point;

    bool wireframe;
    bool capture;
    bool save;

    XMLPrinter* printer;
    std::string modelName, maskFolder, maskPath;

    void initOpenGL();
    void setupProjection(int width, int height);

    void saveCamera();
    void saveMask();

    void write();
    void writeMaskData(const std::string& filename);

    //std::vector<Vector3d> points;
};

#endif // OBJVIEWER_H
