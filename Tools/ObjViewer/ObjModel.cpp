#include "ObjModel.hpp"

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <sstream>
#include <string>
#include <vector>
#include <cmath>

using namespace std;


ObjModel::ObjModel()
{
    _numVertices = 0;
    _numTriangles = 0;

    _normals = NULL;
    _triangles = NULL;
    _vertices = NULL;
}

/**
 * Calculate the normal of a triangular face defined by three points
 *
 * @param[in] coord1 the first vertex
 * @param[in] coord2 the second vertex
 * @param[in] coord3 the third vertex
 * @param[out] norm the normal
 */
void ObjModel::computeNormal( const float coord1[3], const float coord2[3], const float coord3[3], float norm[3]  )
{
    //*********************************************
    // Compute the normal vector of the 3 input vertices:
    // Considering the first vertex as "reference", compute the
    // vectors connecting the reference and the other 2 vertices.
    // The normal is given by the cross product of these two vertices
    //
    // Remember to normalize the final vector
    //*********************************************
    float va[3], vb[3], val;

    //**************************************************
    // Compute the vector connecting the "reference" with the second vertex
    //**************************************************
    for (int i = 0; i < 3; i++)
        va[i] = coord2[i] - coord1[i];

    //**************************************************
    // Compute the vector connecting the "reference" with the third vertex
    //**************************************************
    for (int i = 0; i < 3; i++)
        vb[i] = coord3[i] - coord1[i];

    //**************************************************
    // Compute the normal as the cross product of the latters
    //**************************************************
    norm[0] = va[1]*vb[2] - va[2]*vb[1];
    norm[1] = va[2]*vb[0] - va[0]*vb[2];
    norm[2] = va[0]*vb[1] - va[1]*vb[0];

    //**************************************************
    // remember to normalize the vector
    //**************************************************
    val = sqrtf(norm[0]*norm[0] + norm[1]*norm[1] + norm[2]*norm[2]);
    for (int i = 0; i < 3; i++)
        norm[i] = norm[i] / val;

}


/**
 * Perform a first scan of the file in order to get the number of vertices and the number of faces
 * @param[in] filename the OBJ file
 * @param[out] vertexNum the number of vertices found
 * @param[out] triangleNum the number of faces found
 */
void ObjModel::firstScan(const char* filename, long &vertexNum, long &triangleNum)
{
    string line;
    ifstream objFile (filename);
    // If obj file is open, continue
    if (objFile.is_open())
    {
        vertexNum = 0;
        triangleNum = 0;

        // Start reading file data
        while (! objFile.eof() )
        {
            //**************************************************
            // get a line of the file (use getline )
            //**************************************************
            getline(objFile, line);

            if (line.empty())
                continue;

            //**************************************************
            // If the first character is a 'v'...
            //**************************************************
            if (line[0] == 'v')
            {
                //**************************************************
                // Increment the number of vertices
                //**************************************************
                vertexNum++;

            }
            //**************************************************
            // If the first character is a 'f' ...
            //**************************************************
            else if (line[0] == 'f')
            {
                //**************************************************
                // Increment the number of triangles
                //**************************************************
                triangleNum++;
            }
        }

        // Close OBJ file
        objFile.close();
    }
    else
    {
        cerr << "Unable to open file" << endl;
    }
}



/**
 * @brief
 * @param filename
 * @return
 */
int ObjModel::load(const char* filename)
{
    release();

    // Count the number of vertices and the number of triangles
    firstScan(filename, _numVertices, _numTriangles);

    cerr << "Found object with "<< _numVertices << " vertices and "
         << _numTriangles << " faces" << endl;

    string line;
    ifstream objFile (filename);

    bool bbInitialized = false;

    // If obj file is open, continue
    if (objFile.is_open())
    {
        //**************************************************
        // Allocate memory for the vertices: use malloc to allocate the memory
        // How many floats do you need overall...?
        //**************************************************
        _vertices = new float[_numVertices * COORD_PER_VERTEX];


        //**************************************************
        // Allocate memory for the triangles: use malloc to allocate the memory
        // How many floats do you need overall...? count how many floats you need
        // for each vertices, hence how many floats for each triangle...
        //**************************************************
        _triangles = new float[_numTriangles * TOTAL_FLOATS_IN_TRIANGLE];

        //**************************************************
        // Allocate memory for the triangles: use normals to allocate the memory
        // How many floats do you need overall...?
        //**************************************************
        _normals = new float[_numTriangles * TOTAL_FLOATS_IN_TRIANGLE];


        // This index is used to run through the triangle array
        int triangle_index = 0;
        // This index is used to run through the normal array
        int normal_index = 0;
        // This index is used to run through the vertex array
        int vertex_index = 0;

        // Start reading file data
        while (! objFile.eof() )
        {
            //**************************************************
            // Get a line from file
            //**************************************************
            getline(objFile, line);

            if (line.empty())
                continue;

            istringstream stream(line);
            stream.ignore(1);

            //**************************************************
            // If the first character is a 'v'...
            //**************************************************
            if (line[0] == 'v')
            {
                //**************************************************
                // Read 3 floats from the line:  X Y Z and store them in the corresponding place in _vertices
                // In order to read the floats in one shot from string you can use sscanf
                //**************************************************
                stream >> _vertices[vertex_index+0];
                stream >> _vertices[vertex_index+1];
                stream >> _vertices[vertex_index+2];

                //**************************************************
                // This is for the 2nd part of the exercise: update the bounding box
                // For the very first vertex read, initialize the bb accordingly
                //**************************************************
                if (!bbInitialized)
                {
                    //**************************************************
                    // Case of the very first vertex read
                    //**************************************************
                    _bb.Xmin = _bb.Xmax = _vertices[vertex_index+0];
                    _bb.Ymin = _bb.Ymax = _vertices[vertex_index+1];
                    _bb.Zmin = _bb.Zmax = _vertices[vertex_index+2];


                    bbInitialized = true;
                }
                else
                {
                    //**************************************************
                    // otherwise check the vertex against the bounding box and in case update it
                    //**************************************************

                    _bb.Xmin = fminf(_bb.Xmin, _vertices[vertex_index+0]);
                    _bb.Xmax = fmaxf(_bb.Xmax, _vertices[vertex_index+0]);

                    _bb.Ymin = fminf(_bb.Ymin, _vertices[vertex_index+1]);
                    _bb.Ymax = fmaxf(_bb.Ymax, _vertices[vertex_index+1]);

                    _bb.Zmin = fminf(_bb.Zmin, _vertices[vertex_index+2]);
                    _bb.Zmax = fmaxf(_bb.Zmax, _vertices[vertex_index+2]);
                }

                // update the vertex
                vertex_index += COORD_PER_VERTEX;

                // just a security check...
                assert((vertex_index <= _numVertices*3));
            }
            //**************************************************
            // If the first character is a 'v'...
            //**************************************************
            else if (line[0] == 'f')
            {
                // this contains temporary the indices of the vertices
                int vertexIdx[3] = { 0, 0, 0 };

                //**************************************************
                // Read 3 integers from the line:  idx1 idx2 idx3 and store them in the corresponding place in vertexIdx
                // In order to read the 3 integers in one shot from string you can use sscanf
                //**************************************************
                stream >> vertexIdx[0];
                stream >> vertexIdx[1];
                stream >> vertexIdx[2];

                //**************************************************
                // correct the indices: OBJ starts counting from 1, in C the arrays starts at 0...
                //**************************************************
                vertexIdx[0]--;
                vertexIdx[1]--;
                vertexIdx[2]--;

                //just a security check
                assert((vertexIdx[0] >= 0 )&&(vertexIdx[0] <= _numVertices));
                assert((vertexIdx[1] >= 0 )&&(vertexIdx[1] <= _numVertices));
                assert((vertexIdx[2] >= 0 )&&(vertexIdx[2] <= _numVertices));

                //*********************************************************************
                //  fill the _triangles array with the 3 vertices
                // tCounter gives you the starting position of each vertex in _triangles
                //*********************************************************************
                int tCounter = 0;
                for (int i = 0; i < VERTICES_PER_TRIANGLE; i++)
                {
                    for (int j = 0; j < COORD_PER_VERTEX; j++)
                    {
                        _triangles[triangle_index+tCounter+j] =
                            _vertices[vertexIdx[i]*COORD_PER_VERTEX+j];
                    }

                    tCounter += VERTICES_PER_TRIANGLE;
                }

                //*********************************************************************
                //  Calculate the normal of the triangles, it will be the same for each vertex
                //*********************************************************************
                float norm[3];

                //*********************************************************************
                //  compute the normal for the 3 vertices we just added
                //*********************************************************************
                computeNormal(&_triangles[triangle_index+0*COORD_PER_VERTEX],
                              &_triangles[triangle_index+1*COORD_PER_VERTEX],
                              &_triangles[triangle_index+2*COORD_PER_VERTEX],
                              norm);


                //*********************************************************************
                //  fill the _normals array with 3 copy of the normal
                // tCounter gives you the starting position of each normal in _normals
                //*********************************************************************
                tCounter = 0;
                for (int i = 0; i < VERTICES_PER_TRIANGLE; i++)
                {
                    for (int j = 0; j < COORD_PER_VERTEX; j++)
                    {
                        _normals[normal_index+tCounter+j] = norm[j];
                    }


                    tCounter += VERTICES_PER_TRIANGLE;
                }


                // update the indices
                triangle_index += TOTAL_FLOATS_IN_TRIANGLE;
                normal_index += TOTAL_FLOATS_IN_TRIANGLE;



                // just a security check
                assert((triangle_index <= _numTriangles*TOTAL_FLOATS_IN_TRIANGLE));
                assert((normal_index <= _numTriangles*TOTAL_FLOATS_IN_TRIANGLE));

            }
        }
        cerr << "TotalConnectedTriangles "<< triangle_index << endl;

        assert( _numTriangles == triangle_index/TOTAL_FLOATS_IN_TRIANGLE );
        assert( _numVertices == vertex_index/COORD_PER_VERTEX );


        // Close OBJ file
        objFile.close();

    }
    else
    {
        cerr << "Unable to open file" << endl;
        return -1;
    }


    cerr << "Object loaded with "<< _numVertices << " vertices and "  << _numTriangles << " faces" << endl;
    cerr << "Bounding box : Xmax=" << _bb.Xmax << "  Xmin=" << _bb.Xmin << "  Ymax=" << _bb.Ymax << "  Ymin=" << _bb.Ymin << "  Zmax=" << _bb.Zmax << "  Zmin=" << _bb.Zmin << endl;
    return 0;
}

void ObjModel::release()
{
    delete _triangles;
    delete _normals;
    delete _vertices;

    _triangles = NULL;
    _normals = NULL;
    _vertices = NULL;
}

void ObjModel::draw(bool wireframe)
{
    glColor3f(1.f, 1.f, 1.f);

    //****************************************
    // Enable vertex arrays
    //****************************************
    glEnableClientState(GL_VERTEX_ARRAY);

    //****************************************
    // Enable normal arrays
    //****************************************
    glEnableClientState(GL_NORMAL_ARRAY);

    //****************************************
    // Vertex Pointer to triangle array
    //****************************************
    glVertexPointer(COORD_PER_VERTEX,
                    GL_FLOAT,
                    0,
                    _triangles);

    //****************************************
    // Normal pointer to normal array
    //****************************************
    glNormalPointer(GL_FLOAT,
                    0,
                    _normals);

    //****************************************
    // Draw the triangles
    //****************************************
    int n = _numTriangles * VERTICES_PER_TRIANGLE;
    if (wireframe)
    {
        for(int i = 0; i < n; i += 3)
            glDrawArrays(GL_LINE_LOOP, i, 3);
    }
    else
    {
        glDrawArrays(GL_TRIANGLES, 0, n);
    }

    //****************************************
    // Disable vertex arrays
    //****************************************
    glDisableClientState(GL_VERTEX_ARRAY);

    //****************************************
    // Disable normal arrays
    //****************************************
    glDisableClientState(GL_NORMAL_ARRAY);
}



/**
 * It scales the model to unitary size by translating it to the origin and
 * scaling it to fit in a unit cube around the origin.
 *
 * @return the scale factor used to transform the model
 */
float ObjModel::unitizeModel()
{
    if(_vertices && _triangles)
    {
        //****************************************
        // calculate model width, height, and
        // depth using the bounding box
        //****************************************
        float w,h,d;


        w = _bb.Xmax - _bb.Xmin;
        h = _bb.Ymax - _bb.Ymin;
        d = _bb.Zmax - _bb.Zmin;

        //****************************************
        // calculate center of the bounding box of the model
        //****************************************
        float cx,cy,cz;

        cx = w / 2.f + _bb.Xmin;
        cy = h / 2.f + _bb.Ymin;
        cz = d / 2.f + _bb.Zmin;


        //****************************************
        // calculate the unitizing scale factor as the
        // maximum of the 3 dimensions
        //****************************************
        float scale = fmaxf(w, fmaxf(h, d));

        cerr << "scale: " << scale << " cx " << cx << " cy " << cy << " cz " << cz << endl;

        // translate each vertex wrt to the center and then apply the scaling to the coordinate
        for (int i = 0; i < _numVertices; i++)
        {
            //****************************************
            // translate the vertex
            //****************************************
            _vertices[i*3+0] -= cx;
            _vertices[i*3+1] -= cy;
            _vertices[i*3+2] -= cz;

            //****************************************
            // apply the scaling
            //****************************************
            _vertices[i*3+0] /= scale;
            _vertices[i*3+1] /= scale;
            _vertices[i*3+2] /= scale;
        }


        // do the same for each vertex of the triangles
        for (int i = 0; i < _numTriangles; ++i)
        {
            // for each triangle
            for (int j = 0; j < VERTICES_PER_TRIANGLE; ++j)
            {
                int index = i*TOTAL_FLOATS_IN_TRIANGLE+j*VERTICES_PER_TRIANGLE;
                //****************************************
                // translate the vertex
                // apply the scaling
                //****************************************
                _triangles[index+0] = (_triangles[index+0] - cx) / scale;
                _triangles[index+1] = (_triangles[index+1] - cy) / scale;
                _triangles[index+2] = (_triangles[index+2] - cz) / scale;
            }

        }

        //****************************************
        // update the bounding box, ie translate and scale the 6 coordinates
        //****************************************
        _bb.Xmin = (_bb.Xmin - cx) / scale;
        _bb.Xmax = (_bb.Xmax - cx) / scale;

        _bb.Ymin = (_bb.Ymin - cy) / scale;
        _bb.Ymax = (_bb.Ymax - cy) / scale;

        _bb.Zmin = (_bb.Zmin - cz) / scale;
        _bb.Zmax = (_bb.Zmax - cz) / scale;

        cerr << "New bounding box : Xmax=" << _bb.Xmax << "  Xmin=" << _bb.Xmin << "  Ymax=" << _bb.Ymax << "  Ymin=" << _bb.Ymin << "  Zmax=" << _bb.Zmax << "  Zmin=" << _bb.Zmin << endl;

        return scale;

    }

    return 0;
}


