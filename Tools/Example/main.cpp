#include "../tinyxml2.h"
#include "../mask_io.h"

#include <MeshImprimator.h>

#include <iostream>
#include <cstdlib>

using namespace std;
using namespace tinyxml2;


IMeshReconstructor* InitDelaunay(const PointCloud& cloud,
                                 const char* xmlFilename);
bool ReadPointCloud(PointCloud& cloud, const char* plyFilename);

class SimpleProgress : public IProgress
{
public:
    SimpleProgress() :
        _min(0),
        _max(100),
        _value(0)
    {
    }

    virtual void setValue(int value)
    {
        int percent = 100 * static_cast<float>(value - _min) / (_max - _min);

        if (_value != percent)
        {
            _value = percent;

            cout << "[";
            for (int i = 1; i < 20; i++)
            {
                cout << ((i < _value / 5) ? '#' : ' ');
            }
            cout << "] " << _value << " %\n";
        }
    }

    virtual bool wasCanceled() const
    {
        return false;
    }

    virtual void setRange(int min, int max)
    {
        _min = min;
        _max = max;
    }

private:
    int _min, _max, _value;
};

void Pause()
{
    string tmp;
    cout << "Press any key to continue..." << endl;
    getline(cin, tmp);
}

void PrintUsage()
{
    cerr << "Usage:\n"
              << "\texample -r delaunay -x <xml-file> -i <input-ply-file> -o <output-stl-file> [-d featurepreserving <denoising-parameters>]\n"
              << "or\n"
              << "\texample -r powercrust -i <input-ply-file> -o <output-stl-file> [-d featurepreserving <denoising-parameters>]\n"
              << "\n"
              << "\t -r defines the reconstruction method used\n"
              << "\t xml-file: file containing the cameras and masks \n"
              << "\t input-ply-file: file containing the point cloud \n"
              << "\t output-stl-file: the output stl file \n"
              << "\t -d defines the denoising method. Its value can be \"featurepreserving\" or \"none\", which is the default.\n"
              << "\t denoising-parameters:\n"
              << "\t\t -t: threshold to determine whether a sharp edge has to be conserved\n"
              << "\t\t -n: number of iteration for the normal updating\n"
              << "\t\t -v: number of iteration for the vertex updating\n"
              << "\n"
              << endl;
}

int main(int argc, const char* argv[])
{
    // initialize parameters
    int curArg = 1;
    string reconstructionMethod = "p"; // Power-Crust
    string denoisingMethod = "n"; // none
    string inputFilename, outputFilename, xmlFilename;
    double threshold = .3;
    int nIterNormals = 20;
    int nIterVertex = 50;

    if (argc < 2)
    {
        PrintUsage();
        return EXIT_FAILURE;
    }

    // read parameters
    while(curArg < argc)
    {
        if(argv[curArg][0] == '-') // not robust
        {
            char argName = argv[curArg++][1];
            if(curArg < argc)
            {
                char *argVal = (char *) argv[curArg++];
                switch (argName) {
                case 'r':
                    reconstructionMethod = argVal;
                    break;
                case 'd':
                    denoisingMethod = argVal;
                    break;
                case 'i':
                    inputFilename = argVal;
                    break;
                case 'o':
                    outputFilename = argVal;
                    break;
                case 'x':
                    xmlFilename = argVal;
                    break;
                case 't':
                    threshold = strtod(argVal, NULL);
                    break;
                case 'n':
                    nIterNormals = strtol(argVal, NULL, 10);
                    break;
                case 'v':
                    nIterVertex = strtol(argVal, NULL, 10);
                    break;
                default:
                    break;
                }
            }
            else
            {
                PrintUsage();
                return EXIT_FAILURE;
            }
        }
        else
        {
            PrintUsage();
            return EXIT_FAILURE;
        }
    }

    if (inputFilename.empty())
    {
        cerr << "Missing input filename." << endl;
        PrintUsage();
        return EXIT_FAILURE;
    }

    if (outputFilename.empty())
    {
        cerr << "Missing output filename." << endl;
        PrintUsage();
        return EXIT_FAILURE;
    }

    //==========================================================================

    PointCloud cloud;

    if (!ReadPointCloud(cloud, inputFilename.c_str()))
    {
        cerr << "Cannot read PLY file." << endl;
        return EXIT_FAILURE;
    }

    //==========================================================================

    IProgress* progress = new SimpleProgress();

    // mesh reconstruction
    IMeshReconstructor* reconstructor = NULL;
    switch (reconstructionMethod[0])
    {
    case 'd':
        reconstructor = InitDelaunay(cloud, xmlFilename.c_str());
        break;
    case 'p':
        reconstructor = new PowercrustReconstructor(cloud);
        break;
    default:
        break;
    }

    if (reconstructor == NULL)
    {
        cerr << "Cannot execute the given reconstructor." << endl;
        return EXIT_FAILURE;
    }

    //==========================================================================

    Pause();

    IndexedTriangleMesh triangleMesh = reconstructor->reconstructIndexed(progress);
    delete reconstructor;

    //==========================================================================

    // mesh denoising
    TriangleMesh finalMesh;
    switch(denoisingMethod[0]){
    case 'f':
    {
        IDenoiser* denoiser = new FeaturePreservingDenoiser(triangleMesh, threshold, nIterNormals, nIterVertex);
        finalMesh = denoiser->denoise(progress);
        delete denoiser;
        break;
    }
    case 'n':
        finalMesh = triangleMesh.toUnindexed();
        break;
    default:
        break;
    }

    delete progress;

    cout << "I reconstructed " << finalMesh.triangles.size()
         << " triangles." << endl;

    STLWriter writer;
    writer.write(finalMesh, outputFilename, outputFilename);

    //==========================================================================

    return EXIT_SUCCESS;
}

IMeshReconstructor* InitDelaunay(const PointCloud& cloud,
                                 const char* xmlFilename)
{
    bool ok;

    cout << "Reading some XML..." << endl;

    list<MaskData> maskDataList = ReadMasksFromXml(xmlFilename, &ok);

    if (!ok)
        return NULL;

    cout << "Found " << maskDataList.size() << " masks." << endl;

    return new DelaunayReconstructor(
                cloud,
                maskDataList,
                1,
                DelaunayReconstructor::TETRAHEDRALIZE);
}

bool ReadPointCloud(PointCloud& cloud, const char* plyFilename)
{
    cout << "Reading some PLY file..." << endl;

    PLYReader reader;
    reader.open(plyFilename);

    int errorCode, line;
    if (!reader.readHeader(&errorCode, &line))
    {
        cerr << "Error: " << errorCode << endl;
        cerr << "Line: " << line << endl;
        return false;
    }

    cout << "PLY header successfully read." << endl;

    if (!reader.readContent(&errorCode, &line))
    {
        cerr << "Error: " << errorCode << endl;
        cerr << "Line: " << line << endl;
        return false;
    }

    cout << "PLY content successfully read." << endl;

    reader.close();

    const Ply::Object* obj = reader.getObject();

    cloud = obj->getPointCloud();
    return true;
}
