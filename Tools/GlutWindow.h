#ifndef GLUTWINDOW_H
#define GLUTWINDOW_H

#include <GL/freeglut.h>

class GlutWindow
{
public:
    explicit GlutWindow(const char* name);
    GlutWindow(const char* name, const char* title, int width, int height);
    virtual ~GlutWindow();

    static void Init(int& argc, char** argv);
    static void Loop(void);
    static void Quit(void);

protected:
    virtual void display(void);
    virtual void reshape(int width, int height);
    virtual void keyboard(unsigned char key, int x, int y);
    virtual void mouse(int button, int state, int x, int y);
    virtual void special(int key, int x, int y);
    virtual void close(void);

    void postRedisplay();
    int getWidth();
    int getHeight();

    void setSize(int width, int height);
    void setPosition(int x, int y);
    void setTitle(const char* title);

private:
    int id;

    void init(const char* name);

    static void DisplayFunc(void);
    static void ReshapeFunc(int width, int height);
    static void KeyboardFunc(unsigned char key, int x, int y);
    static void MouseFunc(int button, int state, int x, int y);
    static void SpecialFunc(int key, int x, int y);
    static void CloseFunc(void);
};

#endif // GLUTWINDOW_H
