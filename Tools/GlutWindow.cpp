#include "GlutWindow.h"

struct WindowLocker
{
    // Stores the current window
    WindowLocker() : window(glutGetWindow())
    {
    }

    // Stores the current window and sets the given window active
    explicit WindowLocker(int id) : window(glutGetWindow())
    {
        glutSetWindow(id);
    }

    // Restores the window
    ~WindowLocker()
    {
        glutSetWindow(window);
    }

    const int window;
};

//==============================================================================

GlutWindow::GlutWindow(const char* name)
{
    init(name);
}

GlutWindow::GlutWindow(const char* name,
                       const char* title,
                       int width,
                       int height)
{
    init(name);
    setTitle(title);
    setSize(width, height);
}

GlutWindow::~GlutWindow()
{
    glutDestroyWindow(id);
}

void GlutWindow::Init(int& argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );

    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION);
}

void GlutWindow::Loop()
{
    glutMainLoop();
}

void GlutWindow::Quit()
{
    glutLeaveMainLoop();
}

//==============================================================================

void GlutWindow::display(void)
{
}

void GlutWindow::reshape(int width, int height)
{
    (void) width;
    (void) height;
}

void GlutWindow::keyboard(unsigned char key, int x, int y)
{
    (void) x;
    (void) y;

    switch (key)
    {
    // key ESCAPE
    case 27:
        GlutWindow::Quit();
        break;
    }
}

void GlutWindow::mouse(int button, int state, int x, int y)
{
    (void) button;
    (void) state;
    (void) x;
    (void) y;
}

void GlutWindow::special(int key, int x, int y)
{
    (void) key;
    (void) x;
    (void) y;
}

void GlutWindow::close(void)
{
}

//==============================================================================

void GlutWindow::postRedisplay()
{
    glutPostRedisplay();
}

int GlutWindow::getWidth()
{
    WindowLocker locker(id);
    return glutGet(GLUT_WINDOW_WIDTH);
}

int GlutWindow::getHeight()
{
    WindowLocker locker(id);
    return glutGet(GLUT_WINDOW_HEIGHT);
}

void GlutWindow::setSize(int width, int height)
{
    WindowLocker locker(id);
    glutReshapeWindow(width, height);
}

void GlutWindow::setPosition(int x, int y)
{
    WindowLocker locker(id);
    glutPositionWindow(x, y);
}

void GlutWindow::setTitle(const char* title)
{
    WindowLocker locker(id);
    glutSetWindowTitle(title);
}

void GlutWindow::init(const char* name)
{
    WindowLocker locker;

    id = glutCreateWindow(name);
    glutSetWindowData(this);

    glutDisplayFunc(DisplayFunc);
    glutReshapeFunc(ReshapeFunc);
    glutKeyboardFunc(KeyboardFunc);
    glutMouseFunc(MouseFunc);
    glutSpecialFunc(SpecialFunc);
    glutCloseFunc(CloseFunc);
}

//==============================================================================

void GlutWindow::DisplayFunc(void)
{
    GlutWindow* win = reinterpret_cast<GlutWindow*>(glutGetWindowData());
    win->display();
}

void GlutWindow::ReshapeFunc(int width, int height)
{
    GlutWindow* win = reinterpret_cast<GlutWindow*>(glutGetWindowData());
    win->reshape(width, height);
}

void GlutWindow::KeyboardFunc(unsigned char key, int x, int y)
{
    GlutWindow* win = reinterpret_cast<GlutWindow*>(glutGetWindowData());
    win->keyboard(key, x, y);
}

void GlutWindow::MouseFunc(int button, int state, int x, int y)
{
    GlutWindow* win = reinterpret_cast<GlutWindow*>(glutGetWindowData());
    win->mouse(button, state, x, y);
}

void GlutWindow::SpecialFunc(int key, int x, int y)
{
    GlutWindow* win = reinterpret_cast<GlutWindow*>(glutGetWindowData());
    win->special(key, x, y);
}

void GlutWindow::CloseFunc()
{
}

