#ifndef MASK_IO_H
#define MASK_IO_H

#include <list>
#include <string>

#include <MeshImprimator.h>

void WriteMask(const MaskData::Mask& mask, const std::string& filename);
bool ReadMask(MaskData::Mask& mask, const std::string& filename);

std::list<MaskData> ReadMasksFromXml(const std::string& xmlFilename,
                                     bool* success = NULL);

#endif
