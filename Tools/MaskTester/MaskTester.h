#ifndef MASKTESTER_H
#define MASKTESTER_H

#include <cstdio>
#include <string>
#include <list>
#include <vector>

#include <MeshImprimator.h>

#include "../tinyxml2.h"
#include "../GlutWindow.h"

using namespace tinyxml2;

class MaskTester : public GlutWindow
{
public:
    MaskTester(const std::string& title, int width, int height);
    virtual ~MaskTester();

    int area;
    std::list<MaskData> maskDataList;

protected:
    virtual void display(void);
    virtual void reshape(int width, int height);
    virtual void keyboard(unsigned char key, int x, int y);
    virtual void special(int key, int x, int y);

private:
    double fieldOfView, zNear, zFar;
    double angleX, angleZ, cameraDistance;

    std::vector<Vector3d> points;

    void initOpenGL();
    void setupProjection(int width, int height);

    bool isInside(const Vector3d& point) const;
};

#endif // MASKTESTER_H
