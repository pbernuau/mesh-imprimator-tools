#include <climits>

#include <algorithm>
#include <exception>
#include <iostream>
#include <fstream>
#include <sstream>

#include "MaskTester.h"

const double DELTA_ANGLE_X  = 5;
const double DELTA_ANGLE_Y  = 5;
const double DELTA_DISTANCE = 0.3;
const double DISTANCE_MIN   = 0.0;

MaskTester::MaskTester(const std::string& title, int width, int height) :
    GlutWindow("MaskTester", title.c_str(), width, height),
    area(1),
    fieldOfView(45),
    zNear(1),
    zFar(500),
    angleX(-60),
    angleZ(30),
    cameraDistance(25)
{
    initOpenGL();
    setupProjection(width, height);

    points.reserve(5000);
    for (int i = 0; i < 5000; i++)
    {
        points.push_back(30.0 * Vector3d::Random());
    }

}

MaskTester::~MaskTester()
{
}

void MaskTester::display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glPushMatrix();
    gluLookAt(0, cameraDistance, 0,
              0, 0, 0,
              0, 0, 1);

    glRotated(angleX, 1, 0, 0);
    glRotated(angleZ, 0, 0, 1);


    glBegin(GL_LINES);
    glColor3f(1, 0, 0); glVertex3f(0, 0, 0); glVertex3f(50, 0, 0);
    glColor3f(0, 1, 0); glVertex3f(0, 0, 0); glVertex3f(0, 50, 0);
    glColor3f(0, 0, 1); glVertex3f(0, 0, 0); glVertex3f(0, 0, 50);
    glEnd();

    glBegin(GL_POINTS);
    for (int i = 0; i < points.size(); i++)
    {
        const Vector3d& point = points.at(i);

        if (isInside(point))
            glColor3f(0, 1, 0);
        else
            glColor3f(1, 0, 0);


        glVertex3f(point(0), point(1), point(2));
    }
    glEnd();

    glPopMatrix();

    glutSwapBuffers();
}

void MaskTester::reshape(int width, int height)
{
    setupProjection(width, height);
    postRedisplay();
}

void MaskTester::keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
    default:
        GlutWindow::keyboard(key, x, y);
        return;
    }

    postRedisplay();
}

void MaskTester::special(int key, int x, int y)
{
    switch (key)
    {
    case GLUT_KEY_UP:
        angleX -= DELTA_ANGLE_X;
        break;
    case GLUT_KEY_DOWN:
        angleX += DELTA_ANGLE_X;
        break;
    case GLUT_KEY_LEFT:
        angleZ += DELTA_ANGLE_Y;
        break;
    case GLUT_KEY_RIGHT:
        angleZ -= DELTA_ANGLE_Y;
        break;
    case GLUT_KEY_PAGE_DOWN:
        cameraDistance += DELTA_DISTANCE;
        break;
    case GLUT_KEY_PAGE_UP:
        cameraDistance -= DELTA_DISTANCE;
        cameraDistance = std::max(DISTANCE_MIN, cameraDistance);
        break;
    default:
        GlutWindow::special(key, x, y);
    }

    postRedisplay();
}

void MaskTester::initOpenGL()
{
    // setup options
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glEnable(GL_CULL_FACE);

    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glPointSize(1.5f);

    // setup lighting
    GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat mat_shininess[] = { 100.0 };
    GLfloat light0_position[] = { 0.0, 0.0, -1.0, 0.0 };
    GLfloat light1_position[] = { 0.0, 1.0, 0.0, 0.0 };
    GLfloat light2_position[] = { 1.0, 0.0, 5.0, 0.0 };
    glShadeModel(GL_SMOOTH);

    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
    glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
    glLightfv(GL_LIGHT0, GL_POSITION, light1_position);
    glLightfv(GL_LIGHT0, GL_POSITION, light2_position);

    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);
}

void MaskTester::setupProjection(int width, int height)
{
    glViewport(0, 0, width, height);

    double aspect = static_cast<double>(width) / height;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(fieldOfView, aspect, zNear, zFar);
}


bool MaskTester::isInside(const Vector3d& point) const
{
    for(std::list<MaskData>::const_iterator it = maskDataList.begin();
        it != maskDataList.end();
        it++)
    {
        if (!it->contains(point, area))
        {
            return false;
        }
    }

    return true;
}
