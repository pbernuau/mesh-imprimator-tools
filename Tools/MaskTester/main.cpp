#include "MaskTester.h"
#include "../mask_io.h"

#include <iostream>
#include <cstdlib>

const int WIDTH = 800;
const int HEIGHT = 600;

using namespace std;

int main(int argc, char* argv[])
{
    cout << "Usage: masktester <xml-file>" << endl;

    GlutWindow::Init(argc, argv);

    if (argc < 2)
    {
        cerr << "Missing argument." << endl;
        return EXIT_FAILURE;
    }

    bool ok;

    MaskTester* window = new MaskTester("Mask Tester", WIDTH, HEIGHT);
    window->maskDataList = ReadMasksFromXml(argv[1], &ok);

    if (!ok)
        return EXIT_FAILURE;


    std::cerr << "I found " << window->maskDataList.size()
              << " masks." << std::endl;

    GlutWindow::Loop();

    delete window;

    return EXIT_SUCCESS;
}
