import sys
# Edit the following line to set the path to the `imprimante3D` repository
sys.path.append('<path>/impression/scripts/addons')
import ultimaker
from printing_preprocessing import register
register() # Add the tool panel to the Properties > Object panel
