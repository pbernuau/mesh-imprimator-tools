import sys
# Edit the following line to set the path to the `imprimante3D` repository
sys.path.append('<path>/impression/scripts/addons')
from ultimaker.modules.manifold import *
C = bpy.context
edges = C.active_object.data.edges
[edges_holes, uho, nb_edges] = get_holes(edges)
