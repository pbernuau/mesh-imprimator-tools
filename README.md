# Welcome to Mesh Imprimator Tools homepage

MeshImprimator is a library written in C++ that reconstructs a mesh from a set
of 3D points. The repository also contains various programs to test and run the
library. The library and the tools should run under Windows and Linux systems.

# Getting started with the library

## Installing dependencies

You will need QtCreator to open `.pro` project files. However, the library
and the tools do not use any Qt libraries.

Under Linux you will need `libtet` to compile the library. Install it from [this site](https://launchpad.net/ubuntu/trusty/+package/libtet1.5) choosing your system's architecture. Also install the development package from [here](https://launchpad.net/ubuntu/trusty/+package/libtet1.5-dev).
	
Some examples need `libfreeglut` too:

    :::shell
	sudo apt-get install freeglut3-dev

## Compiling

You must build the library first. Open `Project/Project.pro` in QtCreator and
run the build command. The generated binaries should go into the `Project/bin`
folder.

To run the examples in the `Tools` directory, open their Qt project file 
(`.pro`) and build them. Each program will go into the `Tools/bin` folder.

## Running the examples

Under Windows, run the Batch script `tools-x86.bat`, or under Linux, run `tools-unix.sh`. This script will
temporarily add the needed directories to the `PATH` environment variable and
open up a command prompt.

You may now run the examples. For instance:

    :::shell
	objviewer

Or:

    :::shell
	example
	
## Running the tests with QtTest

Open the `Tools/Tests/Tests.pro` file under Qt and run it. The results of the different tests done are displayed on the Qt Console.
	
## Generating the documentation

Run the Batch script `tools-x86.bat` or `tools-unix.sh` and shift the current directory to the Project directory.
Generate the documentation with :

    :::shell
	cd Project
	doxygen
  
Then, go to the `Project/doc/html` folder and click on the `index.html` file. This will lead to an html page showing the documentation generated.