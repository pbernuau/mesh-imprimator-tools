%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage[french]{babel}
\usepackage[USenglish]{isodate}
\usepackage[margin=2.5cm]{geometry}
\usepackage[default,osfigures,scale=0.95]{opensans}
\usepackage{setspace}
\usepackage[usenames,dvipsnames]{color}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{array}
\usepackage{lastpage}
\usepackage{fancyhdr}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\onehalfspacing

\makeatletter
\DeclareRobustCommand{\em}{%
  \@nomath\em \if b\expandafter\@car\f@series\@nil
  \normalfont \else \bfseries\color{MidnightBlue} \fi}
\makeatother

\cleanlookdateon

\makeatletter
\def\title#1{\gdef\@title{#1}\gdef\thetitle{#1}}
\makeatother

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\pagestyle{fancy}
\fancyhf{}

\lhead{\thetitle}
\chead{}
\rhead{Projet Long \\ 2013 - 2014}

\lfoot{\parbox{3cm}{\includegraphics[width=4cm]{../enseeiht.png}}}
\cfoot{Surface reconstruction and 3D printing}
\rfoot{Page \thepage\ of \pageref{LastPage}}

\renewcommand{\footrulewidth}{0.2pt}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Technical Specifications \\ Revision 2.1}
\author{
    Bernuau Paul         \\ \texttt{paul.bernuau@etu.enseeiht.fr}         \and
    Ho Florence          \\ \texttt{florence.ho@etu.enseeiht.fr}          \and
    Lefrançois William   \\ \texttt{william.lefrancois@etu.enseeiht.fr}   \and
    Ripplinger Victor    \\ \texttt{victor.ripplinger@etu.enseeiht.fr}    \and
    Thonet Thibaut       \\ \texttt{thibaut.thonet@etu.enseeiht.fr} 
}
\date{9 February 2014}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

  \maketitle
  
  \tableofcontents
  \pagebreak
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Introduction}

This document intends to define and list the \emph{requirements} and
\emph{specifications} for the \textit{Surface reconstruction and 3D printing project}.
The goal of the project is to develop a library that implements a 3D
surface reconstruction from a set of points. The aim is reached if the
implemented algorithms are robust enough to reconstruct the mesh of a
point cloud acquired from real camera pictures. \\
The first section defines what the different versions are and when they are due.
The following section specifies the different steps of the pipeline
used to reconstruct a mesh. The last section expands on how the
program will be tested and how its quality will be assessed.

\section*{Revisions}

\begin{description}
    \item[1.0] 24 January 2014 - Initial version.
    \item[2.0] 27 January 2014 - Detailing Delaunay Reconstruction. Removing clean point cloud constraint from V0.
    \item[2.1] 9 February 2014 - Detailing Noise Removal part. Adding reference to Amenta's sampling condition in Mesh Reconstruction. 
\end{description}

\pagebreak

\section{The deliverables}

\subsection{Version 0}

Version 0 is a minimalist working version of the pipeline.
\begin{enumerate}
\item The delivrables will consist of a \emph{program} which takes its
input from \emph{hard-coded PLY files}.
\item These files will be supposed to represent \emph{any point cloud}.
\item Only the \emph{Delaunay reconstruction with carving} will be implemented
and used in this version.
\item There are no requirements regarding the quality of the mesh in this version.
\item The expected output is \emph{valid STL files}. It must be possible to use
the \emph{Slic3r} program in order to \emph{convert from STL to G-code}
(G-code being the 3D printer file format).
\item The software is \emph{due for Friday 7 February}.
\item Its \emph{tests and documentation} are \emph{due for Friday 14 February}.
\end{enumerate}


\subsection{Version 1}

Version 1 is the complete implementation of the pipeline.

\bigskip

The deliverables for the final version consist of:
\begin{enumerate}
\item A \emph{static C++ library} that can be embedded in a program.
\item An example \emph{program} that uses the library and generates an
\emph{STL file} from a \emph{PLY file}. The user may provide an optional
set of \emph{masks} that represents the object to be reconstructed viewed from
\emph{different camera angles and positions}.
\item The \emph{library documentation} (automatically generated from the source files).
\item The \emph{example program documentation}. This documentation may be integrated
into the program itself (for instance, in a terminal: \texttt{program -h} or
\texttt{program --help}).
\item A \emph{set of tests} that assert the software quality and the robustness of
the algorithms against noise and outliers. 
\end{enumerate}

\bigskip

There are several project-wide requirements:
\begin{enumerate}
\item Only \emph{open source and free} external libraries may be used.
\item The deliverables must be \emph{compatible with UNIX systems}.
\item External libraries must not be embedded into the final deliverable library.
\end{enumerate}

\bigskip

All the deliverables for version 1  are \emph{due for Friday 14 March}.

\pagebreak

\section{The Pipeline}

\subsection{Inputs}

A set of 3D points. An optional set of masks representing the object
associated with the camera position and angle for each of these masks.

\subsection{Noise removal}

\textit{This step is not part of Version 0}.

\bigskip

The noise removal step \emph{cleans up the point cloud}: it removes
outliers and handles local error inherent to each point.  It is expected
that the points are filtered so that they all reproject into the masks.

\bigskip

Outlier points may be determined and removed from the cloud using
Principal Component Analysis on a set of surrounding points.

\subsection{Mesh reconstruction}

There are three possible algorithms. The first two \emph{must be
implemented}. Implementing the last one is \emph{optional}.

\begin{enumerate}
\item Power Crust;
\item Delaunay tetrahedralization, carving and surface extraction;
\item Poisson Surface Reconstruction.
\end{enumerate}

The result must be a \emph{triangle mesh}.

\bigskip

Amenta's sampling condition can be used to assess the quality of
the reconstruction. In particular, points may be colored according
to their reliability.

\subsection{Mesh Correction}

To print the mesh, the following conditions must hold true:
\begin{enumerate}
\item The mesh is \emph{manifold}. In particular, its surface must be
free of holes.
\item The mesh \emph{stands still} and the iterative stacking of the slices (from
bottom to top) is stable too.
\end{enumerate}

Remarks:
\begin{itemize}
\item It is the responsibility of the user to fix the mesh by hand if the mesh
is not printable. The library may optionally fix the mesh (using the same algorithm
as the one in the Blender add-on).
\item An algorithm to check the conditions may be developed.
\item If relevant, the expected output mesh format is \emph{STL}. It must be
possible to use Slic3r to convert this STL file to G-code.
\end{itemize}

\pagebreak

\section{Quality Control}

To prove the quality of the code, the program must pass a set of tests:
\begin{enumerate}
\item unit tests;
\item functional tests;
\item integration tests;
\item user tests.
\end{enumerate}

\bigskip

In these tests, the robustness of the algorithms will be checked
by means of different input scenarios:
\begin{enumerate}
\item point clouds extracted from \emph{computer generated images};
\item point clouds extracted from \emph{real camera pictures}.
\end{enumerate}

\bigskip

There must be \emph{no regression in the quality} of the mesh.
In the case of virtual images, the result mesh can be
\emph{compared against the original mesh}.
If this is not applicable, the mesh can be \emph{compared against the
results obtained from the previous work}.
  
\end{document}
      
      
