Methods submitted to unit tests:

1: MaskData::contains
2: DelaunayReconstructor::triangulate
3: DelaunayReconstructor::tetrahedralize
4: DelaunayReconstructor::isInside
5: Compare3d::equals
6: Compare3d::operator()
7: Triangle::getBarycenter
8: Triangle::getNormal
9: PowercrustReconstructor::createVoronoiDiagram
10: PowercrustReconstructor::computeVoronoiPoles
11: PowercrustReconstructor::computePowerDiagram
12: PowercrustReconstructor::computeDINP
13: PowercrustReconstructor::labelVoronoiPoles


Test scenarios:

1.1: 
	ref: MD_CONTAINS_T1
	input: a vertex (Vector3d); a margin (int)
	description: the area computed from the parameter vertex and the margin is 
	totally inside the mask. The mask cannot be empty. 
	expected result: true is returned.
1.2: 
	ref: MD_CONTAINS_T2
	input: a vertex (Vector3d); a margin (int)
	description: the area computed from the parameter vertex and the margin is
	totally outside the mask. The mask cannot be empty. 
	expected result: false is returned.
1.3:
	ref: MD_CONTAINS_T3
	input: a vertex (Vector3d); a margin (int)
	description: the area computed from the parameter vertex is both inside and
	outside the mask. The mask cannot be empty. 
	expected result: true is returned.
1.4:
	ref: MD_CONTAINS_T4
	input: a vertex (Vector3d); a margin (int)
	description: the mask is empty. 
	expected result: false is returned.
1.5:
	ref: MD_CONTAINS_T5
	input: a vertex (Vector3d); a margin (int)
	description: the area computed from the parameter vertex is at the edge of
	the camera's visible area. The mask intersects the area.
	expected result: true is returned.
1.6:
	ref: MD_CONTAINS_T6
	input: a vertex (Vector3d); a margin (int)
	description: the area computed from the parameter vertex is at the edge of 
	the camera's visible area. The mask is empty.
	expected result: false is returned.
1.7:
	ref: MD_CONTAINS_T7
	input: a vertex (Vector3d); a margin (int)
	description: the vertex is behind the camera but within its angles of 
	sight. The projection of the vertex is within the mask.
	expected result: false is returned.
2.1:
	ref: DR_TRIANGULATE_T1
	input: a point cloud (PointCloud)
	description: a point cloud of 4 points (a tetrahedron) is triangulated.
	expected result: no point has been created; each point belongs to a face; 
	1 tetrahedron has been created.
2.2:
	ref: DR_TRIANGULATE_T2
	input: a point cloud (PointCloud)
	description: a point cloud of 8 points representing a (unit) cube is 
	triangulated.
	expected result: there is no face with twice the same point; there are no
	identical triangles; progress is displayed.
2.3:
	ref: DR_TRIANGULATE_T3
	input: a point cloud (PointCloud)
	description: a point cloud of 8 points representing a (unit) cube is 
	triangulated. The execution is canceled (via Progress) at the beginning
	of the program.
	expected result: the triangulate method terminates and the progress bar 
	stops.
3.1:
	ref: DR_TETRAHEDRALIZE_T1
	input: a point cloud (PointCloud)
	description: a point cloud of 4 points (a tetrahedron) is triangulated
	expected result: no point has been created; each point belongs to a face; 
	1 tetrahedron has been created.
3.2:
	ref: DR_TETRAHEDRALIZE_T2
	input: a point cloud (PointCloud)
	description: a point cloud of 8 points representing a (unit) cube is 
	triangulated.
	expected result: there is no face with twice the same point; there are no 
	identical triangles; progress is displayed.
3.3:
	ref: DR_TETRAHEDRALIZE_T3
	input: a point cloud (PointCloud)
	description: a point cloud of 8 points representing a (unit) cube is 
	triangulated. The execution is canceled (via Progress).
	expected result: the triangulate method terminates and the progress bar 
	stops.
4.1:
	ref: DR_ISINSIDE_T1
	input: a point (Vector3d)
	description: the area computed from the parameter point is inside all masks
	of MaskDataList.
	expected result: true is returned.
4.2:
	ref: DR_ISINSIDE_T2
	input: a point (Vector3d)
	description: the area computed from the parameter point is outside one mask
	of MaskDataList.
	expected result: false is returned.
5.1:
	ref: C3D_EQUALS_T1
	input: x = 1.0 (double); y = 2.0 (double)
	description: x and y are compared; |x - y| = 1.0 > 0.00000001.
	expected result: false is returned.
5.2:
	ref: C3D_EQUALS_T2
	input: x = 1.0 (double); y = 1.0 (double)
	description: x and y are compared; |x - y| = 0.0 <= 0.00000001.
	expected result: true is returned.
5.3:
	ref: C3D_EQUALS_T3
	input: x = 1.00000001 (double); y = 1.00000003 (double)
	description: x and y are compared; |x - y| = 0.00000002 > 0.00000001.
	expected result: false is returned.
5.4:
	ref: C3D_EQUALS_T4
	input: x = 1.000000010 (double); y = 1.000000019 (double)
	description: x and y are compared; |x - y| = 0.00000001 <= 0.00000001.
	expected result: true is returned.
5.5:
	ref: C3D_EQUALS_T5
	input: x = 1.000000001 (double); y = 1.000000003 (double)
	description: x and y are compared; |x - y| = 0.000000002 <= 0.00000001.
	expected result: true is returned.
6.1:
	ref: C3D_OPERATOR_T1
	input: u = (0, 2, 3) (Vector3d); v = (1, 4, 1) (Vector3d)
	description: u and v are compared; ux < vx.
	expected result: true is returned.
6.2:
	ref: C3D_OPERATOR_T2
	input: u = (0, 0, 3) (Vector3d); v = (0, 1, 1) (Vector3d)
	description: u and v are compared; ux = vx and uy < vy.
	expected result: true is returned.
6.3:
	ref: C3D_OPERATOR_T3
	input: u = (0, 0, 0) (Vector3d); v = (0, 0, 1) (Vector3d)
	description: u and v are compared; ux = vx and uy = vy and uz < vz.
	expected result: true is returned.
6.4:
	ref: C3D_OPERATOR_T4
	input: u = (1, 2, 3) (Vector3d); v = (0, 4, 1) (Vector3d)
	description: u and v are compared; ux > vx.
	expected result: false is returned.
6.5:
	ref: C3D_OPERATOR_T5
	input: u = (0, 1, 3) (Vector3d); v = (0, 0, 1) (Vector3d)
	description: u and v are compared; ux = vx and uy > vy.
	expected result: false is returned.
6.6:
	ref: C3D_OPERATOR_T3
	input: u = (0, 0, 1) (Vector3d); v = (0, 0, 0) (Vector3d)
	description: u and v are compared; ux = vx and uy = vy and uz > vz.
	expected result: false is returned.
6.7:
	ref: C3D_OPERATOR_T3
	input: u = (0, 0, 0) (Vector3d); v = (0, 0, 0) (Vector3d)
	description: u and v are compared; ux = vx and uy = vy and uz = vz.
	expected result: false is returned.
7.1
	ref: T_GETBARYCENTER_T1
	input: Triangle(Vector3d(0, 0, 0), Vector3d(0, 0, 0), Vector3d(0, 0, 0))
	(Triangle)
	description: the barycenter of the triangle is computed; the three vertices
	are identical.
	expected result: (0, 0, 0) is returned.
7.2
	ref: T_GETBARYCENTER_T2
	input: Triangle(Vector3d(3, 0, 0), Vector3d(0, 3, 0), Vector3d(0, 0, 3))
	(Triangle)
	description: the barycenter of the triangle is computed; the three vertices 
	are different.
	expected result: (1, 1, 1) is returned.
8.1
	ref: T_GETNORMAL_T1
	input: Triangle(Vector3d(0, 0, 0), Vector3d(0, 0, 0), Vector3d(0, 0, 0))
	(Triangle)
	description: the normal of the triangle is computed; the three vertices are
	identical.
	expected result: (0, 0, 0) is returned.
8.2
	ref: T_GETNORMAL_T2
	input: Triangle(Vector3d(0, 0, 0), Vector3d(1, 0, 0), Vector3d(0, 1, 0))
	(Triangle)
	description: the normal of the triangle is computed; the three vertices 
	define the (Oxy) plan.
	expected result: (0, 0, 1) is returned.
9.1
	ref: PR_CREATE_VORONOI_DIAGRAM_T1
	input: a point cloud representing a cube / a tetrahedron (PointCloud)
	description: createVoronoiDiagram is tested.
	expected result: for each cell, each voronoi vertex of the cell is 
	contained in the list of the adjacent voronoi vertices of the other 
	vertices of the cell.
9.2
	ref: PR_CREATE_VORONOI_DIAGRAM_T2
	input: a point cloud representing a cube / a tetrahedron (PointCloud)
	description: createVoronoiDiagram is tested.
	expected result: each cell must have at least one voronoi vertex
    and at least two voronoi edges.
10.1
	ref: PR_COMPUTE_VORONOI_POLES_T1
	input: a point cloud representing a cube / a tetrahedron (PointCloud)
	description: the post-conditions of computeVoronoiPoles are tested.
	expected result: the cosBeta of a cell is in the range [-1;0].
10.2
	ref: PR_COMPUTE_VORONOI_POLES_T2
	input: a point cloud representing a cube / a tetrahedron (PointCloud)
	description: the post-conditions of computeVoronoiPoles are tested.
	expected result: the two poles of a VoroVertex are different and the 
	negative pole is not -1.
10.3
	ref: PR_COMPUTE_VORONOI_POLES_T3
	input: a point cloud representing a cube / a tetrahedron (PointCloud)
	description: the post-conditions of computeVoronoiPoles are tested.
	expected result: the VoroVertices and their positive poles are correctly
	attached.
11.1
	ref: PR_COMPUTE_POWER_DIAGRAM_T1
	input: a point cloud representing a cube / a tetrahedron (PointCloud)
	description: computePowerDiagram is tested.
	expected result: the powers of each vorovertex must be > 0 and finite.
12.1
	ref: PR_COMPUTE_DINP_T1
	input: a point cloud representing a cube / a tetrahedron (PointCloud)
	description: computeDINP is tested.
	expected result: check that no pole is in the list twice.
12.2
	ref: PR_COMPUTE_DINP_T2
	input: a point cloud representing a cube / a tetrahedron (PointCloud)
	description: computeDINP is tested.
	expected result: check that any deeply intersecting pole belongs to the set
	of adjacent Voronoi vertices.
12.3
	ref: PR_COMPUTE_DINP_T3
	input: a point cloud representing a cube / a tetrahedron (PointCloud)
	description: computeDINP is tested.
	expected result: check that the poles are properly connected.
13.1
	ref: PR_LABEL_VORONOI_POLES_T1
	input: a point cloud representing a cube / a tetrahedron (PointCloud)
	description: labelVoronoiPoles is tested.
	expected result: each VoroVertex label must not be undefined and must be 
	labeled consistently with its in and out attributes.
13.2
	ref: PR_LABEL_VORONOI_POLES_T2
	input: a point cloud representing a cube / a tetrahedron (PointCloud)
	description: labelVoronoiPoles is tested.
	expected result: the two poles of a cell must have opposite labels.
13.3
	ref: PR_LABEL_VORONOI_POLES_T3
	input: a point cloud representing a cube / a tetrahedron (PointCloud)
	description: labelVoronoiPoles is tested.
	expected result: there exists a voronoi vertex that is labelled IN.