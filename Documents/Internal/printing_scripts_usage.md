Contents of printing scripts
============================

The `impression` directory contains Blender scripts in the directory `scripts` to preprocess the mesh before printing it.

In this directory the Bash script `addons/run_tests.sh` runs regularity tests on some 3D models. To do that it calls the Python script `addons/ultimaker/tests/all.py` which in turn calls all Python scripts inside its directory. Among those scripts, only `addons/ultimaker/modules/manifold.py` and `addons/ultimaker/modules/planar_faces.py` actually run tests. The other files are just for defining modules.

`manifold.py` loads `model/happy_vrip_res4.ply`, fills the holes and checks that it is manifold. The main function for that is `correction(destructive, fast_processing)` which calls `fill_and_check(edges, destructive, fast_processing, old_nb_edges, pt)`. It uses for instance the function `get_holes(edges)` to detect holes. The parameters of each function is documented.

* `edges` is the set of edges to check in the Blender format
* `desctructive` is a boolean to allow the method to supress some vertices in order to obtain a full manifold object. The run tests have this parameter enabled.
* `fast_processing` is a boolean to allow fast-processing which can result in bad accurate meshes. The run tests have this parameter disabled.
* `old_nb_edges` is just used for recursivity
* `pt` is the progress text. (It is just to display a message.)

GUI addon
---------

The script `startup/printing_preprocessing.py` provides a graphical interface tool to preprocess a mesh before printing it.

The function `register()` is just used to integrate all the defined panel classes into Blender interface.

The class `MeshVerificationPanel` defines the graphical elements of the tool panel. The functions called when clicking on its buttons are the `execute` methods of the classes: `CheckMeshOperator`, `ChooseCurrentPlaneOperator`, `ChooseSelectedFacesOperator`, `CutObjectUnderBasePlaneOperator`, `DestructiveManifoldWatertightOperator`, `FindSupportingPlanesOperator`, `GenerateSocleOperator`, `NonDestructiveManifoldWatertightOperator`, `VizualizeNextPlaneOperator`, `VizualizePreviousPlaneOperator`.

The tool is divided in three steps. At the start only the button `Check your mesh` is visible and the three steps are greyed out. This button checks if the mesh is manifold. If so, the first step is skipped and the step 2 is directly activated.

The only methods useful to _clean a mesh are the `execute` methods of `CheckMeshOperator` and `[Non]DestructiveManifoldWatertightOperator`.

Custom scripts to use scripts of the older project without a specific Blender startup file
==========================================================================================

Launch Blender, create a model, select it and load one of these scripts:

* `manifold_in_blender.py` to test the manifoldness of a model
* `import_GUI_tools.py` to show the printing preprocessing graphical tools

In those files, you will have to change a line to set the right path for modules.

