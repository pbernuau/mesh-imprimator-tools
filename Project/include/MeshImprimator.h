#ifndef MESHIMPRIMATOR_H
#define MESHIMPRIMATOR_H

#include <MeshImprimator/Config.h>
#include <MeshImprimator/Geometry.h>
#include <MeshImprimator/Algorithms.h>
#include <MeshImprimator/IO.h>

#endif // MESHIMPRIMATOR_H
