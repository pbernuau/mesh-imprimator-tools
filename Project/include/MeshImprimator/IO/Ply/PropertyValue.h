#ifndef PROPERTYVALUE_H
#define PROPERTYVALUE_H

#include <MeshImprimator/Config.h>

namespace Ply
{

union PropertyValue
{
    signed char     asChar;
    signed short    asShort;
    signed int      asInt;

    unsigned char   asUChar;
    unsigned short  asUShort;
    unsigned int    asUInt;

    float           asFloat;
    double          asDouble;
};

}

#endif // PROPERTYVALUE_H
