#ifndef ELEMENTINSTANCE_H
#define ELEMENTINSTANCE_H

#include <MeshImprimator/Config.h>
#include <MeshImprimator/IO/Ply/Exceptions.h>
#include <MeshImprimator/IO/Ply/PropertyValue.h>

class PLYReader;

namespace Ply
{

class ElementClass;

/*!
 * \brief The ElementInstance class.
 *
 * Represents one of the entry listed in a PLY class.
 *
 * For instance, an ElementInstance of the class `vertex` may be
 *
 *     { x: 1.0; y: 2.0; z: 3.0 }
 */
class MESH_IMPRIMATOR_API ElementInstance
{
    ElementInstance(const ElementClass* elementClass, int index);
public:
    /*!
     * \brief Copy constructor.
     *
     * \param other
     */
    ElementInstance(const ElementInstance& other);

    /*!
     * \brief Retrieves the value associated to the given property.
     *
     * \param property A property of the class
     * \return The property value
     * \throw ClassMismatchException The element and the property are not bound
     * to the same class.
     */
    PropertyValue operator[](const Property& property) const;

    /*!
     * \brief Overload provided for convenience.
     *
     * This method is provided for convenience. Given a variable of type
     * ElementInstance, the following lines are equivalent:
     *
     *     double ptX = pt[vertexClass.property("x")].asDouble;
     *
     *     double ptX = pt["x"].asDouble;
     *
     * However, it is more efficient to fetch the property once and reuse it.
     * This class get<T>() method may be more convenient because it also
     * casts the returned value.
     *
     * \param property The name of the property
     * \return The property value
     * \throw NoSuchPropertyException No property with this name exists in the
     * class
     */
    PropertyValue operator[](const std::string& property) const;

    /*!
     * \brief Retrieves the value associated with the property and casts it.
     *
     * This method will interpret the value given the type of the property
     * and cast it to the given type template parameter. It means that
     * even if the file defined the property as an integer, a double may
     * be returned with proper conversion.
     *
     * The values are cast with `static_cast`. An overflow may occur if the
     * return type you specified has fewer bits than the value stored.
     *
     * Example:
     *
     *     ElementInstance pt = myClass.get(0);
     *     Property x = myClass.property("x");
     *     double ptX = pt.get<double>(x);
     *
     * \param property A property of the class
     * \return The property value cast to the template type parameter
     * \throw ClassMismatchException See ElementInstance::operator []()
     * \throw UnknownPropertyTypeException Thrown if the type of the property
     * could not be interpreted (e.g., the type is not implemented)
     *
     * \todo Improve overflow handling
     */
    template<typename T>
    T get(const Property& property) const
    {
        PropertyValue value = (*this)[property];
        switch (property.getType())
        {
        case Property::T_CHAR:      return static_cast<T>(value.asChar);
        case Property::T_SHORT:     return static_cast<T>(value.asShort);
        case Property::T_INT:       return static_cast<T>(value.asInt);
        case Property::T_UCHAR:     return static_cast<T>(value.asUChar);
        case Property::T_USHORT:    return static_cast<T>(value.asUShort);
        case Property::T_UINT:      return static_cast<T>(value.asUInt);
        case Property::T_FLOAT:     return static_cast<T>(value.asFloat);
        case Property::T_DOUBLE:    return static_cast<T>(value.asDouble);
        default:                    throw UnknownPropertyTypeException();
        }
    }

private:
    const ElementClass* elementClass;
    int index;

    friend class ::PLYReader;
    friend class Object;
    friend class ElementClass;
    friend class Property;
};

}

#endif // ELEMENTINSTANCE_H
