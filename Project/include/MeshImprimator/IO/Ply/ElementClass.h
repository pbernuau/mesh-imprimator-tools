#ifndef ELEMENTCLASS_H
#define ELEMENTCLASS_H

#include <MeshImprimator/Config.h>
#include <MeshImprimator/IO/Ply/PropertyValue.h>

#include <list>
#include <ostream>
#include <string>
#include <vector>

class PLYReader;

namespace Ply
{

class ElementInstance;
class Property;

/*!
 * \brief The ElementClass class.
 *
 * This class represents one of the section listed in the PLY file. An
 * ElementClass holds several properties and elements that are instances of
 * the ElementClass.
 *
 * For instance, the class name may be `vertex` and have three properties `x`,
 * `y` and `z`.
 *
 * \todo Add a method to test the existence of a property
 */
class MESH_IMPRIMATOR_API ElementClass
{
    ElementClass(const std::string& name, int elementCount);
    ~ElementClass();
public:
    std::string getName() const;

    /*!
     * \brief Retrieves one of this class' property.
     *
     * Returns a Property defined in this class whose name matches the given
     * name. Even if two classes define the same property name with the same
     * type, Property instances must not be shared between classes.
     *
     * If there is no property with the given name, a NoSuchPropertyException
     * is thrown.
     *
     * \param name The name of the property
     * \return The property
     * \throw NoSuchPropertyException No property with this name exists
     */
    const Property& property(const std::string& name) const;

    /*!
     * \brief Retrieves an instance of this class.
     *
     * Returns an ElementInstance contains by this class given its \a index.
     * The index must be in the range `[ 0, getElementCount() ]` or a
     * NoSuchElementInstanceException will be thrown.
     *
     * Since the elements are stored sequentially in memory, the method returns
     * the element in constant time.
     *
     * \param index The index of the element
     * \return The element
     * \throw NoSuchElementInstanceException There is no element with the given
     * index
     */
    ElementInstance get(int index) const;

    /*!
     * \brief Returns the number of elements in this class.
     *
     * \return The element count
     */
    int getElementCount() const;

    /*!
     * \brief Prints debugging information.
     *
     * \param stream The output stream
     */
    void print(std::ostream& stream) const;

private:
    std::string name;
    std::list<Property*> properties;
    std::vector<PropertyValue> values;

    int elementCount;

    void clear();

    friend class ::PLYReader;
    friend class Object;
    friend class ElementInstance;
    friend class Property;
};

}

#endif // ELEMENTCLASS_H
