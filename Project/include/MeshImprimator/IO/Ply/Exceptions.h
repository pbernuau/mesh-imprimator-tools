#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <MeshImprimator/Config.h>

namespace Ply
{

/*!
 * \brief The base class for all exceptions thrown by a Ply::Object.
 *
 */
class Exception : public std::exception {};
class UnknownPropertyTypeException : public Exception {};
class NoSuchPropertyException : public Exception {};
class NoSuchElementClassException : public Exception {};
class NoSuchElementInstanceException : public Exception {};
class ClassMismatchException : public Exception {};

}

#endif // EXCEPTIONS_H
