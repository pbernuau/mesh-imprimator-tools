#ifndef OBJECT_H
#define OBJECT_H

#include <MeshImprimator/Config.h>

#include <ostream>
#include <string>
#include <vector>

class PointCloud;
class PLYReader;

namespace Ply
{

class ElementClass;

/*!
 * \brief Describes a PLY Object file.
 *
 * A PLY Object is composed of several classes, e.g. `vertex`, `face`, etc.
 * Each class contains different properties of any type and the values for
 * each entry in the file.
 *
 * A PLY Object cannot be instanciated directly. You may retrieve an instance
 * with the PLYReader class.
 *
 * \todo Add a method to test a class existence.
 */
class MESH_IMPRIMATOR_API Object
{
    Object(int reserve = 4);
    ~Object();
public:
    /*!
     * \brief Gets a PLY class by its name.
     *
     * Returns an ElementClass whose name is the given name or throw a
     * NoSuchElementClassException.
     *
     * \param className The name of the class
     * \return The class
     * \throw NoSuchElementClassException No class with this name exists
     */
    const ElementClass& get(const std::string& className) const;

    /*!
     * \brief Computes a point cloud from the given PLY object.
     *
     * This method is provided for convenience. It reads the object and computes
     * a cloud point.
     *
     * It assumes that the object contains a class named `vertex` which has at
     * least three properties: `x`, `y` and `z`. Otherwise, the corresponding
     * exceptions will be thrown. If there are other classes or properties,
     * they will be silently ignored.
     *
     * \return The computed point cloud
     * \throw NoSuchClassException There is no class named `vertex`
     * \throw NoSuchPropertyException A property `x`, `y` or `z` is missing
     */
    PointCloud getPointCloud() const;

    /*!
     * \brief Prints debugging information.
     *
     * Prints the object header information to the given stream. The different
     * classes and their properties will be listed in the order they were read.
     *
     * \param stream The ouput stream
     */
    void print(std::ostream& stream) const;

private:
    std::vector<ElementClass*> classes;

    void clear();

    friend class ::PLYReader;
    friend class ElementClass;
    friend class ElementInstance;
    friend class Property;
};

}

#endif // OBJECT_H
