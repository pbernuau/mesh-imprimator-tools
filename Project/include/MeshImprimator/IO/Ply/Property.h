#ifndef PROPERTY_H
#define PROPERTY_H

#include <MeshImprimator/Config.h>

#include <string>
#include <ostream>

class PLYReader;

namespace Ply
{

class ElementClass;

/*!
 * \brief The Property class.
 *
 * A property of a class is defined by a name and a type. It is bound to the
 * class in which it was defined, thus properties cannot be used with instances
 * from another class.
 */
class MESH_IMPRIMATOR_API Property
{
public:
    /*!
     * \brief The Type enum.
     *
     * Defines a numeric constant for every PLY types.
     *
     * \see PropertyValue
     */
    enum Type
    {
        T_CHAR,     //!< 1 byte, signed
        T_SHORT,    //!< 2 bytes, signed
        T_INT,      //!< 4 bytes, signed

        T_UCHAR,    //!< 1 byte, unsigned
        T_USHORT,   //!< 2 bytes, unsigned
        T_UINT,     //!< 4 bytes, unsigned

        T_FLOAT,    //!< 4 bytes, simple precision floating point
        T_DOUBLE,   //!< 8 bytes, double precision floating point

        T_LIST,     //!< Indexed lists are not supported
        T_UNKNOWN,  //!< Unknown type
    };

    /*!
     * \brief Copy constructor.
     *
     * \param other
     */
    Property(const Property& other);

    std::string getName() const;
    Type getType() const;

    void print(std::ostream& stream) const;

private:
    const ElementClass* ownerClass;
    int offset;
    std::string name;
    Type type;

    Property(const ElementClass* ownerClass,
             int offset,
             const std::string& name,
             Property::Type type);

    friend class ::PLYReader;
    friend class Object;
    friend class ElementClass;
    friend class ElementInstance;
};

}

#endif // PROPERTY_H
