#ifndef PLYREADER_H
#define PLYREADER_H

#include <MeshImprimator/Config.h>
#include <MeshImprimator/IO/Ply/Property.h>

#include <exception>
#include <fstream>
#include <map>
#include <string>

class PointCloud;

namespace Ply
{
class Object;
class ElementClass;
union PropertyValue;
}

class PLYReaderException;

/*!
 * \brief The PLYReader class.
 *
 * A PLYReader class allows you to read an object from a polygon file (PLY). A
 * description of the format is available here:
 *
 * http://paulbourke.net/dataformats/ply/
 *
 * The PLYReader does not support all of the specifications:
 * -    binary files, both little and big endian, are not supported
 * -    indexed properties are not supported, but the reader will ignore them
 *      silently
 *
 * There are commonly used type aliases that are not mentionned in the
 * format description but are implemented: `(u)int8/16/32` and `float32/64`.
 *
 * To use the class, call open(), readHeader(), readContent() and close(). These
 * methods will construct an internal 3d object that may be retrieved with
 * the getObject() method.
 * It is worth to note that the header and the content may be in separate files.
 * In that case, you must open the content file after readHeader() and before
 * readContent().
 *
 * Alternatively, you may use the all-in-one read() method provided for
 * convenience.
 */
class MESH_IMPRIMATOR_API PLYReader
{
public:
    /*!
     * \brief The ErrorCode enum.
     *
     * An error code returned by readHeader() or readContent().
     */
    enum ErrorCode
    {
        NO_ERROR                = 0, //!< Returned on success
        INPUT_FILE_IS_NOT_OPEN  = 1, //!< open() not called or failed
        MISSING_PLY_SIGNATURE   = 2, //!< The file does not start with `PLY`
        MISSING_PLY_FORMAT      = 3, //!< The file does not define its format
        UNSUPPORTED_PLY_FORMAT  = 4, //!< The file uses an unsupported format
        UNSUPPORTED_PLY_VERSION = 5, //!< The file uses an unsupported version
        PREMATURE_END_OF_FILE   = 6, //!< The file is troncated
        INVALID_COMMAND         = 7, //!< A command could not be parsed

        HEADER_NOT_LOADED       = 8, //!< Missing readHeader() before readContent()
        INVALID_ELEMENT         = 9, //!< A property of an element could not be read
    };

    /*!
     * \brief Default constructor.
     *
     */
    PLYReader();

    /*!
     * \brief Destructor.
     *
     * The destructor will be automatically clear()ed and close()d. Any object
     * retrieved will be freed.
     */
    ~PLYReader();

    /*!
     * \brief Opens a file for reading.
     *
     * Attempts to open the given filename for reading. No data will be read
     * on success.
     *
     * This method does not throw any PLYReaderException.
     *
     * \param filename A path to a PLY file
     * \return true if the file is readable, false otherwise
     */
    bool open(const std::string& filename);

    /*!
     * \brief Reads a PLY header from the open file.
     *
     * Attempts to read a PLY reader from the previously open file. Any object
     * previously loaded will be cleared and become invalid.
     * If provided, the `errorCode` and `line` parameters will be populated
     * with relevant information.
     *
     * On failure, no data is stored from the invalid header.
     *
     * This method does not throw any PLYReaderException.
     *
     * \param errorCode See \ref ErrorCode for a list of possible values
     * \param line The line number at which the parsing stopped
     * \return true if the header has been successfully read, false otherwise
     */
    bool readHeader(int* errorCode = NULL, int* line = NULL);

    /*!
     * \brief Reads a PLY object from the open file given its header
     * information.
     *
     * Attempts to read the object content from the previously open file. The
     * header must be already loaded with readHeader(), otherwise, the method
     * will fail.
     * It is possible to read the header and the content from different files.
     * If provided, the `errorCode` and `line` parameters will be populated
     * with relevant information.
     *
     * On success, you may retrieve the object with the getObject() method.
     * On failure, the whole object will be freed, even if the header was
     * successfully read.
     *
     * This method does not throw any PLYReaderException
     *
     * \param errorCode See \ref ErrorCode for a list of possible values
     * \param line The line number at which the parsing stopped.
     * \return  true if the content has been successfully read, false otherwise
     */
    bool readContent(int* errorCode = NULL, int* line = NULL);

    /*!
     * \brief Closes the open file if any.
     *
     * Closes the file opened with open() if any or does nothing. It is not
     * necessary to call this method. The file object will be automatically
     * closed when this instance is destroyed.
     */
    void close();

    /*!
     * \brief Clears the inner object.
     *
     * Frees the inner object loaded with a previous call to readHeader(),
     * possibly followed by a call to readContent().
     * Any pointer retrieved with getObject() will become invalid.
     */
    void clear();

    /*!
     * \brief Indicates whether a file is open for reading.
     *
     * This method may be used to verify that a call to open() succeeded.
     *
     * \return true if the file is open, false otherwise
     */
    bool isOpen() const;

    /*!
     * \brief Returns a constant reference to the parsed object.
     *
     * This method may be invoked after a call to readHeader() and/or a call
     * to readContent(). Returns the inner object holding the information that
     * has been read so far. If nothing has been read, returns NULL.
     *
     * You may use the returned object after a call to readHeader() to display
     * the object's properties information before they are actually read:
     *
     *     plyReader.read("cube.ply");
     *     if (plyReader.readHeader())
     *         plyReader.getObject()->print(std::cout);
     *     else
     *         return -1;
     *
     * Two calls to getObject() before and after readContent() may return the
     * same object address but you should not rely on this. The reference may
     * remain valid as long as no other method is invoked.
     *
     * \return A full object or a partially loaded object
     */
    const Ply::Object* getObject() const;

    /*!
     * \brief All-in-one convenience method to read a point cloud from a
     * PLY file.
     *
     * This method clears any previously loaded data. This method will attempt
     * to open the file and read its header and content. The file will be
     * closed afterwards.
     * On success, the object is retrieved and the associated point cloud is
     * fetched. You should look at the Ply::Object::getPointCloud() method to
     * see how the cloud will be loaded.
     * On failure, an empty point cloud is returned.
     *
     * This method will not throw any PLYReaderException except the ones
     * thrown by Ply::Object::getPointCloud().
     *
     * \param filename The name of the PLY file that contains the point cloud
     * \return The point cloud
     */
    PointCloud read(const std::string& filename);

private:
    std::map<std::string, Ply::Property::Type> typeIdentifiers;

    Ply::Object* object;
    std::ifstream file;
    int lineNumber;

    std::string readLine();
    void readSignature();
    void readFormat();
    void readInstances(Ply::ElementClass* elementClass);
    Ply::PropertyValue readInstance(std::istream& iss,
                                    const Ply::Property* property);

    Ply::Property::Type getType(const std::string& typeId) const;
};

class MESH_IMPRIMATOR_API PLYReaderException : public std::exception
{
public:
    PLYReaderException(int code);

    const int code;
};

#endif // PLYREADER_H
