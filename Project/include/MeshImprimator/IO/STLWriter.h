#ifndef STLWRITER_H
#define STLWRITER_H

#include <MeshImprimator/Config.h>

#include <string>

class TriangleMesh;

/*!
 * \brief The STLWriter class
 *
 * Write the mesh triangle in a STL format file.
 */
class MESH_IMPRIMATOR_API STLWriter
{
public:
    /*!
     * \brief The Status enum
     */
    enum Status
    {
        SUCCESS, //!< The output file has been successfully written
        IO_ERROR, //!< Input/Output related error
    };

    STLWriter();

    Status getStatus()
    {
        return status;
    }

    /*!
     * \brief write
     *
     * Write the mesh triangle in a STL format file.
     * \param mesh The triangle mesh to write
     * \param outputFilename The output name of the STL file
     * \param meshName The name of the mesh
     */
    void write(const TriangleMesh& mesh,
               const std::string& outputFilename,
               const std::string& meshName);

private:
    Status status;
};


#endif // STLWRITER_H
