#ifndef MESHIMPRIMATOR_GEOMETRY_H
#define MESHIMPRIMATOR_GEOMETRY_H

#include <MeshImprimator/Geometry/Compare3d.h>
#include <MeshImprimator/Geometry/IndexedTriangle.h>
#include <MeshImprimator/Geometry/IndexedTriangleMesh.h>
#include <MeshImprimator/Geometry/PointCloud.h>
#include <MeshImprimator/Geometry/Triangle.h>
#include <MeshImprimator/Geometry/TriangleMesh.h>

#endif // MESHIMPRIMATOR_GEOMETRY_H
