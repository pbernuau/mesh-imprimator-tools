#ifndef IPROGRESS_H
#define IPROGRESS_H

#include <MeshImprimator/Config.h>

/*!
 * \brief The IProgress class
 *
 *  An object to keep track of the progress of an algorithm.
 *
 */


class MESH_IMPRIMATOR_API IProgress
{
public:
    virtual ~IProgress() {}

    /*!
     * \brief setValue
     *
     * Assigns a value representing the progress of an algorithm in percentage.
     * The value should be in the range specified by setRange().
     *
     * \param value The value (percentage) to be assigned for the progress
     */
    virtual void setValue(int value) = 0;

    /*!
     * \brief setRange
     *
     * Defines the lower and the upper value of the progress.
     *
     * \param min the min value of the range
     * \param max the max value of the range
     */
    virtual void setRange(int min, int max) = 0;

    /*!
     * \brief wasCanceled
     *
     * Indicates whether the algorithm was stopped by the user or not.
     * The caller should return in that case.
     *
     * \return true if the algorithm was stopped by the user
     */
    virtual bool wasCanceled() const = 0;
};

#endif // IPROGRESS_H
