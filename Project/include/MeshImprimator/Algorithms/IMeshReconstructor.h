#ifndef IMESHRECONSTRUCTOR_H
#define IMESHRECONSTRUCTOR_H

#include <MeshImprimator/Config.h>

class TriangleMesh;
class IndexedTriangleMesh;

class IProgress;

/*!
 * \brief The IMeshReconstructor class
 *
 * Interface for the reconstruction of the mesh from the point cloud.
 */
class MESH_IMPRIMATOR_API IMeshReconstructor
{
public:
    virtual ~IMeshReconstructor() {}
    /*!
     * \brief reconstruct
     *
     * Reconstructs the unindexed mesh by using one of the following methods :
     * the Delaunay reconstruction or the PowerCrust or the Poisson
     * reconstruction.
     * \param progress An object to keep track of the process of the algorithm
     * \return The triangle mesh
     */
    virtual TriangleMesh reconstruct(IProgress* progress) = 0;
    /*!
     * \brief reconstructIndexed
     *
     * Reconstructs the indexed mesh by using one of the following methods :
     * the Delaunay reconstruction or the PowerCrust or the Poisson
     * reconstruction.
     * \param progress An object to keep track of the process of the algorithm
     * \return The indexed triangle mesh
     */
    virtual IndexedTriangleMesh reconstructIndexed(IProgress* progress) = 0;
};


#endif // IMESHRECONSTRUCTOR_H
