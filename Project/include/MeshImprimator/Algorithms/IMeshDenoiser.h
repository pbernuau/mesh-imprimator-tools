#ifndef IMESHDENOISER_H
#define IMESHDENOISER_H

#include <MeshImprimator/Config.h>

class TriangleMesh;
class IndexedTriangleMesh;

class IProgress;

/*!
 * \brief The IMeshDenoiser class
 *
 * Interface for the denoising of the mesh.
 */
class MESH_IMPRIMATOR_API IMeshDenoiser
{
public:
    virtual ~IMeshDenoiser() {}
    /*!
     * \brief denoise
     *
     * Denoises the indexed triangle mesh.
     * \param progress An object to keep track of the progress of the algorithm
     * \return The unindexed denoised mesh
     */
    virtual TriangleMesh denoise(IProgress* progress) = 0;
    /*!
     * \brief denoiseIndexed
     *
     * Denoises the indexed triangle mesh.
     * \param progress An object to keep track of the progress of the algorithm
     * \return The indexed denoised mesh
     */
    virtual IndexedTriangleMesh denoiseIndexed(IProgress* progress) = 0;
};

// TODO
typedef IMeshDenoiser IDenoiser; // deprecated

#endif // IMESHDENOISER_H
