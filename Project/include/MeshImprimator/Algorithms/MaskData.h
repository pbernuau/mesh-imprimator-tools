#ifndef MASKDATA_H
#define MASKDATA_H

#include <MeshImprimator/Config.h>

/*!
 * \brief The MaskData struct
 *
 * Data containing the mask of an image and the camera model from which the
 * image was taken.
 */
struct MESH_IMPRIMATOR_API MaskData
{
    typedef Eigen::Matrix<double, 3, 4, Eigen::DontAlign> CameraMatrix;
    typedef Eigen::Matrix<signed char, Eigen::Dynamic, Eigen::Dynamic> Mask;

    /*!
     * \brief camera
     *
     * 4*4 matrix modeling a perspective camera
     */
    CameraMatrix camera;

    /*!
     * \brief mask
     *
     * Matrix composed of 0 (the object is absent) and strictly positive values
     * (the object is present)
     */
    Mask mask;

    /*!
     * \brief Determines if a point is projecting into the mask
     *
     * The point is projected using the camera matrix. The result coordinates
     * are then used to compute an area surrounding the point. If the mask
     * and the area intersect, the point is inside.
     *
     * \param point The point to project in the mask
     * \param margin The number of pixels between the point and one side of the
     * square area
     * \return true if the projection is inside the mask
     */
    bool contains(const Vector3d& point, int margin) const;

    /*!
     * \brief Check the validity of the mask
     *
     * \return True if the mask is not empty
     */
    bool isValid() const;
};

#endif // MASKDATA_H
