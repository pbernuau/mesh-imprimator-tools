#ifndef POWERCRUSTRECONSTRUCTOR_H
#define POWERCRUSTRECONSTRUCTOR_H

#include <MeshImprimator/Config.h>
#include <MeshImprimator/Geometry/IndexedTriangleMesh.h>
#include <MeshImprimator/Geometry/PointCloud.h>
#include <MeshImprimator/Algorithms/IMeshReconstructor.h>

class TriangleMesh;
class IProgress;
class VoroData;

/*!
 * \brief The PowercrustReconstructor class
 *
 * Uses the Amenta's Powercrust algorithm to reconstruct an object surface from
 * a point cloud. Unlike the Delaunay reconstructor, it does not need any mask.
 */
class MESH_IMPRIMATOR_API PowercrustReconstructor : public IMeshReconstructor
{
public:
    /*!
     * \brief PowercrustReconstructor
     *
     * \param pointCloud The point cloud
     */
    PowercrustReconstructor(const PointCloud& pointCloud);
    virtual ~PowercrustReconstructor();

    /*!
     * \brief reconstruct
     *
     * Reconstructs the unindexed mesh by using the Powercrust algorithm
     * \param progress An object to keep track of the progress of the algorithm
     * \return The unindexed triangle mesh
     */
    virtual TriangleMesh reconstruct(IProgress* progress);

    /*!
     * \brief reconstructIndexed
     *
     * Reconstructs the indexed mesh by computing using the Powercrust algorithm
     * \param progress An object to keep track of the progress of the algorithm
     * \return The indexed triangle mesh
     */
    virtual IndexedTriangleMesh reconstructIndexed(IProgress* progress);

#ifdef MI_DEBUG

    inline const VoroData* debugVoroData() const
    {
        return voroData;
    }

#endif

private:
    PointCloud pointCloud;

    IndexedTriangleMesh triangleMesh;
    VoroData* voroData;

    void clear();
    bool createVoronoiDiagram(IProgress* progress);
    bool computeVoronoiPoles(IProgress* progress);
    bool computePowerDiagram(IProgress* progress);
    bool computeDINP(IProgress* progress);
    bool labelVoronoiPoles(IProgress* progress);
    bool filterDelaunayFaces(IProgress* progress);
};

#endif // POWERCRUSTRECONSTRUCTOR_H
