#ifndef FEATUREPRESERVINGDENOISER_H
#define FEATUREPRESERVINGDENOISER_H

#include <MeshImprimator/Config.h>
#include <MeshImprimator/Geometry/IndexedTriangle.h>
#include <MeshImprimator/Algorithms/IMeshDenoiser.h>

#include <list>
#include <map>
#include <set>
#include <vector>

class IndexedTriangleMesh;
class TriangleMesh;

class IProgress;

/*!
 * \brief The FeaturePreservingDenoiser class
 *
 * Uses a feature preserving denoising algorithm to denoise the mesh. This
 * method consists in correcting the normals of the mesh triangles to obtain
 * a smoother mesh. The normal modifications are then propagated on the
 * vertices. A threshold parameter allows the preservation of more or less
 * sharp edges.
 */
class MESH_IMPRIMATOR_API FeaturePreservingDenoiser : public IDenoiser
{
public:
    /*!
     * \brief FeaturePreservingDenoiser
     *
     * \param mesh A noisy indexed triangle mesh
     * \param threshold A threshold to determine whether a sharp edge has to
     * be conserved
     * \param nbIterNormal A number of iteration for the normal updating
     * \param nbIterVertex A number of iteration for the vertex updating
     */
    FeaturePreservingDenoiser(
            const IndexedTriangleMesh& mesh,
            const double threshold = 0.4,
            const int nbIterNormal = 20,
            const int nbIterVertex = 50);
    virtual ~FeaturePreservingDenoiser();
    /*!
     * \brief denoise
     *
     * Denoises the indexed triangle mesh with the feature preserving denoising
     * algorithm. The normals of the mesh are computed and corrected to make
     * a smoother mesh. The vertices are then updated to adapt to the new
     * normals.
     * This method uses denoiseIndexed() first to create the indexedMesh and
     * then, from it, the unindexed mesh is built.
     * \param progress An object to keep track of the progress of the algorithm
     * \return The unindexed denoised mesh
     */
    virtual TriangleMesh denoise(IProgress* progress);
    /*!
     * \brief denoise
     *
     * Denoises the indexed triangle mesh with the feature preserving denoising
     * algorithm. The normals of the mesh are computed and corrected to make
     * a smoother mesh. The vertices are then updated to adapt to the new
     * normals.
     * \param progress An object to keep track of the progress of the algorithm
     * \return The indexed denoised mesh
     */
    virtual IndexedTriangleMesh denoiseIndexed(IProgress* progress);

private:
    double threshold;
    int nbIterNormal;
    int nbIterVertex;
    int nbVertices;
    int nbTriangles;
    std::vector<IndexedTriangle> triangles;
    // normals[i] is the normal of the ith triangle
    std::vector<Vector3d> normals;
    std::vector<Vector3d> vertices;
    // nFs[i] is the list of the ith triangle's neighboring triangles indices
    std::vector< std::set<int> > nFs;
    // nV[i] is the set of vertex indices that are connected to the ith vertex
    // by an edge
    std::vector< std::set<int> > nV;
    // cardFV[i] is the cardinal of the set of triangles that share the ith
    // vertex as a common vertex
    std::vector<int> cardFV;
    // edgeTrianglesMap[i][j] is the list of triangles indices that contain
    // the ith and jth vertices (that is, contain the <i, j> edge)
    std::vector< std::map<int, std::list<int> > > edgeTrianglesMap;

    /*!
     * \brief computeNormals
     *
     * Computes attribute normals, containing for each face of the mesh the
     * normal vector.
     */
    void computeNormals();
    /*!
     * \brief computeNFs
     *
     * Computes attribute nFs, containing for each face of the mesh the set of
     * indices of its neighbors (faces sharing an edge with it). If
     * normalsMakeConsistent is true, it will at the same time change the
     * orientation of faces so that the normals of neighbor faces will be
     * oriented to the same side.
     * \param normalsMakeConsistent Boolean to specify if faces need to be
     * reoriented for consistency of normals
     */
    void computeNFs(bool normalsMakeConsistent = true);
    /*!
     * \brief recComputeNFs
     *
     * Recursive function used by computeNFs(). Computes neighbors of faces in
     * toProcess, that are connex (in the sense of neighbors) to
     * triangles[triangleId]. If normalsMakeConsistent is true, it will at the
     * same time change the orientation of faces so that the normals of neighbor
     * faces will be oriented to the same side.
     * \param triangleId Index of the face by which the propagative search
     * starts.
     * \param toProcess Set of face indices in which the search of neighbors
     * is made. It doesn't need to contain triangleId
     * \param normalsMakeConsistent Boolean to specify if faces need to be
     * reoriented for consistency of normals
     */
    void recComputeNFs(int triangleId,
                       std::set<int> &toProcess,
                       bool normalsMakeConsistent = true);
    /*!
     * \brief computeNVCardFV
     *
     * Computes attributes nV and cardFv. nV is such that nV[i] is the set of
     * vertex indices that are connected to the ith vertex by an edge. cardFV is
     * such that cardFV[i] is the cardinal of the set of triangles that share
     * the ith vertex as a common vertex.
     */
    void computeNVCardFV();
    /*!
     * \brief computeEdgeTrianglesMap
     *
     * Computes attribute edgeTrianglesMap. edgeTrianglesMap is such that
     * edgeTrianglesMap[i][j] is the list of triangles indices that contain the
     * ith and jth vertices (that is, contain the <i, j> edge)
     */
    void computeEdgeTrianglesMap();
    /*!
     * \brief updateNormals
     *
     * Updates the normals iteratively. The sharp edges are conserved depending
     * on the attribute threshold.
     * \param progress An object to keep track of the progress of the algorithm
     */
    void updateNormals(IProgress* progress);
    /*!
     * \brief updateVertices
     *
     * Updates the vertices iteratively. The vertex updating allows the vertices
     * to fit the normal updating. The sharp edges are conserved depending on
     * the threshold attribute.
     * \param progress An object to keep track of the progress of the algorithm
     */
    void updateVertices(IProgress* progress);
    /*!
     * \brief edgeInTriangle
     *
     * Checks whether the edge made of vertices \a i and \a j is inside the
     * \a triangle.
     * This method uses vertexInTriangle().
     * \param i A vertex index
     * \param j A vertex index
     * \param triangle An indexed triangle
     */
    bool edgeInTriangle(int i, int j, IndexedTriangle triangle);
    /*!
     * \brief vertexInTriangle
     *
     * Checks whether the vertex \a i is inside the \a triangle.
     * \param i A vertex index
     * \param triangle An indexed triangle
     */
    bool vertexInTriangle(int i, IndexedTriangle triangle);
};


#endif // FEATUREPRESERVINGDENOISER_H
