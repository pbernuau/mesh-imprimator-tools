#ifndef DELAUNAYRECONSTRUCTOR_H
#define DELAUNAYRECONSTRUCTOR_H

#include <MeshImprimator/Config.h>
#include <MeshImprimator/Geometry/IndexedTriangleMesh.h>
#include <MeshImprimator/Geometry/PointCloud.h>
#include <MeshImprimator/Algorithms/IMeshReconstructor.h>
#include <MeshImprimator/Algorithms/MaskData.h>

#include <list>

class TriangleMesh;
class IProgress;

/*!
 * \brief The DelaunayReconstructor class
 *
 * Uses the Delaunay tetrahedralization to reconstruct the mesh. The convex hull
 * of the object is computed, then the convex hull is carved by removing all
 * the tetrahedron barycenters that do not belong to all the object masks.
 */
class MESH_IMPRIMATOR_API DelaunayReconstructor : public IMeshReconstructor
{
public:
    /*!
     * \brief The MethodChoice enum
     *
     * Choice to use either the Triangulate method or the Tetrahedralize method
     * to reconstruct the surface.
     *
     */
    enum MethodChoice
    {
        TRIANGULATE, //!< To choose the Triangulate method which computes
                     //!< projections of triangles
        TETRAHEDRALIZE //!< To choose the Tetrahedralize method which computes
                       //!< projections of tetradra involving barycenters
                       //!< and with more precision in the results
    };

    /*!
     * \brief DelaunayReconstructor
     *
     * \param pointCloud The point cloud
     * \param maskDataList The list of all the image masks
     * \param epsilon Fuzzy parameter to determine the area around the
     * projected point to tell whether it is inside the mask
     * \param methodChoice the reconstruction method chosen,
     * the default method is Tetrahedralize
     */
    DelaunayReconstructor(const PointCloud& pointCloud,
                          const std::list<MaskData>& maskDataList,
                          int epsilon = 1,
                          MethodChoice methodChoice = TETRAHEDRALIZE);
    virtual ~DelaunayReconstructor();
    /*!
     * \brief reconstruct
     *
     * Reconstructs the unindexed mesh by computing the convex hull and carving
     * it.
     * \param progress An object to keep track of the progress of the algorithm
     * \return The unindexed triangle mesh
     */
    virtual TriangleMesh reconstruct(IProgress* progress);
    /*!
     * \brief reconstructIndexed
     *
     * Reconstructs the indexed mesh by computing the convex hull and carving
     * it.
     * \param progress An object to keep track of the progress of the algorithm
     * \return The indexed triangle mesh
     */
    virtual IndexedTriangleMesh reconstructIndexed(IProgress* progress);

private:
    /*!
     * \brief DelaunayReconstructor::triangulate
     * Computes all the triangles of the mesh. The triangles may be inside the
     * mesh or at the surface.
     */
    void triangulate(IProgress* progress);
    void tetrahedralize(IProgress* progress);
    bool testTetrahedron(const Vector3d vertices[4]) const;
    bool isInside(const Vector3d& point) const;

    const int epsilon;
    IndexedTriangleMesh triangleMesh;
    PointCloud pointCloud;
    std::list<MaskData> maskDataList;
    MethodChoice methodChoice;
};


#endif // DELAUNAYRECONSTRUCTOR_H
