#ifndef TRIANGLEMESH_H
#define TRIANGLEMESH_H

#include <MeshImprimator/Config.h>
#include <MeshImprimator/Geometry/Triangle.h>

#include <list>

/*!
 * \brief The TriangleMesh struct
 *
 * The triangle mesh.
 */
struct MESH_IMPRIMATOR_API TriangleMesh
{
    /*!
     * \brief triangles
     *
     * The mesh triangles
     */
    std::list<Triangle> triangles;
};

#endif // TRIANGLEMESH_H
