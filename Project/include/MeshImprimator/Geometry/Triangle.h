#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <MeshImprimator/Config.h>

/*!
 * \brief The Triangle class
 */
class MESH_IMPRIMATOR_API Triangle
{
public:
    /*!
     * \brief Triangle
     * \param v1 1st vertex of the triangle
     * \param v2 2nd vertex of the triangle
     * \param v3 3rd vertex of the triangle
     */
    Triangle(const Vector3d& v1, const Vector3d& v2, const Vector3d& v3);
    /*!
     * \brief vertex
     * \param i Index of the vertex to get, i is between 0 and 2
     * \return The i-th vertex
     */
    Vector3d& vertex(int i);
    const Vector3d& vertex(int i) const;
    /*!
     * \brief getBarycenter
     * \return The barycenter of the triangle
     */
    Vector3d getBarycenter() const;
    /*!
     * \brief getNormal
     * \return The normal of the triangle
     */
    Vector3d getNormal() const;

private:
    Vector3d vertices[3];
};


#endif // TRIANGLE_H
