#ifndef INDEXEDTRIANGLEMESH_H
#define INDEXEDTRIANGLEMESH_H

#include <MeshImprimator/Config.h>
#include <MeshImprimator/Geometry/IndexedTriangle.h>

#include <set>
#include <vector>

class TriangleMesh;

/*!
 * \brief The IndexedTriangleMesh struct
 *
 * The indexed triangle mesh.
 */
struct MESH_IMPRIMATOR_API IndexedTriangleMesh
{
    /*!
     * \brief triangles
     *
     * The indexed mesh triangles
     */
    std::set<IndexedTriangle> triangles;

    /*!
     * \brief vertices
     *
     * The mesh vertices
     */
    std::vector<Vector3d> vertices;

    /*!
     * \brief toUnindexed
     *
     * \return The unindexed mesh
     */
    TriangleMesh toUnindexed() const;
};


#endif // INDEXEDTRIANGLEMESH_H
