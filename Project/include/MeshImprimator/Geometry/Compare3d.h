#ifndef COMPARE3D_H
#define COMPARE3D_H

#include <MeshImprimator/Config.h>

struct Compare3d
{
    bool equals(double x, double y) const
    {
        return (x + MI_EPSILON >= y) && (x - MI_EPSILON <= y);
    }

    bool operator()(const Vector3d& u, const Vector3d& v) const
    {
        const double ux = u(0), uy = u(1), uz = u(2);
        const double vx = v(0), vy = v(1), vz = v(2);

        return  (ux < vx && !equals(ux, vx)) ||
                (equals(ux, vx) && uy < vy && !equals(uy, vy)) ||
                (equals(ux, vx) && equals(uy, vy) && uz < vz && !equals(uz, vz));
    }
};

#endif // COMPARE3D_H
