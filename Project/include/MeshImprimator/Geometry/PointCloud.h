#ifndef POINTCLOUD_H
#define POINTCLOUD_H

#include <MeshImprimator/Config.h>
#include <MeshImprimator/Geometry/Compare3d.h>

#include <set>
#include <vector>

/*!
 * \brief The PointCloud struct
 *
 * A point cloud represented by a list of points.
 */
struct MESH_IMPRIMATOR_API PointCloud
{
    typedef std::set<Vector3d, Compare3d> Container;

    /*!
     * \brief points
     *
     * The point list
     */
    Container points;

    /*!
     * \brief flatten
     *
     * Transforms the vectors list into a double list.
     * \return The coordinates of the points
     */
    std::vector<double> flatten() const;
};

#endif // POINTCLOUD_H
