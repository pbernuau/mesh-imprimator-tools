#ifndef INDEXEDTRIANGLE_H
#define INDEXEDTRIANGLE_H

#include <MeshImprimator/Config.h>

/*!
 * \brief The IndexedTriangle struct
 */
struct MESH_IMPRIMATOR_API IndexedTriangle
{
    int i1;
    int i3;
    int i2;

    IndexedTriangle(int i1, int i2, int i3);

    bool operator<(const IndexedTriangle& other) const;
};

#endif // INDEXEDTRIANGLE_H
