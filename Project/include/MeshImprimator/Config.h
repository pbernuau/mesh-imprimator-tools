#ifndef MESHIMPRIMATOR_CONFIG_H
#define MESHIMPRIMATOR_CONFIG_H


// Define a DLLIMPORT and DLLEXPORT macro specific to the compiler and the OS

#if defined(_WIN32) || defined(__WIN32__)
    #define MI_DLLEXPORT __declspec(dllexport)
    #define MI_DLLIMPORT __declspec(dllimport)
#else
    #if __GNUC__ >= 4
        #define MI_DLLEXPORT __attribute__((visibility ("default")))
        #define MI_DLLIMPORT __attribute__((visibility ("default")))
    #else
        #define MI_DLLEXPORT
        #define MI_DLLIMPORT
    #endif
#endif

#ifdef MESH_IMPRIMATOR_STATIC
    #define MESH_IMPRIMATOR_API
#else
    #ifdef MESH_IMPRIMATOR_BUILD
        #define MESH_IMPRIMATOR_API MI_DLLEXPORT
    #else
        #define MESH_IMPRIMATOR_API MI_DLLIMPORT
    #endif
#endif

// Define a library wide epsilon value
const double MI_EPSILON = 0.00000001;

// Include <iostream> if debugging is on
#ifdef MI_DEBUG
#include <iostream>
#endif

//==============================================================================

//
// TETGEN
//

#if defined(tetgenH) && !defined(TETLIBRARY)
#error tetgen.h already included but TETLIBRARY was not defined
#endif

// Define TETLIBRARY
#define TETLIBRARY 1

//
// EIGEN
//

// Disable Eigen memory alignment
#define EIGEN_DONT_ALIGN 1

#include <Eigen/Core>

using Eigen::Vector3d;

#endif // MESHIMPRIMATOR_CONFIG_H
