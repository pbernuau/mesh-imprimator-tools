#include <MeshImprimator/Geometry/Triangle.h>
#include <MeshImprimator/Geometry/Compare3d.h>

#include <Eigen/Geometry>

Triangle::Triangle(const Vector3d& v1, const Vector3d& v2, const Vector3d& v3)
{
    vertices[0] = v1;
    vertices[1] = v2;
    vertices[2] = v3;
}

Vector3d& Triangle::vertex(int i)
{
    return vertices[i];
}

const Vector3d& Triangle::vertex(int i) const
{
    return vertices[i];
}

Vector3d Triangle::getBarycenter() const
{
    return (vertices[0] + vertices[1] + vertices[2])/3;
}

Vector3d Triangle::getNormal() const
{
    Vector3d v1, v2, n;

    v1 = vertices[1] - vertices[0];
    v2 = vertices[2] - vertices[0];

    n = v1.cross(v2);

    Compare3d c;

    if (c.equals(n(0), 0.0) && c.equals(n(1), 0.0) && c.equals(n(2), 0.0))
        return n;
    else
        return n.normalized();
}
