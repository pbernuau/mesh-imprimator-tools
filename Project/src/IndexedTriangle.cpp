#include <MeshImprimator/Geometry/IndexedTriangle.h>

IndexedTriangle::IndexedTriangle(const int i1, const int i2, const int i3):
    // The vertices are sorted in the ascending order defined by their index
    i1(std::min(std::min(i1, i2), i3)),
    i3(std::max(std::max(i1, i2), i3)),
    i2(i1 + i2 + i3 - this->i1 - this->i3)
{
}

bool IndexedTriangle::operator<(const IndexedTriangle& other) const
{
    const int ux = i1, uy = i2, uz = i3;
    const int vx = other.i1, vy = other.i2, vz = other.i3;

    return  (ux < vx) ||
            (ux == vx && uy < vy) ||
            (ux == vx && uy == vy && uz < vz);
}
