#include <MeshImprimator/Algorithms/DelaunayReconstructor.h>

#include <MeshImprimator/Geometry/TriangleMesh.h>
#include <MeshImprimator/Algorithms/IProgress.h>

#include "utility.h"

#include <tetgen.h>

DelaunayReconstructor::DelaunayReconstructor(
        const PointCloud& pointCloud,
        const std::list<MaskData>& maskDataList,
        int epsilon,
        MethodChoice methodChoice) :
    epsilon(epsilon),
    pointCloud(pointCloud),
    maskDataList(maskDataList),
    methodChoice(methodChoice)
{
}

DelaunayReconstructor::~DelaunayReconstructor()
{
}

void DelaunayReconstructor::triangulate(IProgress* progress)
{
    // The progress bar is at 0% at the beginning of the algorithm
    progress->setRange(0, 1);
    progress->setValue(0);

    tetgenbehavior b;
#ifndef MI_DEBUG
    b.quiet = true;
#endif

    tetgenio in;
    in.firstnumber              = 0;
    in.numberofpoints           = pointCloud.points.size();
    std::vector<double> points  = pointCloud.flatten();
    in.pointlist                = &points[0];

    tetgenio out;
    // Compute the tetrahedra of the point cloud
    ::tetrahedralize(&b, &in, &out);
    in.pointlist = NULL;

    // The progress bar is at 50% after Delaunay triangulation
    progress->setRange(0, 2*out.numberoftetrahedra - 1);
    progress->setValue(out.numberoftetrahedra);

    // The user canceled the execution
    if (progress->wasCanceled())
    {
        return;
    }

    // Copy tetgen's point list to vertices
    triangleMesh.vertices.reserve(out.numberofpoints);
    for (int i = 0; i < out.numberofpoints; i++)
    {
        triangleMesh.vertices.push_back(
                    vector_from_pointer(&out.pointlist[i*3]));
    }

    std::set<IndexedTriangle>::iterator it;

    // For each tetrahedron
    for (int i = 0; i < out.numberoftetrahedra; i++)
    {
        // At each iteration on the tetrahedra, the progress bar increases
        progress->setValue(i + out.numberoftetrahedra);

        // The user canceled the execution
        if (progress->wasCanceled())
        {
            return;
        }

        // Create the four triangles that compose the tetrahedron
        IndexedTriangle t1(out.tetrahedronlist[i*4+0],
                           out.tetrahedronlist[i*4+1],
                           out.tetrahedronlist[i*4+2]);

        IndexedTriangle t2(out.tetrahedronlist[i*4+1],
                           out.tetrahedronlist[i*4+2],
                           out.tetrahedronlist[i*4+3]);

        IndexedTriangle t3(out.tetrahedronlist[i*4+2],
                           out.tetrahedronlist[i*4+3],
                           out.tetrahedronlist[i*4+0]);

        IndexedTriangle t4(out.tetrahedronlist[i*4+3],
                           out.tetrahedronlist[i*4+0],
                           out.tetrahedronlist[i*4+1]);

        IndexedTriangle triangles[] = { t1, t2, t3, t4 };

        // For each of those triangles
        for (int i = 0; i < 4; i++)
        {
            // Find the triangle vertices
            const double* ptr1 = &points.at(triangles[i].i1*3);
            const double* ptr2 = &points.at(triangles[i].i2*3);
            const double* ptr3 = &points.at(triangles[i].i3*3);

            // For each vertice, the coordinates are obtained from a pointer on
            // points at the appropriate location
            Vector3d vertex1, vertex2, vertex3;

            vertex1 << ptr1[0], ptr1[1], ptr1[2];
            vertex2 << ptr2[0], ptr2[1], ptr2[2];
            vertex3 << ptr3[0], ptr3[1], ptr3[2];

            // Compute the barycenter of the triangle and check whether
            // its projection is inside all the mask objects
            Vector3d barycenter = (vertex1 + vertex2 + vertex3)/3;
            if (isInside(barycenter))
            {
                // Add the triangle to the mesh
                it = triangleMesh.triangles.find(triangles[i]);

                if (it == triangleMesh.triangles.end())
                {
                    triangleMesh.triangles.insert(triangles[i]);
                }
                else
                {
                    triangleMesh.triangles.erase(it);
                }
            }
        }
    }
}

void DelaunayReconstructor::tetrahedralize(IProgress* progress)
{
    // The progress bar is at 0% at the beginning of the algorithm
    progress->setRange(0, 1);
    progress->setValue(0);

    tetgenbehavior b;
#ifndef MI_DEBUG
    b.quiet = true;
#endif

    tetgenio in;
    in.firstnumber              = 0;
    in.numberofpoints           = pointCloud.points.size();
    std::vector<double> points  = pointCloud.flatten();
    in.pointlist                = &points[0];

    tetgenio out;
    // Compute the tetrahedra of the point cloud
    ::tetrahedralize(&b, &in, &out);
    in.pointlist = NULL;

    // The progress bar is at 50% after Delaunay triangulation
    progress->setRange(0, 2*out.numberoftetrahedra - 1);
    progress->setValue(out.numberoftetrahedra);

    // The user canceled the execution
    if (progress->wasCanceled())
    {
        return;
    }

    // Copy tetgen's point list to vertices
    triangleMesh.vertices.reserve(out.numberofpoints);
    for (int i = 0; i < out.numberofpoints; i++)
    {
        triangleMesh.vertices.push_back(
                    vector_from_pointer(&out.pointlist[i*3]));
    }

    std::set<IndexedTriangle>::iterator it;

    // For each tetrahedron
    for (int i = 0; i < out.numberoftetrahedra; i++)
    {
        // At each iteration on the tetrahedra, the progress bar increases
        progress->setValue(i + out.numberoftetrahedra);

        // The user canceled the execution
        if (progress->wasCanceled())
        {
            return;
        }

        // The 4 vertices of the tetrahedron
        Vector3d vertices[4];

        // A tetrahedron is described as a set of four consecutive vertices
        // indices in out.tetrahedronlist
        for (int j = 0; j < 4; j++)
        {
            int index = out.tetrahedronlist[i*4+j];

            // For each vertex, the coordinates are obtained from a pointer on
            // points at the appropriate location
            vertices[j] = vector_from_pointer(&points[index * 3]);
        }

        // Check whether the barycenter projection is inside all the mask
        // objects and, if so, add the tetrahedron to the mesh
        if (testTetrahedron(vertices))
        {
            // Create the four triangles that compose the tetrahedron
            IndexedTriangle t1(out.tetrahedronlist[i*4+0],
                               out.tetrahedronlist[i*4+1],
                               out.tetrahedronlist[i*4+2]);

            IndexedTriangle t2(out.tetrahedronlist[i*4+1],
                               out.tetrahedronlist[i*4+2],
                               out.tetrahedronlist[i*4+3]);

            IndexedTriangle t3(out.tetrahedronlist[i*4+2],
                               out.tetrahedronlist[i*4+3],
                               out.tetrahedronlist[i*4+0]);

            IndexedTriangle t4(out.tetrahedronlist[i*4+3],
                               out.tetrahedronlist[i*4+0],
                               out.tetrahedronlist[i*4+1]);


            IndexedTriangle triangles[] = { t1, t2, t3, t4 };

            for(int i = 0; i < 4; i++)
            {
                it = triangleMesh.triangles.find(triangles[i]);

                if (it == triangleMesh.triangles.end())
                {
                    triangleMesh.triangles.insert(triangles[i]);
                }
                else
                {
                    triangleMesh.triangles.erase(it);
                }
            }

        }
    }
}


bool DelaunayReconstructor::testTetrahedron(const Vector3d vertices[4]) const
{
    double weights[][4] =
    {
        { 0.25, 0.25, 0.25, 0.25 },
        { 0.70, 0.10, 0.10, 0.10 },
        { 0.10, 0.70, 0.10, 0.10 },
        { 0.10, 0.10, 0.70, 0.10 },
        { 0.10, 0.10, 0.10, 0.70 }
    };

    for(int i = 0; i < 5; i++)
    {
        // Compute the barycenter of the tetrahedron
        Vector3d barycenter =
                weights[i][0] * vertices[0] +
                weights[i][1] * vertices[1] +
                weights[i][2] * vertices[2] +
                weights[i][3] * vertices[3];

        // Check whether the barycenter projection is inside all the mask
        // objects and, if so, add the tetrahedron to the mesh
        if (!isInside(barycenter))
        {
            return false;
        }
    }

    return true;

}

bool DelaunayReconstructor::isInside(const Vector3d& point) const
{
    for(std::list<MaskData>::const_iterator it = maskDataList.begin();
        it != maskDataList.end();
        it++)
    {
        if (!it->contains(point, epsilon))
        {
            return false;
        }
    }

    return true;
}

IndexedTriangleMesh DelaunayReconstructor::reconstructIndexed(IProgress* progress)
{
    switch (methodChoice)
    {
    case TRIANGULATE:
        triangulate(progress);
        break;
    case TETRAHEDRALIZE:
        tetrahedralize(progress);
        break;
    default:
    {
#ifdef MI_DEBUG
        std::cerr << "Invalid method choice." << std::endl;
#endif
    }
    }

    return triangleMesh;
}

TriangleMesh DelaunayReconstructor::reconstruct(IProgress* progress)
{
    reconstructIndexed(progress);
    return triangleMesh.toUnindexed();
}
