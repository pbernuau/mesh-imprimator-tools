#include <MeshImprimator/Geometry/IndexedTriangleMesh.h>

#include <MeshImprimator/Geometry/TriangleMesh.h>

TriangleMesh IndexedTriangleMesh::toUnindexed() const
{
    TriangleMesh triangleMesh;

    for (std::set<IndexedTriangle>::iterator it = triangles.begin();
         it != triangles.end();
         ++it)
    {
        const Vector3d& v1 = vertices.at(it->i1);
        const Vector3d& v2 = vertices.at(it->i2);
        const Vector3d& v3 = vertices.at(it->i3);

        triangleMesh.triangles.push_back(Triangle(v1, v2, v3));
    }

    return triangleMesh;
}
