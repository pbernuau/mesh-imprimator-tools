#include <MeshImprimator/Geometry/PointCloud.h>

std::vector<double> PointCloud::flatten() const
{
    std::vector<double> flattened;
    flattened.reserve(3 * points.size());

    for(Container::const_iterator it = points.begin();
        it != points.end();
        ++it)
    {
        flattened.push_back((*it)(0));
        flattened.push_back((*it)(1));
        flattened.push_back((*it)(2));
    }

    return flattened;
}
