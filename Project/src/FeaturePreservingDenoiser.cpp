#include <MeshImprimator/Algorithms/FeaturePreservingDenoiser.h>

#include <MeshImprimator/Geometry/IndexedTriangleMesh.h>
#include <MeshImprimator/Geometry/TriangleMesh.h>
#include <MeshImprimator/Algorithms/IProgress.h>

FeaturePreservingDenoiser::FeaturePreservingDenoiser(
        const IndexedTriangleMesh &mesh,
        const double threshold,
        const int nbIterNormal,
        const int nbIterVertex) :
    threshold(threshold),
    nbIterNormal(nbIterNormal),
    nbIterVertex(nbIterVertex),
    nbVertices(mesh.vertices.size()),
    nbTriangles(mesh.triangles.size())
{
    vertices = mesh.vertices;
    triangles.reserve(nbTriangles);
    std::copy(mesh.triangles.begin(), mesh.triangles.end(),
              std::back_inserter(triangles));
    normals.reserve(nbTriangles);
    nFs.resize(nbTriangles); // each list of nFs is initialized and empty
    nV.resize(nbVertices); // each set of nV is initialized and empty
    cardFV.reserve(nbVertices);
    edgeTrianglesMap.resize(nbVertices);
}

FeaturePreservingDenoiser::~FeaturePreservingDenoiser()
{
}

TriangleMesh FeaturePreservingDenoiser::denoise(IProgress* progress)
{
    return denoiseIndexed(progress).toUnindexed();
}

IndexedTriangleMesh FeaturePreservingDenoiser::denoiseIndexed(
        IProgress* progress)
{
    int stepProgress = nbIterNormal*nbIterVertex;

    progress->setRange(0, 6*stepProgress);

    progress->setValue(0);

    computeNFs();
    progress->setValue(stepProgress);

    computeNormals();
    progress->setValue(2*stepProgress);

    computeNVCardFV();
    progress->setValue(3*stepProgress);

    computeEdgeTrianglesMap();
    progress->setValue(4*stepProgress);

    updateNormals(progress);
    progress->setValue(5*stepProgress);

    updateVertices(progress);
    progress->setValue(6*stepProgress);

    IndexedTriangleMesh mesh;
    mesh.vertices = vertices;
    std::copy(triangles.begin(), triangles.end(),
              std::inserter(mesh.triangles, mesh.triangles.begin()));

    return mesh;
}

void FeaturePreservingDenoiser::computeNormals()
{
    for (int i = 0; i < nbTriangles; i++)
    {
        // Compute normals for the current triangle
        Triangle triangle(vertices[triangles[i].i1],
                vertices[triangles[i].i2],
                vertices[triangles[i].i3]);
        Vector3d normal = triangle.getNormal();
        normals.push_back(normal);
    }
}

void FeaturePreservingDenoiser::computeNFs(bool normalsMakeConsistent)
{
    // Set of triangle indices to process, initialized to all triangles
    std::set<int> toProcess;
    for (int i = 0; i < nbTriangles; ++i)
        toProcess.insert(i);
    // One loop iteration per connex component of the mesh
    while (!toProcess.empty())
    {
        std::set<int>::iterator it = toProcess.begin();
        int triangleId = *it;
        toProcess.erase(triangleId);
        recComputeNFs(triangleId, toProcess, normalsMakeConsistent);
    }
}

void FeaturePreservingDenoiser::recComputeNFs(int triangleId,
                                              std::set<int> &toProcess,
                                              bool normalsMakeConsistent)
{
    // Triangle is by definition in its own neighbor list
    nFs[triangleId].insert(triangleId);
    // Find every other neighbor of triangle in toProcess. If toProcess is
    // empty, recursion will stop.
    // Instruction of increase is contitionally defined inside the loop.
    std::list<int> foundNeighbors;
    for (std::set<int>::iterator it = toProcess.begin();
         it != toProcess.end();
         ++it)
    {
        int otherTriangleId = *it;
        int commonVertex1, commonVertex2;
        // Do triangle and otherTriangle have an edge in common?
        bool neighbors = false;
        if (edgeInTriangle(triangles[triangleId].i1,
                           triangles[triangleId].i2,
                           triangles[otherTriangleId]))
        {
            commonVertex1 = triangles[triangleId].i1;
            commonVertex2 = triangles[triangleId].i2;
            neighbors = true;
        }
        else if(edgeInTriangle(triangles[triangleId].i2,
                               triangles[triangleId].i3,
                               triangles[otherTriangleId]))
        {
            commonVertex1 = triangles[triangleId].i2;
            commonVertex2 = triangles[triangleId].i3;
            neighbors = true;
        }
        else if(edgeInTriangle(triangles[triangleId].i3,
                               triangles[triangleId].i1,
                               triangles[otherTriangleId]))
        {
            commonVertex1 = triangles[triangleId].i3;
            commonVertex2 = triangles[triangleId].i1;
            neighbors = true;
        }
        if (neighbors)
        {
            if (normalsMakeConsistent)
            {
                // Are common edges in the same direction?
                if (
                        (commonVertex1 == triangles[otherTriangleId].i1 &&
                         commonVertex2 == triangles[otherTriangleId].i2) ||
                        (commonVertex1 == triangles[otherTriangleId].i2 &&
                         commonVertex2 == triangles[otherTriangleId].i3) ||
                        (commonVertex1 == triangles[otherTriangleId].i3 &&
                         commonVertex2 == triangles[otherTriangleId].i1)
                        )
                {
                    // Swap two vertices in the new triangle
                    int tmp = triangles[otherTriangleId].i1;
                    triangles[otherTriangleId].i1 =
                            triangles[otherTriangleId].i2;
                    triangles[otherTriangleId].i2 = tmp;
                }
            }
            // Update neighbors
            nFs[triangleId].insert(otherTriangleId);
            foundNeighbors.push_back(otherTriangleId);
            nFs[otherTriangleId].insert(triangleId);
            // Recursive call is not made here but after the loop, because it
            // would invalidate the iterator.
            // A triangle has no more than 4 II-neighbors, so stop if they have
            // all been found.
            if (nFs[triangleId].size() >= 4)
                break;
        }
    }
    // Recursive calls on triangle's neighbors and toProcess queue
    for (std::list<int>::iterator it = foundNeighbors.begin();
         it != foundNeighbors.end();
         ++it)
    {
        int triangleId = *it;
        toProcess.erase(triangleId);
        recComputeNFs(triangleId, toProcess);
    }
}

void FeaturePreservingDenoiser::computeNVCardFV()
{
    for (int i = 0; i < nbVertices; i++)
    {
        int cardFVTmp = 0;
        for (int j = 0; j < nbTriangles; j++)
        {
            if (vertexInTriangle(i, triangles[j]))
            {
                // Increment cardFVTmp if the ith vertex is in the jth triangle
                cardFVTmp++;

                // Compute nV for the current vertex
                if (i != triangles[j].i1)
                {
                    nV[i].insert(triangles[j].i1);
                }
                if (i != triangles[j].i2)
                {
                    nV[i].insert(triangles[j].i2);
                }
                if (i != triangles[j].i3)
                {
                    nV[i].insert(triangles[j].i3);
                }
            }
        }
        cardFV.push_back(cardFVTmp);
    }
}

void FeaturePreservingDenoiser::computeEdgeTrianglesMap()
{
    for (int i = 0; i < nbTriangles; i++)
    {
        edgeTrianglesMap[triangles[i].i1][triangles[i].i2].push_back(i);
        edgeTrianglesMap[triangles[i].i2][triangles[i].i3].push_back(i);
        edgeTrianglesMap[triangles[i].i1][triangles[i].i3].push_back(i);
    }
}

void FeaturePreservingDenoiser::updateNormals(IProgress* progress)
{
    int initialProgress = 3*nbIterNormal*nbIterVertex;

    // updatedNormals is the updated version of normals
    std::vector<Vector3d> updatedNormals;
    updatedNormals.reserve(nbTriangles);

    // Update normals
    for (int n = 0; n < nbIterNormal; n++)
    {
        progress->setValue(n*nbIterVertex + initialProgress);

        for (int i = 0; i < nbTriangles; i++)
        {
            Vector3d updatedNormal(0, 0, 0);

            // Compute updatedNormal
            for (std::set<int>::iterator it = nFs[i].begin();
                it != nFs[i].end();
                ++it)
            {
                double dp = normals[i].dot(normals[*it]);
                if (dp > threshold)
                {
                    double diff = dp - threshold;
                    // To compute h, the square function is chosen; any
                    // strictly increasing function could be used
                    double h = diff*diff;
                    updatedNormal += h*normals[*it];
                }
            }

            updatedNormal.normalize();

            updatedNormals.push_back(updatedNormal);
        }

        // normals is updated
        for (int i = 0; i < nbTriangles; i++)
        {
            normals[i] = updatedNormals[i];
        }

        // updatedNormals is cleared for the next iteration
        updatedNormals.clear();
    }
}

void FeaturePreservingDenoiser::updateVertices(IProgress *progress)
{
    int initialProgress = 4*nbIterNormal*nbIterVertex;

    // updatedVertices is the updated version of vertices
    std::vector<Vector3d> updatedVertices;
    updatedVertices.reserve(nbVertices);

    // Update vertices
    for (int n = 0; n < nbIterVertex; n++)
    {
        progress->setValue(n*nbIterNormal + initialProgress);

        for (int i = 0; i < nbVertices; i++)
        {
            Vector3d updatedVertex(0, 0, 0);

            // Compute updatedVertex
            for (std::set<int>::iterator it1 = nV[i].begin();
                it1 != nV[i].end();
                ++it1)
            {
                // Compute j, k such that j = min(i, *it1) and k = max(i, *it1).
                // This step is necessary to go through edgeTriangleMap lists
                // because edgeTriangleMap is filled in a triangular way:
                // edgeTriangleMap[j][k] has been filled up iff j < k, otherwise
                // edgeTriangleMap[j][k] is empty
                int j, k;
                if (i < *it1)
                {
                    j = i;
                    k = *it1;
                }
                else
                {
                    j = *it1;
                    k = i;
                }

                // trianglesJK is the list of triangle indices such that those
                // triangles contain the edge <j, k>
                std::list<int>& trianglesJK = edgeTrianglesMap[j][k];

                // Compute updatedVertex
                for (std::list<int>::iterator it2 = trianglesJK.begin();
                     it2 != trianglesJK.end();
                     ++it2)
                {
                    updatedVertex += normals[*it2].dot(
                                vertices[*it1] - vertices[i])*normals[*it2];
                }
            }

            updatedVertex = vertices[i] + 1.0/(3*cardFV[i])*updatedVertex;

            updatedVertices.push_back(updatedVertex);
        }

        // vertices is updated
        for (int i = 0; i < nbVertices; i++)
        {
            vertices[i] = updatedVertices[i];
        }

        // updatedVertices is cleared for the next iteration
        updatedVertices.clear();
    }
}

bool FeaturePreservingDenoiser::edgeInTriangle(
                                int i, int j, IndexedTriangle triangle)
{
    return vertexInTriangle(i, triangle) && vertexInTriangle(j, triangle);
}

bool FeaturePreservingDenoiser::vertexInTriangle(
                                int i, IndexedTriangle triangle)
{
    return (i == triangle.i1) ||
           (i == triangle.i2) ||
           (i == triangle.i3);
}
