#include <MeshImprimator/IO/STLWriter.h>

#include <MeshImprimator/Geometry/Compare3d.h>
#include <MeshImprimator/Geometry/TriangleMesh.h>

#include <fstream>

STLWriter::STLWriter() {
    status = SUCCESS;
}

void STLWriter::write(const TriangleMesh& mesh,
                      const std::string& outputFilename,
                      const std::string& meshName)
{
    std::ofstream ofs(outputFilename.c_str(), std::ofstream::out);

    if (!ofs.is_open())
    {
       status = IO_ERROR;
       return;
    }

    ofs << "solid " << meshName << "\n";

    for (std::list<Triangle>::const_iterator it = mesh.triangles.begin();
         it != mesh.triangles.end();
         it++)
    {
        Vector3d normal = it->getNormal();

        Compare3d compare;

        if (compare.equals(normal(0), 0.0)
                && compare.equals(normal(1), 0.0)
                && compare.equals(normal(2), 0.0))
        {
#ifdef MI_DEBUG
            std::cerr << "There is an undefined normal: " << std::endl;
            std::cerr << "Vertex #1: " << it->vertex(0) << std::endl;
            std::cerr << "Vertex #2: " << it->vertex(1) << std::endl;
            std::cerr << "Vertex #3: " << it->vertex(2) << std::endl;
            std::cerr << "Normal: " << normal << std::endl;
            std::cerr << "This triangle will be ignored. " << std::endl;
#endif
            continue;
        }

        ofs << "facet normal "
            << normal(0) << " "
            << normal(1) << " "
            << normal(2) << "\n";

        ofs << "\touter loop\n";

        for (int i = 0; i < 3; i++)
        {
            ofs << "\t\tvertex "
                << it->vertex(i)(0) << " "
                << it->vertex(i)(1) << " "
                << it->vertex(i)(2) << "\n";
        }

        ofs << "\tendloop\n";

        ofs << "endfacet\n";
    }

    ofs << "endsolid " << meshName;

    ofs.close();
}


