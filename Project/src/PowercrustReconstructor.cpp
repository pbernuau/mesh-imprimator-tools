#include <MeshImprimator/Algorithms/PowercrustReconstructor.h>

#include <MeshImprimator/Geometry/TriangleMesh.h>
#include <MeshImprimator/Algorithms/IProgress.h>
#include <MeshImprimator/Algorithms/Voronoi.h>

#include "utility.h"

#include <Eigen/Geometry>
#include <tetgen.h>

#include <algorithm>
#include <deque>

#include <cmath>

bool ComputeIntersectAngle(Vector3d c1, double r1,
                           Vector3d c2, double r2,
                           double& cosAlpha);

//==============================================================================

PowercrustReconstructor::PowercrustReconstructor(const PointCloud& pointCloud) :
    pointCloud(pointCloud),
    voroData(NULL)
{
}

PowercrustReconstructor::~PowercrustReconstructor()
{
    clear();
}

TriangleMesh PowercrustReconstructor::reconstruct(IProgress* progress)
{
    reconstructIndexed(progress);
    return triangleMesh.toUnindexed();
}

IndexedTriangleMesh PowercrustReconstructor::reconstructIndexed(
        IProgress* progress)
{
    clear();

    progress->setRange(0, 1000);
    progress->setValue(0);

    createVoronoiDiagram(progress) &&
    computeVoronoiPoles(progress) &&
    computePowerDiagram(progress) &&
    computeDINP(progress) &&
    labelVoronoiPoles(progress) &&
    filterDelaunayFaces(progress);

    progress->setValue(1000);

    return triangleMesh;
}

void PowercrustReconstructor::clear()
{
    triangleMesh.triangles.clear();
    triangleMesh.vertices.clear();

    if (voroData != NULL)
    {
        delete voroData;
        voroData = NULL;
    }
}

// Compute the power of every voronoi vertex
// The power of this Voronoi vertex is the smallest distance between this
// vertex and any Delaunay point in the cloud
bool PowercrustReconstructor::createVoronoiDiagram(IProgress* progress)
{
#ifdef MI_DEBUG
    std::cerr << "Creating Voronoi diagram..." << std::endl;
#endif
    tetgenio in;
    in.firstnumber              = 0;
    in.numberofpoints           = pointCloud.points.size();
    std::vector<double> points  = pointCloud.flatten();
    in.pointlist                = points.data();

    tetgenbehavior b;
    b.voroout = true;
    b.edgesout = true;
    b.facesout = true;
#ifndef MI_DEBUG
    b.quiet = true;
#endif

    tetgenio out;
    // Compute the tetrahedra of the point cloud
    tetrahedralize(&b, &in, &out);
    in.pointlist = NULL;

    //
    // Create new Voronoi Data object
    //

    voroData = new VoroData();

    // Step 1: set Delaunay vertices

    voroData->delauVertices.reserve(out.numberofpoints);

    for(int i = 0; i < out.numberofpoints; i++)
    {
        voroData->delauVertices.push_back(
                    vector_from_pointer(&out.pointlist[3*i]));
    }

    // Step 2: set Voronoi vertices

    // Tetgen Voronoi vertices are not unique

    // vvertices is an associative array mapping a unique Voronoi vertex
    // position to its new index
    std::map<Vector3d, int, Compare3d> vvertices;
    int index = 0;

    // vindices is an array mapping a tetgen Voronoi index to its new unique
    // index in VoroData::voroVertices
    std::vector<int> vindices;
    vindices.reserve(out.numberofvpoints);

    // fill Voronoi vertices in VoroData and compute index map
    for (int i = 0; i < out.numberofvpoints; i++)
    {
        progress->setValue(i * 100 / out.numberofvpoints);
        if (progress->wasCanceled())
            return false;

        Vector3d pos = vector_from_pointer(&out.vpointlist[3*i]);

        std::map<Vector3d, int, Compare3d>::iterator it = vvertices.find(pos);
        if (it == vvertices.end())
        {
            // there is no VoroVertex at this position,
            // we map this new position to a unique index
            // and push the new VoroVertex into voroData

            vvertices.insert(std::make_pair(pos, index));
            vindices.push_back(index);
            index++;

            VoroVertex vv;
            vv.position = pos;
            voroData->voroVertices.push_back(vv);
        }
        else
        {
            // there is a VoroVertex at this position,
            // we map i to the unique index

            vindices.push_back(it->second);
        }
    }

    // Step 3: set Voronoi edges

    // Compute and push all VoroEdge and connect the vertices.

    for (int i = 0; i < out.numberofvedges; i++)
    {
        const tetgenio::voroedge* edge = &(out.vedgelist[i]);

        int v1 = vindices[edge->v1];
        int v2 = (edge->v2 == -1 ? -1 : vindices[edge->v2]);

        // create a VoroEdge
        VoroEdge myEdge;
        myEdge.direction(0) = edge->vnormal[0];
        myEdge.direction(1) = edge->vnormal[1];
        myEdge.direction(2) = edge->vnormal[2];
        myEdge.v1 = v1;
        myEdge.v2 = v2;
        myEdge.delauTriangle.v1 = out.trifacelist[3 * i + 0];
        myEdge.delauTriangle.v2 = out.trifacelist[3 * i + 1];
        myEdge.delauTriangle.v3 = out.trifacelist[3 * i + 2];
        voroData->voroEdges.push_back(myEdge);

        // connect the VoroVertices
        if (v2 != -1 && v1 != v2)
        {
            voroData->voroVertices[v1].adjacentVoroVertices.insert(v2);
            voroData->voroVertices[v2].adjacentVoroVertices.insert(v1);
        }
    }

    // Step 4: set Voronoi cells and set adjacent Voronoi vertices

    voroData->voroCells.reserve(out.numberofvcells);

    for(int i = 0; i < out.numberofvcells; i++)
    {
        VoroCell cell;

        cell.delauVertex = i;

        // for each facet...
        int facetCount = out.vcelllist[i][0] - 1;
        for(int j = 0; j < facetCount; j++)
        {
            const tetgenio::vorofacet* facet =
                    &(out.vfacetlist[out.vcelllist[i][j+1]]);

            // for each edges...
            int edgeCount = facet->elist[0] - 1;
            for(int k = 0; k < edgeCount; k++)
            {
                int edgeIndex = facet->elist[k+1];
                const tetgenio::voroedge& edge = out.vedgelist[edgeIndex];

                // add the index of this edge to the list
                cell.voroEdges.insert(edgeIndex);

                // add the VoroVertices to the cell
                cell.voroVertices.insert(vindices[edge.v1]);
                if (edge.v2 != -1)
                    cell.voroVertices.insert(vindices[edge.v2]);
            }
        }

        voroData->voroCells.push_back(cell);
    }

    return true;
}

bool PowercrustReconstructor::computeVoronoiPoles(IProgress* progress)
{
#ifdef MI_DEBUG
    std::cerr << "Computing Voronoi poles..." << std::endl;
#endif

    unsigned int nbCells = voroData->voroCells.size();

    // for each voronoi cell
    for(unsigned int i = 0; i < nbCells; i++)
    {
        progress->setValue(100 + i * 100 / nbCells);
        if (progress->wasCanceled())
            return false;

        VoroCell* cell = &voroData->voroCells[i];

        // p is the Delaunay Vertex at the center of the cell
        Vector3d p = voroData->delauVertices[cell->delauVertex];
        std::set<int> infiniteEdges = voroData->getInfiniteEdges(*cell);

        //
        // compute the positive pole of the cell
        //

        Vector3d u(0.0, 0.0, 0.0);

        if (infiniteEdges.empty())
        {
            // the cell is bounded

            // the positive pole is the voronoi vertex of the cell with the
            // maximal distance to the sample point p

            double max = -1.0, d = 0.0;

            for(std::set<int>::iterator vert = cell->voroVertices.begin();
                vert != cell->voroVertices.end();
                ++vert)
            {
                d = (voroData->voroVertices.at(*vert).position - p).norm();

                if (d > max)
                {
                    max                 = d;
                    cell->positivePole  = *vert;
                }
            }

            // attach the delaunay vertex of the cell to its pole
            voroData->voroVertices[cell->positivePole]
                    .attachedDelauVertices.insert(cell->delauVertex);

            // let u be the vector from p to the positive pole
            u = voroData->voroVertices[cell->positivePole].position - p;
            u.normalize();
        }
        else
        {
            // the cell is unbounded

            // the positive pole is undefined
            cell->positivePole = -1;

            // let u be the average direction of all unbounded voro edges
            for(std::set<int>::iterator edge = infiniteEdges.begin();
                edge != infiniteEdges.end();
                ++edge)
            {
                u += voroData->voroEdges[*edge].direction;
            }

            u = -u / static_cast<double>(infiniteEdges.size());
            u.normalize();
        }

        //
        // compute the negative pole of the cell
        //

        double max = -1.0;
        for(std::set<int>::iterator vert = cell->voroVertices.begin();
            vert != cell->voroVertices.end();
            ++vert)
        {
            // Compute the distance and direction of the vector vp between
            // the delaunay vertex (p) and the voronoi vertex
            Vector3d vp = voroData->voroVertices.at(*vert).position - p;

            double delauVoroVertexDist  = vp.norm();
            vp                          = vp.normalized();

            // Compute the cosine between the two directions
            double cos = u.dot(vp);
            // if the voronoi vertex is not on the same side as the first
            // pole
            if (cos < 0.0)
            {
                if (delauVoroVertexDist > max)
                {
                    max                 = delauVoroVertexDist;
                    cell->negativePole  = *vert;
                    cell->cosBeta       = cos;
                }
            }
        }
    }

    return true;
}

// For each voronoi vertex,
bool PowercrustReconstructor::computePowerDiagram(IProgress* progress)
{
#ifdef MI_DEBUG
    std::cerr << "Computing power diagram..." << std::endl;
#endif

    unsigned int nbCells = voroData->voroCells.size();

    for (unsigned int i = 0; i < nbCells; i++)
    {
        progress->setValue(200 + i * 100 / nbCells);
        if (progress->wasCanceled())
            return false;

        VoroCell& cell = voroData->voroCells[i];
        Vector3d dV = voroData->delauVertices[cell.delauVertex];

        for (std::set<int>::iterator j = cell.voroVertices.begin();
             j != cell.voroVertices.end();
             ++j)
        {
            VoroVertex& voroV = voroData->voroVertices[*j];
            double distance = (voroV.position - dV).norm();

            voroV.power = std::min(voroV.power, distance);
        }
    }

    return true;
}

bool PowercrustReconstructor::computeDINP(IProgress* progress)
{
#ifdef MI_DEBUG
    std::cerr << "Computing deeply intersecting neighboring poles..."
              << std::endl;
#endif

    unsigned int nbVertices = voroData->voroVertices.size();

    for (unsigned int i = 0; i < nbVertices; i++)
    {
        progress->setValue(300 + i * 100 / nbVertices);
        if (progress->wasCanceled())
            return false;

        VoroVertex& v1 = voroData->voroVertices[i];

        for (std::set<int>::iterator it = v1.adjacentVoroVertices.begin();
             it != v1.adjacentVoroVertices.end();
             ++it)
        {
            int j = *it;

            // skip cases already done
            if (j > (int) i)
                continue;

            VoroVertex& v2 = voroData->voroVertices[j];

            double cosAlpha;

            // compute the intersection angle
            if (ComputeIntersectAngle(v1.position, v1.power,
                                      v2.position, v2.power,
                                      cosAlpha))
            {
                // the two balls deeply intersect
                if (cosAlpha < 0.0)
                {
                    v1.deeplyIntersectNeighPoles.push_back(
                                std::make_pair(j, cosAlpha));
                    v2.deeplyIntersectNeighPoles.push_back(
                                std::make_pair(i, cosAlpha));
                }
            }
        }
    }

    return true;
}

bool PowercrustReconstructor::labelVoronoiPoles(IProgress* progress)
{
#ifdef MI_DEBUG
    std::cerr << "Labeling Voronoi poles..." << std::endl;
    std::cerr << "(" << voroData->voroVertices.size() << " poles)" << std::endl;
#endif

    // for each cell c such as c.positivePole = -1 : q.in = w_psq
    for (unsigned int i = 0; i < voroData->voroCells.size(); i++)
    {
        VoroCell& cell = voroData->voroCells[i];

        if (cell.positivePole == -1)
        {
            VoroVertex& vv = voroData->voroVertices[cell.negativePole];
            vv.in = std::max(vv.in, -cell.cosBeta);
        }
    }

    VoroQueue queue;

    // for each pole p, insert p in the queue
    for (std::vector<VoroVertex>::iterator it = voroData->voroVertices.begin();
         it != voroData->voroVertices.end();
         ++it)
    {
        it->bind(&queue);
    }

    unsigned int nbVertices = voroData->voroVertices.size();
    unsigned int count = 0;

    while (!queue.empty())
    {
        progress->setValue(400 + (count++) * 500 / nbVertices);
        if (progress->wasCanceled())
            return false;

        double tmp;

        //Remove the top priority element
        VoroQueue::iterator last = queue.end();
        last--;
        VoroVertex& vp = *(last->second);
        vp.bind(NULL);

        if (vp.in > MI_EPSILON + vp.out)
        {
            vp.label = VoroVertex::IN;
            tmp = vp.in;
        }
        else
        {
            vp.label = VoroVertex::OUT;
            tmp = vp.out;
        }

        // For each sample s of which p is the pole
        for (std::set<int>::iterator it = vp.attachedDelauVertices.begin();
             it != vp.attachedDelauVertices.end();
             ++it)

        {
            const VoroCell& cell = voroData->voroCells.at(*it);

            int q = cell.negativePole;

            if (q == -1)
                continue;

            // Update the other pole opposite label of p
            VoroVertex& vq = voroData->voroVertices.at(q);
            if (vq.label != VoroVertex::UNDEFINED)
                continue;

            if (vp.label == VoroVertex::IN)
            {
                vq.out = std::max(-tmp * cell.cosBeta, vq.out);
            }
            else if (vp.label == VoroVertex::OUT)
            {
                vq.in = std::max(-tmp * cell.cosBeta, vq.in);
            }
            else
            {
#ifdef MI_DEBUG
                std::cerr << "Error in labelVoronoiPoles()." << std::endl;
#endif
                return false;
            }
            vq.update();
        }

        //For each deeply intersecting neighbouring pole q
        for (std::list< std::pair<int, double> >::iterator it =
                vp.deeplyIntersectNeighPoles.begin();
             it != vp.deeplyIntersectNeighPoles.end();
             ++it)
        {

            // Update the other pole same label
            VoroVertex& vq = voroData->voroVertices[it->first];
            if (vq.label != VoroVertex::UNDEFINED)
                continue;

            if (vp.label == VoroVertex::IN)
            {
                vq.in = std::max(-tmp * (it->second), vq.in);
            }
            else if (vp.label == VoroVertex::OUT)
            {
                vq.out = std::max(-tmp * (it->second), vq.out);
            }
            else
            {
#ifdef MI_DEBUG
                std::cerr << "Error in labelVoronoiPoles()." << std::endl;
#endif
                return false;
            }
            vq.update();
        }
    }

#ifdef MI_DEBUG
    std::cerr << "Done!" << std::endl;
#endif

    return true;
}

bool PowercrustReconstructor::filterDelaunayFaces(IProgress* progress)
{
    triangleMesh.vertices = voroData->delauVertices;

    unsigned int nbEdges = voroData->voroEdges.size();

    for (unsigned int i = 0; i < nbEdges; i++)
    {
        progress->setValue(900 + i * 100 / nbEdges);
        if (progress->wasCanceled())
            return false;

        const VoroEdge& edge = voroData->voroEdges[i];
        const VoroVertex& v1 = voroData->voroVertices[edge.v1];

        if (edge.isFinite())
        {
            const VoroVertex& v2 = voroData->voroVertices[edge.v2];

            // We only keep edges that have endpoints with opposite labels
            if (v1.label != v2.label)
            {
                IndexedTriangle tri(edge.delauTriangle.v1,
                                    edge.delauTriangle.v2,
                                    edge.delauTriangle.v3);
                triangleMesh.triangles.insert(tri);
            }
        }
        else
        {
            // We only keep rays whose finite endpoint is inside the surface.
            if (v1.label == VoroVertex::IN)
            {
                IndexedTriangle tri(edge.delauTriangle.v1,
                                    edge.delauTriangle.v2,
                                    edge.delauTriangle.v3);
                triangleMesh.triangles.insert(tri);
            }
        }
    }

    return true;
}


bool ComputeIntersectAngle(Vector3d c1, double r1,
                           Vector3d c2, double r2,
                           double& cosAlpha)
{
    //Compute the first axis u
    Vector3d u  = c2 - c1;
    double d    = u.norm();
    u.normalize();

    //Test if the two spheres intersect
    if (r1 + r2 < d)
    {
        return false;
    }

    Vector3d z(0.0, 0.0, 1.0);

    //Compute the second axis
    Vector3d v = u.cross(z);

    if (v.isZero())
    {
        v = u.cross(Vector3d(0.0, 1.0, 0.0));
    }

    //Compute the intersection
    double xI = (r1*r1 - r2*r2 + d*d)/(2*d);
    double yI = sqrt(r1*r1 - xI*xI);

    Vector3d I = c1 + xI * u + yI * v ;

    cosAlpha = - ((c1-I).dot(c2-I)/((c1-I).norm() * (c2-I).norm()));

    return true;
}

