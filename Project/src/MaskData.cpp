#include <MeshImprimator/Algorithms/MaskData.h>

using Eigen::Vector4d;

bool MaskData::contains(const Vector3d& vertex, int margin) const
{
    Vector4d vec4(vertex(0), vertex(1), vertex(2), 1.0);

    // Compute the projection of the vertex
    Vector3d proj = camera * vec4;
    proj /= proj(2);

    int x = static_cast<int>(round(proj(0)));
    int y = static_cast<int>(round(proj(1)));

    // Consider an area of neighbours around the projected point
    int xmin = x - margin;
    int xmax = x + margin;
    int ymin = y - margin;
    int ymax = y + margin;

    int sizeY = mask.rows();
    int sizeX = mask.cols();

    // Check if the area has no intersection with the object
    if (((xmin>sizeX) || (xmax<0) || (ymin>sizeY) || (ymax<0)))
    {
        // Why we do return true here:
        // If the object is partially visible on the mask
        // we have no information on its reprojection.
        // Simply put, we only return false when we are sure that
        // the point is not inside.
        return true;
    }

    xmin = std::max(0, xmin);
    ymin = std::max(0, ymin);
    xmax = std::min(xmax, sizeX - 1);
    ymax = std::min(ymax, sizeY - 1);

    bool intersect = false;

    // For each pixel of the considered area
    for (int i = ymin; i <= ymax && !intersect; i++)
    {
        for (int j = xmin; j <= xmax && !intersect; j++)
        {
            // Check whether the object is present
            if (mask(i,j) > 0)
            {
                // The object is present
                intersect = true;
            }
        }
    }

    // If the point is not inside the object, return false
    if (!intersect)
    {
        return false;
    }


    return true;
}

bool MaskData::isValid() const
{
    return mask.rows() * mask.cols() > 0;
}
