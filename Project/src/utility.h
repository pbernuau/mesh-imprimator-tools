#ifndef UTILITY_H
#define UTILITY_H

#include <MeshImprimator/Config.h>

template <typename T>
Vector3d vector_from_pointer(T* const ptr)
{
    return Vector3d(static_cast<double>(ptr[0]),
                    static_cast<double>(ptr[1]),
                    static_cast<double>(ptr[2]));
}

#endif // UTILITY_H
