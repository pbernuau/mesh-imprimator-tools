#include <MeshImprimator/IO/PLYReader.h>

#include <MeshImprimator/Geometry/PointCloud.h>
#include <MeshImprimator/IO/Ply/ElementClass.h>
#include <MeshImprimator/IO/Ply/ElementInstance.h>
#include <MeshImprimator/IO/Ply/Exceptions.h>
#include <MeshImprimator/IO/Ply/Object.h>
#include <MeshImprimator/IO/Ply/Property.h>
#include <MeshImprimator/IO/Ply/PropertyValue.h>

//==============================================================================

PLYReaderException::PLYReaderException(int code) :
    std::exception(),
    code(code)
{
}

PLYReader::PLYReader() :
    object(NULL),
    lineNumber(0)
{
    // Standard types
    typeIdentifiers["char"]     = Ply::Property::T_CHAR;
    typeIdentifiers["short"]    = Ply::Property::T_SHORT;
    typeIdentifiers["int"]      = Ply::Property::T_INT;

    typeIdentifiers["uchar"]    = Ply::Property::T_UCHAR;
    typeIdentifiers["ushort"]   = Ply::Property::T_USHORT;
    typeIdentifiers["uint"]     = Ply::Property::T_UINT;

    typeIdentifiers["float"]    = Ply::Property::T_FLOAT;
    typeIdentifiers["double"]   = Ply::Property::T_DOUBLE;

    typeIdentifiers["list"]     = Ply::Property::T_LIST;

    // Add type aliases
    typeIdentifiers["int8"]     = Ply::Property::T_CHAR;
    typeIdentifiers["int16"]    = Ply::Property::T_SHORT;
    typeIdentifiers["int32"]    = Ply::Property::T_INT;

    typeIdentifiers["uint8"]    = Ply::Property::T_UCHAR;
    typeIdentifiers["uint16"]   = Ply::Property::T_USHORT;
    typeIdentifiers["uint32"]   = Ply::Property::T_UINT;

    typeIdentifiers["float32"]  = Ply::Property::T_FLOAT;
    typeIdentifiers["float64"]  = Ply::Property::T_DOUBLE;


}

PLYReader::~PLYReader()
{
    clear();
    close();
}

bool PLYReader::open(const std::string& filename)
{
    file.open(filename.c_str());
    if (file.is_open())
    {
        lineNumber = 1;
        return true;
    }
    else
    {
        return false;
    }
}

bool PLYReader::readHeader(int* errorCode, int* line)
{
    // Make errorCode and line point to a valid location
    int tmp;

    if (line == NULL)
        line = &tmp;
    if (errorCode == NULL)
        errorCode = &tmp;

    // Clear any previously loaded object
    clear();

    object = new Ply::Object();

    Ply::ElementClass* currentClass = NULL;
    int propertyCount = 0;

    try
    {
        if (!file.is_open())
            throw PLYReaderException(INPUT_FILE_IS_NOT_OPEN);

        readSignature();
        readFormat();

        for (;;)
        {
            std::string cmd;

            std::istringstream iss(readLine());
            iss >> cmd;

            if (cmd == "comment")
            {
            }
            else if (cmd == "element")
            {
                if (currentClass != NULL)
                    object->classes.push_back(currentClass);

                std::string name;
                int count;

                iss >> name >> count;
                if (!iss)
                    throw PLYReaderException(INVALID_COMMAND);

                currentClass = new Ply::ElementClass(name, count);
                propertyCount = 0;
            }
            else if (cmd == "property")
            {
                if (currentClass == NULL)
                    throw PLYReaderException(INVALID_COMMAND);

                std::string name, typeId;

                iss >> typeId >> name;
                if (typeId == "list")
                {
                    iss >> name >> name;
                }

                if (!iss)
                    throw PLYReaderException(INVALID_COMMAND);

                Ply::Property* property = new Ply::Property(currentClass,
                                                            propertyCount,
                                                            name,
                                                            getType(typeId));
                currentClass->properties.push_back(property);
                propertyCount++;
            }
            else if (cmd == "end_header")
            {
                if (currentClass != NULL)
                    object->classes.push_back(currentClass);

                currentClass = NULL;
                break;
            }
            else
            {
                throw PLYReaderException(INVALID_COMMAND);
            }
        }

        *errorCode = NO_ERROR;
        *line = lineNumber;
    }
    catch (PLYReaderException& e)
    {
        *errorCode = e.code;
        *line = lineNumber;

        close();
        clear();
    }

    if (currentClass != NULL)
        delete currentClass;

    return object != NULL;
}

bool PLYReader::readContent(int* errorCode, int* line)
{
    // Make errorCode and line point to a valid location
    int tmp;

    if (line == NULL)
        line = &tmp;
    if (errorCode == NULL)
        errorCode = &tmp;

    try
    {
        if (object == NULL)
            throw PLYReaderException(HEADER_NOT_LOADED);

        if (!file.is_open())
            throw PLYReaderException(INPUT_FILE_IS_NOT_OPEN);

        std::vector<Ply::ElementClass*>::iterator it;

        // Read all classes
        for (it = object->classes.begin(); it != object->classes.end(); ++it)
        {
            readInstances(*it);
        }

        *errorCode = NO_ERROR;
        *line = lineNumber;
    }
    catch (PLYReaderException& e)
    {
        *errorCode = e.code;
        *line = lineNumber;

        close();
        clear();
    }

    return object != NULL;
}

void PLYReader::close()
{
    file.close();
    lineNumber = 0;
}

void PLYReader::clear()
{
    if (object != NULL)
        delete object;
    object = NULL;
}

bool PLYReader::isOpen() const
{
    return file.is_open();
}

const Ply::Object* PLYReader::getObject() const
{
    return object;
}

PointCloud PLYReader::read(const std::string& filename)
{
    clear();

    open(filename);
    readHeader();
    readContent();
    close();

    if (object != NULL)
        return object->getPointCloud();
    else
        return PointCloud();
}

std::string PLYReader::readLine()
{
    std::string line;
    if (!std::getline(file, line))
        throw PLYReaderException(PREMATURE_END_OF_FILE);

    lineNumber++;
    return line;
}

void PLYReader::readSignature()
{
    std::string sig = readLine();
    if (sig != "ply")
        throw PLYReaderException(MISSING_PLY_SIGNATURE);
}

void PLYReader::readFormat()
{
    std::string format, mode, version;

    std::istringstream iss(readLine());
    iss >> format >> mode >> version;

    if (format != "format")
        throw PLYReaderException(MISSING_PLY_FORMAT);

    if (mode != "ascii")
        throw PLYReaderException(UNSUPPORTED_PLY_FORMAT);

    if (version != "1.0")
        throw PLYReaderException(UNSUPPORTED_PLY_VERSION);
}

void PLYReader::readInstances(Ply::ElementClass* elementClass)
{
    // Reserve memory for the values
    int count = elementClass->elementCount;
    elementClass->values.reserve(count * elementClass->properties.size());

    std::list<Ply::Property*>::const_iterator it;

    // Read elements line by line
    for (int i = 0; i < count; i++)
    {
        std::istringstream iss(readLine());
        for (it = elementClass->properties.begin();
             it != elementClass->properties.end();
             ++it)
        {
            Ply::PropertyValue value = readInstance(iss, *it);
            elementClass->values.push_back(value);
        }

        if (!iss)
            throw PLYReaderException(INVALID_ELEMENT);
    }
}

Ply::PropertyValue PLYReader::readInstance(std::istream& iss,
                                           const Ply::Property* property)
{
    Ply::PropertyValue value;
    unsigned long int n = 0;

    switch (property->getType())
    {
    case Ply::Property::T_CHAR:
        iss >> value.asInt;
        // TODO: check overflow
        value.asChar = static_cast<signed char>(value.asInt);
        break;
    case Ply::Property::T_SHORT:
        iss >> value.asShort;
        break;
    case Ply::Property::T_INT:
        iss >> value.asInt;
        break;
    case Ply::Property::T_UCHAR:
        iss >> value.asUInt;
        value.asUChar = static_cast<unsigned char>(value.asUInt);
        break;
    case Ply::Property::T_USHORT:
        iss >> value.asUShort;
        break;
    case Ply::Property::T_UINT:
        iss >> value.asUInt;
        break;
    case Ply::Property::T_FLOAT:
        iss >> value.asFloat;
        break;
    case Ply::Property::T_DOUBLE:
        iss >> value.asDouble;
        break;
    case Ply::Property::T_LIST:
        // TODO: this workaround won't work in binary mode
        iss >> n;
        for (unsigned long int i = 0; i < n; i++)
        {
            std::string tmp;
            iss >> tmp;
        }
        value.asUInt = 0;
        break;
    default:
        iss >> n;
        value.asUInt = 0;
    }

    return value;
}

Ply::Property::Type PLYReader::getType(const std::string& typeId) const
{
    std::map<std::string, Ply::Property::Type>::const_iterator it =
            typeIdentifiers.find(typeId);

    if (it == typeIdentifiers.end())
        return Ply::Property::T_UNKNOWN;
    else
        return it->second;
}

//==============================================================================

namespace Ply
{

//==============================================================================

Object::Object(int reserve)
{
    classes.reserve(reserve);
}

Object::~Object()
{
    clear();
}

const ElementClass& Object::get(const std::string& className) const
{
    for (std::vector<ElementClass*>::const_iterator it = classes.begin();
         it != classes.end();
         ++it)
    {
        if ((*it)->getName() == className)
            return **it;
    }

    throw NoSuchElementClassException();
}

PointCloud Object::getPointCloud() const
{
    PointCloud cloud;

    const ElementClass& vertices = get("vertex");
    Property x = vertices.property("x");
    Property y = vertices.property("y");
    Property z = vertices.property("z");

    int count = vertices.getElementCount();
    for (int i = 0; i < count; i++)
    {
        ElementInstance vertex = vertices.get(i);
        cloud.points.insert(Vector3d(vertex.get<double>(x),
                                     vertex.get<double>(y),
                                     vertex.get<double>(z)));
    }

    return cloud;
}

void Object::print(std::ostream& stream) const
{
    stream << "Number of classes: " << classes.size() << std::endl;
    stream << "{" << std::endl;
    for (std::vector<ElementClass*>::const_iterator it = classes.begin();
         it != classes.end();
         ++it)
    {
        (*it)->print(stream);
    }
    stream << "}" << std::endl;
}

void Object::clear()
{
    for (std::vector<ElementClass*>::iterator it = classes.begin();
         it != classes.end();
         ++it)
    {
        delete *it;
    }

    classes.clear();
}

//==============================================================================

ElementClass::ElementClass(const std::string& name, int elementCount) :
    name(name),
    elementCount(elementCount)
{
}

ElementClass::~ElementClass()
{
    clear();
}

std::string ElementClass::getName() const
{
    return name;
}

const Property& ElementClass::property(const std::string& name) const
{
    for (std::list<Property*>::const_iterator it = properties.begin();
         it != properties.end();
         ++it)
    {
        if ((*it)->getName() == name)
            return **it;
    }

    throw NoSuchPropertyException();
}

ElementInstance ElementClass::get(int index) const
{
    if (index < 0 || index >= elementCount)
        throw NoSuchElementInstanceException();

    return ElementInstance(this, index);
}

int ElementClass::getElementCount() const
{
    return elementCount;
}

void ElementClass::print(std::ostream& stream) const
{
    stream << "\tClassname: " << name << std::endl;
    stream << "\tNumber of properties: " << properties.size() << std::endl;
    stream << "\tNumber of elements: " << elementCount << std::endl;
    stream << "\t{" << std::endl;
    for (std::list<Property*>::const_iterator it = properties.begin();
         it != properties.end();
         ++it)
    {
        (*it)->print(stream);
    }
    stream << "\t}" << std::endl;
}

void ElementClass::clear()
{
    for (std::list<Property*>::iterator it = properties.begin();
         it != properties.end();
         ++it)
    {
        delete *it;
    }
}

//==============================================================================

ElementInstance::ElementInstance(const ElementClass* elementClass, int index) :
    elementClass(elementClass),
    index(index)
{
}

ElementInstance::ElementInstance(const ElementInstance& other) :
    elementClass(other.elementClass),
    index(other.index)
{
}

PropertyValue ElementInstance::operator[](const Property& property) const
{
    if (property.ownerClass != elementClass)
        throw ClassMismatchException();

    int valueIndex = index * elementClass->properties.size() + property.offset;
    return elementClass->values.at(valueIndex);
}

PropertyValue ElementInstance::operator[](const std::string& property) const
{
    return (*this)[elementClass->property(property)];
}

//==============================================================================

Property::Property(const ElementClass* ownerClass,
                   int offset,
                   const std::string& name,
                   Property::Type type) :
    ownerClass(ownerClass),
    offset(offset),
    name(name),
    type(type)
{
}

Property::Property(const Property& other) :
    ownerClass(other.ownerClass),
    offset(other.offset),
    name(other.name),
    type(other.type)
{
}

std::string Property::getName() const
{
    return name;
}

Property::Type Property::getType() const
{
    return type;
}

void Property::print(std::ostream& stream) const
{
    stream << "\t\tProperty name: " << name << std::endl;
    stream << "\t\t{" << std::endl;
    stream << "\t\t\tOffset: " << offset << std::endl;
    stream << "\t\t\tType: ";

    switch (type)
    {
    case T_CHAR:    stream << "char";   break;
    case T_SHORT:   stream << "short";  break;
    case T_INT:     stream << "int";    break;
    case T_UCHAR:   stream << "uchar";  break;
    case T_USHORT:  stream << "ushort"; break;
    case T_UINT:    stream << "uint";   break;
    case T_FLOAT:   stream << "float";  break;
    case T_DOUBLE:  stream << "double"; break;
    case T_LIST:    stream << "list";   break;
    default:        stream << "???";    break;
    }

    stream << std::endl;
    stream << "\t\t}" << std::endl;
}

//==============================================================================


} // namespace Ply


