TEMPLATE = lib
CONFIG += dll
CONFIG -= lib_bundle
CONFIG -= qt

PROJECT_DIR = $$relative_path($$_PRO_FILE_PWD_, $$OUT_PWD)

DESTDIR = $$PROJECT_DIR/bin
EXTLIBS = $$PROJECT_DIR/../extlibs

CONFIG(debug, debug|release) {
    TARGET = meshimprimator-d
    DEFINES += MI_DEBUG _GLIBCXX_DEBUG
}

CONFIG(release, debug|release) {
    TARGET = meshimprimator
}

win32 {
    LIBS += -L$$EXTLIBS/libs-mingw/x86
    INCLUDEPATH += $$EXTLIBS/include
}

LIBS += -ltet

DEFINES += MESH_IMPRIMATOR_BUILD

INCLUDEPATH += include src
VPATH += include src

SOURCES += \
    src/STLWriter.cpp \
    src/Triangle.cpp \
    src/DelaunayReconstructor.cpp \
    src/PointCloud.cpp \
    src/PLYReader.cpp \
    src/MaskData.cpp \
    src/PowercrustReconstructor.cpp \
    src/IndexedTriangle.cpp \
    src/IndexedTriangleMesh.cpp \
    src/FeaturePreservingDenoiser.cpp

HEADERS += \
    include/MeshImprimator.h \
    include/MeshImprimator/Geometry.h \
    include/MeshImprimator/Algorithms.h \
    include/MeshImprimator/IO.h \
    include/MeshImprimator/Config.h \
    include/MeshImprimator/Geometry/Compare3d.h \
    include/MeshImprimator/Geometry/PointCloud.h \
    include/MeshImprimator/Geometry/Triangle.h \
    include/MeshImprimator/Geometry/TriangleMesh.h \
    include/MeshImprimator/Geometry/IndexedTriangle.h \
    include/MeshImprimator/Geometry/IndexedTriangleMesh.h \
    include/MeshImprimator/Algorithms/IProgress.h \
    include/MeshImprimator/Algorithms/IMeshReconstructor.h \
    include/MeshImprimator/Algorithms/MaskData.h \
    include/MeshImprimator/Algorithms/DelaunayReconstructor.h \
    include/MeshImprimator/Algorithms/PowercrustReconstructor.h \
    include/MeshImprimator/Algorithms/IMeshDenoiser.h \
    include/MeshImprimator/Algorithms/FeaturePreservingDenoiser.h \
    include/MeshImprimator/Algorithms/Voronoi.h \
    include/MeshImprimator/IO/PLYReader.h \
    include/MeshImprimator/IO/STLWriter.h \
    include/MeshImprimator/IO/Ply/Exceptions.h \
    include/MeshImprimator/IO/Ply/PropertyValue.h \
    include/MeshImprimator/IO/Ply/Property.h \
    include/MeshImprimator/IO/Ply/ElementInstance.h \
    include/MeshImprimator/IO/Ply/ElementClass.h \
    include/MeshImprimator/IO/Ply/Object.h \
    src/utility.h
